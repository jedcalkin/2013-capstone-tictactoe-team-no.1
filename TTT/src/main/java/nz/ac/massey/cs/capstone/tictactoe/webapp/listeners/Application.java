package nz.ac.massey.cs.capstone.tictactoe.webapp.listeners;


import java.io.StringWriter;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

/**
 * The TicTacToe Application
 *
 * @author Colin Campbell
  */
public class Application implements ServletContextListener {

	public static final String LOBBY = "_LOBBY";
	public static final String BOT = "_BOT";
	public static final String TIMEOUTS = "_TIMEOUTS";
	public static final String USER_DAO = "_USER_DAO";
	public static final String BOT_DAO = "_BOT_DAO";
	private static final String DRIVER = "jdbc:sqlite:";

	@Override
    public void contextInitialized( final ServletContextEvent sce ) {

		/*
		 * Database URL
		 */
		final String databaseUrl = DRIVER +
				sce.getServletContext()
				.getRealPath("TTT.db");

		/* Instantiate Lobby */
		sce.getServletContext().setAttribute( LOBBY, new Lobby() );

		/* Instantiate Timeouts */
		sce.getServletContext().setAttribute( TIMEOUTS, new ConcurrentHashMap<Game, Long[]>() );

		try {

			/* Database Connection */
			final ConnectionSource connectionSource = new JdbcConnectionSource( databaseUrl );

			/* Create Tables for user */
			TableUtils.createTableIfNotExists( connectionSource, User.class );
			
			/* Create Tables for botCode */
			TableUtils.createTableIfNotExists( connectionSource, BotCode.class );

			/* DataAccess Objects for user*/
			sce.getServletContext().setAttribute( USER_DAO, DaoManager.createDao( connectionSource, User.class ) );

			final Dao<BotCode, Integer> botDao  = DaoManager.createDao( connectionSource, BotCode.class );

			/* DataAccess Objects for bot code */
			sce.getServletContext().setAttribute( BOT_DAO, botDao );

			final StringWriter dumbBotSourceCode = new StringWriter();
			dumbBotSourceCode.append( "import java.util.Random;" );
			dumbBotSourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
			dumbBotSourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
			dumbBotSourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
			dumbBotSourceCode.append( "public class CustomBot implements Robot {" );
			dumbBotSourceCode.append( "public Move nextPosition( final Game game ) {" );
			dumbBotSourceCode.append( "final Random randomNext = new Random();" );
			dumbBotSourceCode.append( "Move nextMove = new Move( randomNext.nextInt( 9 ) + 0, randomNext.nextInt( 9 ) + 0 );" );
			dumbBotSourceCode.append( "if( game.isVaildMove(nextMove) ){");
			dumbBotSourceCode.append( "return nextMove;}");
			dumbBotSourceCode.append( "else{return nextPosition(game);}");
			dumbBotSourceCode.append( "}" );
			dumbBotSourceCode.append( "}" );
			
			final StringWriter smartBotSourceCode = new StringWriter();
			smartBotSourceCode.append( "import java.util.Random;" );
			smartBotSourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
			smartBotSourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
			smartBotSourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
			smartBotSourceCode.append( "public class CustomBot implements Robot {" );
			smartBotSourceCode.append( "public Move nextPosition( final Game game ) {" );
			smartBotSourceCode.append( "final Random randomNext = new Random();" );
			smartBotSourceCode.append( "Move nextMove = new Move( randomNext.nextInt( 9 ) + 0, randomNext.nextInt( 9 ) + 0 );" );
			smartBotSourceCode.append( "if( game.isVaildMove(nextMove) ){");
			smartBotSourceCode.append( "return nextMove;}");
			smartBotSourceCode.append( "else{return nextPosition(game);}");
			smartBotSourceCode.append( "}" );
			smartBotSourceCode.append( "}" );

			final BotCode dumbBotCode = new BotCode( "Default Dumb", dumbBotSourceCode.toString() );
			final BotCode smartBotCode = new BotCode( "Default Smart", dumbBotSourceCode.toString() );

			dumbBotCode.compile();
			smartBotCode.compile();
			
			if(botDao.queryForId(1)==null){
				
				botDao.createIfNotExists(dumbBotCode);
				botDao.createIfNotExists(smartBotCode);
				
			}
			

		} catch ( SQLException e ) {

			throw new RuntimeException( e );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    }

	@Override
    public void contextDestroyed( final ServletContextEvent sce ) {

		/* Tidy Up */
		sce.getServletContext().removeAttribute( LOBBY );
		sce.getServletContext().removeAttribute( BOT );
		sce.getServletContext().removeAttribute( USER_DAO );
		sce.getServletContext().removeAttribute( TIMEOUTS );
		sce.getServletContext().removeAttribute( BOT_DAO );
    }
}
