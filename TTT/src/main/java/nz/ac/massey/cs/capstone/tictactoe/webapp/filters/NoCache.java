package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * NoCache Filter
 *
 * @author Colin Campbell
 */
public class NoCache implements Filter {

	@Override
	public void doFilter( final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {

		if ( response instanceof HttpServletResponse ) {

			( ( HttpServletResponse ) response ).setHeader("Cache-Control", "no-cache");
			( ( HttpServletResponse ) response ).setDateHeader("Expires", 0);
			( ( HttpServletResponse ) response ).setHeader("Pragma", "No-cache");
		}

		chain.doFilter( request, response );
	}

	@Override
	public void init( final FilterConfig filterConfig ) throws ServletException {

		// Do nothing

	}

	@Override
	public void destroy() {

		// Do nothing
	}
}
