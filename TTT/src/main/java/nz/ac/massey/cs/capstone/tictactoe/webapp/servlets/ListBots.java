package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;

/**
 * Servlet for Listing all verified Bots
 * 
 * @author Colin Campbell
 */
public class ListBots extends HttpServlet {

	/** Gson Object */
	private Gson gson;

	/** The BotCode Data Access Object */
	private Dao<BotCode, Integer> botCodeDao;

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

	/**
	 * Field Map of Bots to Query
	 */
	private static final ImmutableMap<String, Object> BOTCODE_VALUES = new ImmutableMap.Builder<String, Object>().put( "verified", Boolean.TRUE ).build();

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 2985879591723383328L;

	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		try {

			gson.toJson(
					Lists.transform(
							botCodeDao.queryForFieldValues( BOTCODE_VALUES ), new Function<BotCode, BotView>() {

								@Override
								public BotView apply( final BotCode botCode ) {

									return new BotView( botCode.getIdentity(), botCode.getName() );
								}
							}
					), response.getWriter() );

		} catch ( final SQLException e) {

			throw new IOException( e );
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize gson */
		gson = new Gson();

		/* Initialize bot code data access object */
		botCodeDao = ( Dao<BotCode, Integer> ) getServletContext().getAttribute( Application.BOT_DAO );
	}

	/**
	 * Readonly View of a BotCode
	 * 
	 * @author Colin Campbell
	 */
	private class BotView {

		@SuppressWarnings("unused")
		public final Integer identity;

		@SuppressWarnings("unused")
		public final String name;

		public BotView( final Integer identity, final String name ) {

			this.identity = identity;
			this.name = name;
		}
	}
}
