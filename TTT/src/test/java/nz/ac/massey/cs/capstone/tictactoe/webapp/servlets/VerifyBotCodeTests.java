package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.j256.ormlite.dao.Dao;

/**
 * Test cases for the CompileBotCode servlet
 *
 * @author Li Sui
 */
@RunWith(MockitoJUnitRunner.class)
public class VerifyBotCodeTests {

	private VerifyBotCode servlet;
	
	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;


	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	
	@Mock
	private Dao<BotCode, Integer> botCodeDao;
	

	@Before
	public void setUp() throws ServletException {
		
		when( context.getAttribute(Application.BOT_DAO)).thenReturn(botCodeDao);
		when( config.getServletContext() ).thenReturn( context );

		servlet = new VerifyBotCode();
		servlet.init( config );
	}
	
	
	/**
	 * Test ClassNotFound
	 * @throws Exception
	 */
	@Test
	public void testWrongClassName() throws Exception{
		
		final StringWriter sourceCode =new StringWriter();
		final StringWriter input = new StringWriter();
		
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
		sourceCode.append( "public class Name implements Robot {" );
		sourceCode.append( "public Move nextPosition( final Game game ) {" );
		sourceCode.append( "return new Move( 1, 0 );" );
		sourceCode.append( "}" );
		sourceCode.append( "}" );
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		
		when( request.getParameter( "botId") ).thenReturn( "2" );
		when( botCodeDao.queryForId(2)).thenReturn(botCode);
		when( botCodeDao.idExists(2)).thenReturn(true);
		
		when( request.getReader() )
		.thenReturn(
			new BufferedReader(
				new StringReader( new Gson().toJson( botCode ) )
			)
		);
		
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		 
		servlet.doPost( request, response );
		
		final JsonObject result = new JsonParser()
		.parse( input.toString() )
		.getAsJsonObject();
		
		assertFalse("should return false",result.get("status").getAsBoolean());
	
	}
	
	/**
	 * Test Save BotCode before compile it(SQL Exception)
	 * @throws Exception
	 */
	@Test
	public void testSaveBeforeCompile() throws Exception{
		
		final StringWriter sourceCode =new StringWriter();
		final StringWriter input = new StringWriter();
		
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
		sourceCode.append( "public class CustomBot implements Robot {" );
		sourceCode.append( "public Move nextPosition( final Game game ) {" );
		sourceCode.append( "return new Move( 1, 0 );" );
		sourceCode.append( "}" );
		sourceCode.append( "}" );
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		
		when( request.getParameter( "botId") ).thenReturn( "2" );
		when( botCodeDao.queryForId(2)).thenReturn(botCode);
		when( botCodeDao.idExists(2)).thenReturn(false);
		
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		 
		servlet.doPost( request, response );
		
		verify( response ).sendError(HttpServletResponse.SC_BAD_REQUEST);
	
	}
	
	/**
	 * Test for Compile errors(not return)
	 * @throws Exception
	 */
	@Test
	public void testCompileError() throws Exception{
		
		final StringWriter sourceCode =new StringWriter();
		final StringWriter input = new StringWriter();
		
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
		sourceCode.append( "public class CustomBot implements Robot {" );
		sourceCode.append( "public Move nextPosition( final Game game ) {" );
		sourceCode.append( "}" );
		sourceCode.append( "}" );
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		when( request.getParameter( "botId") ).thenReturn( "2" );
		when( botCodeDao.queryForId(2)).thenReturn(botCode);
		when( botCodeDao.idExists(2)).thenReturn(true);
		
		when( request.getReader() )
		.thenReturn(
			new BufferedReader(
				new StringReader( new Gson().toJson( botCode ) )
			)
		);
		
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		 
		servlet.doPost( request, response );
		
		final JsonObject result = new JsonParser()
		.parse( input.toString() )
		.getAsJsonObject();
		
		assertFalse("should return IllegalAccess",result.get("status").getAsBoolean());
	
	}
	
	/**
	 * Test for Parse errors (extra curly bracket)
	 * @throws Exception
	 */
	@Test
	public void testParseError() throws Exception{
		
		final StringWriter sourceCode =new StringWriter();
		final StringWriter input = new StringWriter();
		
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;\n" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;\n" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;\n" );
		sourceCode.append( "public class CustomBot implements Robot {\n" );
		sourceCode.append( "public Move nextPosition( final Game game ) {\n" );
		sourceCode.append( "return new Move( 1, 0 );}\n" );
		sourceCode.append( "}\n" );
		sourceCode.append( "}\n" );
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		when( request.getParameter( "botId") ).thenReturn( "2" );
		when( botCodeDao.queryForId(2)).thenReturn(botCode);
		when( botCodeDao.idExists(2)).thenReturn(true);
		
		
		when( request.getReader() )
		.thenReturn(
			new BufferedReader(
				new StringReader( new Gson().toJson( botCode ) )
			)
		);
		
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		 
		servlet.doPost( request, response );
		
		final JsonObject result = new JsonParser()
		.parse( input.toString() )
		.getAsJsonObject();
		
		assertFalse("should return IllegalAccess",result.get("status").getAsBoolean());
	
	}

	
	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCompileSuccessfully() throws Exception{
		
		final StringWriter sourceCode =new StringWriter();
		final StringWriter input = new StringWriter();
		
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;\n" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;\n" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;\n" );
		sourceCode.append( "public class CustomBot implements Robot {\n\n" );
		sourceCode.append( "\t@Override\n");
		sourceCode.append( "\tpublic Move nextPosition( final Game game ) {\n\n" );
		sourceCode.append( "\t\treturn new Move( 1, 0 );\n" );
		sourceCode.append( "\t}\n" );
		sourceCode.append( "}\n" );
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		when( request.getParameter( "botId") ).thenReturn( "2" );
		when( botCodeDao.queryForId(2)).thenReturn(botCode);
		when( botCodeDao.idExists(2)).thenReturn(true);
		
		
		when( request.getReader() )
		.thenReturn(
			new BufferedReader(
				new StringReader( new Gson().toJson( botCode ) )
			)
		);
		
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		 
		servlet.doPost( request, response );
		
		final JsonObject result = new JsonParser()
		.parse( input.toString() )
		.getAsJsonObject();
		
		assertTrue("should Compile Successfully",result.get("status").getAsBoolean());
	
	}
	
	
	/**
	 * Test that user access to illegal lib(java.IO)
	 * @throws Exception
	 */
	@Test
	public void testNotVerified() throws Exception{
		
		final StringWriter sourceCode =new StringWriter();
		final StringWriter input = new StringWriter();
		
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;\n" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;\n" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;\n" );
		sourceCode.append( "public class CustomBot implements Robot {\n" );
		sourceCode.append( "public Move nextPosition( final Game game ){\n " );
		sourceCode.append( "new java.io.File( \".\");\n" );
		sourceCode.append( "return new Move( 1, 0 );\n" );
		sourceCode.append( "}\n" );
		sourceCode.append( "}\n" );
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		when( request.getParameter( "botId") ).thenReturn( "2" );
		when( botCodeDao.queryForId(2)).thenReturn(botCode);
		when( botCodeDao.idExists(2)).thenReturn(true);
		
		
		when( request.getReader() )
		.thenReturn(
			new BufferedReader(
				new StringReader( new Gson().toJson( botCode ) )
			)
		);
		
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		 
		servlet.doPost( request, response );
		
		final JsonObject result = new JsonParser()
		.parse( input.toString() )
		.getAsJsonObject();
		
		assertFalse("should failed verifcation",result.get("status").getAsBoolean());
	
	}
	
	
}
