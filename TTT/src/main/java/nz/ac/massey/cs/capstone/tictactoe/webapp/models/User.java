package nz.ac.massey.cs.capstone.tictactoe.webapp.models;

import java.io.Serializable;
import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * User
 *
 * @author Li Sui
 */
@DatabaseTable(tableName = "User")
public class User implements Serializable {

	private static final long serialVersionUID = -452897042616369131L;

	@DatabaseField(id=true)
	private String identity;

	@DatabaseField
	private String displayName;

	@DatabaseField
	private String email;

	@DatabaseField
	private Integer numWins;

	@DatabaseField
	private Integer numDraws;

	@DatabaseField
	private Integer numLoses;

	@DatabaseField
	private String gender;

	@DatabaseField
	private String location;

	@DatabaseField
	private String aboutMe;

	@DatabaseField
	private String age;
	
	@ForeignCollectionField
	private Collection<BotCode> botCodes;

	/**
	 * The default constructor does not do anything
	 */
	public User() {

		// Do Nothing
	}


	/**
	 * @return the identity
	 */
	public String getIdentity() {
		return identity;
	}


	/**
	 * @param identity the identity to set
	 */
	public void setIdentity(String identity) {
		this.identity = identity;
	}


	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the numWins
	 */
	public Integer getNumWins() {
		return numWins;
	}

	/**
	 * @param numWins the numWins to set
	 */
	public void setNumWins(Integer numWins) {
		this.numWins = numWins;
	}

	/**
	 * @return the numDraws
	 */
	public Integer getNumDraws() {
		return numDraws;
	}

	/**
	 * @param numDraws the numDraws to set
	 */
	public void setNumDraws(Integer numDraws) {
		this.numDraws = numDraws;
	}

	/**
	 * @return the numLoses
	 */
	public Integer getNumLoses() {
		return numLoses;
	}

	/**
	 * @param numLoses the numLoses to set
	 */
	public void setNumLoses(Integer numLoses) {
		this.numLoses = numLoses;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the aboutMe
	 */
	public String getAboutMe() {
		return aboutMe;
	}

	/**
	 * @param aboutMe the aboutMe to set
	 */
	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}
	
	/**
	 * @return the botCodes
	 */
	public Collection<BotCode> getBots() {
		return botCodes;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aboutMe == null) ? 0 : aboutMe.hashCode());
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result
				+ ((botCodes == null) ? 0 : botCodes.hashCode());
		result = prime * result
				+ ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result
				+ ((identity == null) ? 0 : identity.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result
				+ ((numDraws == null) ? 0 : numDraws.hashCode());
		result = prime * result
				+ ((numLoses == null) ? 0 : numLoses.hashCode());
		result = prime * result + ((numWins == null) ? 0 : numWins.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals()
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (aboutMe == null) {
			if (other.aboutMe != null)
				return false;
		} else if (!aboutMe.equals(other.aboutMe))
			return false;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (botCodes == null) {
			if (other.botCodes != null)
				return false;
		} else if (!botCodes.equals(other.botCodes))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (numDraws == null) {
			if (other.numDraws != null)
				return false;
		} else if (!numDraws.equals(other.numDraws))
			return false;
		if (numLoses == null) {
			if (other.numLoses != null)
				return false;
		} else if (!numLoses.equals(other.numLoses))
			return false;
		if (numWins == null) {
			if (other.numWins != null)
				return false;
		} else if (!numWins.equals(other.numWins))
			return false;
		return true;
	}
	
}