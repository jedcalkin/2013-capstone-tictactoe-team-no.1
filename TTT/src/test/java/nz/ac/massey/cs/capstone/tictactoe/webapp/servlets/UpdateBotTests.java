package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.j256.ormlite.dao.Dao;

/**
 * @author Li Sui
 */
@RunWith( MockitoJUnitRunner.class )
public class UpdateBotTests {

	private UpdateBot servlet;

	@Mock
    private User user;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	
	@Mock
	private Dao<BotCode, Integer> botDao;
	
	@Mock
    private HttpSession session;
	
	@Mock
	private Dao<User, String> userDao;

	@Before
	public void setUp() throws Exception {

		when( context.getAttribute( Application.USER_DAO ) ).thenReturn( userDao );
		when( context.getAttribute( Application.BOT_DAO ) ).thenReturn( botDao );
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession() ).thenReturn( session );
		when( session.getAttribute( "user" ) ).thenReturn( user );
		when( user.getIdentity() ).thenReturn( "Li" );

		servlet = new UpdateBot();
		servlet.init( config );
	}

	/**
	 * Test CreateOrUpdate
	 */
	@Test
	public void testCreateOrUpdate() throws Exception {

		final BotCode botCode = new BotCode( "MyBot", "MyCode" );
		final StringWriter input = new StringWriter();

		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getReader() )
			.thenReturn(
				new BufferedReader(
					new StringReader( new Gson().toJson( botCode ) )
				)
			);

		botCode.setUser( user );
		servlet.doPut( request, response );

		verify( botDao )
			.createOrUpdate(  botCode );
	}

	/**
	 * Test Response User
	 */
	@Test
	public void testUser() throws Exception {

		final StringWriter input = new StringWriter();

		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'identity' : 0, 'verified' : false, 'sourceCode' : codes ,'name' : name }" ) ) );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

        servlet.doPut( request, response );

        final JsonObject result = new JsonParser()
        	.parse( input.toString() )
        	.getAsJsonObject();

        assertEquals( "Li", result.get( "user" ).getAsJsonObject().get( "identity" ).getAsString() );
	}

}
