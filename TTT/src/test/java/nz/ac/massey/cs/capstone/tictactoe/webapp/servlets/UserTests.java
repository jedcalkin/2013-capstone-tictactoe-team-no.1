package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Li Sui
 */
public class UserTests {
	
	private User user;
	
	@Before
	public void setup(){
		
		 user= new User();
		 
	}

	/**
	 * Test method for {@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getGamerTag()}.
	 */
	@Test
	public void testGetGamerTag() {
		
		user.setDisplayName( "li" );

		assertEquals("should get user li","li",user.getDisplayName() );
	}

	/**
	 * Test method for {@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getOpenId()}.
	 */
	@Test
	public void testGetOpenId() {
		
		user.setIdentity( "openid" );
		
		assertEquals("should get openid of user li","openid", user.getIdentity() );
		
	}

	/**
	 * Test method for {@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getemail()}.
	 */
	@Test
	public void testGetemail() {
		
		user.setEmail( "email" );
		
		assertEquals("should get image link of user li","email",user.getEmail());	
	}
	
	/**
	 * Test method for number of wins{@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getNumWins()}.
	 */
	@Test
	public void testGetNumWins() {
		
		user.setNumWins(3);
		
		assertTrue(3 == user.getNumWins());	
	}
	
	/**
	 * Test method for number of draws{@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getNumDraws()}.
	 */
	@Test
	public void testGetNumDraws() {
		
		user.setNumDraws(3);
		
		assertTrue(3 == user.getNumDraws());	
	}
	
	/**
	 * Test method for number of losses{@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getNumLoses()}.
	 */
	@Test
	public void testGetNumLoses() {
		
		user.setNumLoses(3);
		
		assertTrue(3 == user.getNumLoses());	
	}
	
	/**
	 * Test method for the gender{@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getGender()}.
	 */
	@Test
	public void testGetGender() {
		
		user.setGender("Male");
		
		assertEquals("should get gender of user li","Male",user.getGender());	
	}
	
	/**
	 * Test method for locations {@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getLocation()}.
	 */
	@Test
	public void testGetLocation() {
		
		user.setLocation("Palmerston North");
		
		assertEquals("should get location of user li","Palmerston North",user.getLocation());	
	}
	
	/**
	 * Test method for about me {@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getAboutMe()}.
	 */
	@Test
	public void testGetAboutMe() {
		
		user.setAboutMe("I'm perfect");
		
		assertEquals("should get about me of user li","I'm perfect",user.getAboutMe());	
	}
	
	/**
	 * Test method for age {@link nz.ac.massey.cs.capstone.tictactoe.webapp.models.User#getAge()}.
	 */
	@Test
	public void testGetAge() {
		
		user.setAge("19");
		
		assertEquals("should get age of user li","19",user.getAge());	
	}
	
}
