package nz.ac.massey.cs.capstone.tictactoe.webapp;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Test cases for first page
 *
 * @author Aaron Cole
 */
public class FirstPageSeleniumTests {

	/**
	 * The web driver
	 */
	private WebDriver driver;
	private WebDriver driver2;
	
	private String userRed="red";
	private String userBlue="blue";
	private String portNumber="8086";

	@Before
	public void setup() {
		
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");// windows version
		driver = new ChromeDriver();
		driver2 = new ChromeDriver();
	}
	
	@Test
	public void testLobby() throws InterruptedException {
		
		driver.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=red");
		assertEquals( "This should be red'email address", "Lobby", driver.findElement(By.id("selenium_Lobby")).getAttribute("value") );
	}
	
	@Test
	public void testGetRedName() throws InterruptedException {

		driver.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=red");
		assertEquals( "This should be red'email address", userRed, driver.findElement(By.id("selenium_userName")).getText() );

	}
	
	@Test
	public void testGetBlueName() throws InterruptedException {
		
		driver2.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=blue");
		assertEquals( "This should be red'email address", userBlue, driver2.findElement(By.id("selenium_userName")).getText() );
  
	}

	@Test
	public void testPublicGame() throws InterruptedException {
		
		driver.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=red");
		driver.findElement(By.xpath("//*[contains(text(), 'CREATE A GAME')]")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("createPublicGame")).click();
		driver2.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=blue");
		Thread.sleep(10000);
		
		driver2.findElement(By.cssSelector("button.public_button")).click();
		Thread.sleep(6000);
		
		assertEquals( "This should be red'email address", "red", driver2.findElement(By.id("creatorTrun")).getAttribute("value") );
	
	}
	
	@Test
	public void testPrivateGame() throws InterruptedException {
		
		driver.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=red");
		driver.findElement(By.xpath("//*[contains(text(), 'CREATE A GAME')]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[contains(text(), 'PRIVATE GAME')]")).click();
		Thread.sleep(3000);
		Alert alert = driver.switchTo().alert();
		Thread.sleep(10000);
		alert.accept();
		driver2.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=blue");
		Thread.sleep(15000);
		
		driver2.findElement(By.cssSelector("button.private_game_button")).click();
		Thread.sleep(10000);
		
		Alert password = driver2.switchTo().alert();
		Thread.sleep(7000);
		password.accept();
		Thread.sleep(5000);
		assertEquals( "This should be red'email address", "red", driver2.findElement(By.id("creatorTrun")).getAttribute("value") );
	}
	
	@Test
	public void testBlueStartBotGame() throws InterruptedException {
		
		driver.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=red");
		Thread.sleep(10000);
		driver.findElement(By.id("playBot")).click();
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("button.public_button")).click();
		Thread.sleep(10000);
		assertEquals( "This should say 'I am a smart bot'", "I am a smart Bot", driver.findElement(By.id("opponent")).getText() );
	}
	
	@Test
	public void testBackToLobby() throws InterruptedException {
		
		driver.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=red");
		driver.findElement(By.xpath("//*[contains(text(), 'CREATE A GAME')]")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("createPublicGame")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[contains(text(), 'GAME LOBBY')]")).click();
		Thread.sleep(2000);
		assertEquals( "This should be red'email address", "Lobby", driver.findElement(By.id("selenium_Lobby")).getAttribute("value") );
	
	}
	
	@Test
	public void testProfilePage() throws InterruptedException {
	 
		driver.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=red");
		
		Thread.sleep(5000);
		driver.get("http://localhost:"+portNumber+"/TTT/profile.jsp?userId=red");
		Thread.sleep(2000);
		assertEquals( "This should be red'email address", "Profile", driver.findElement(By.id("selenium_Profile")).getAttribute("value") );
	}
	
	@Test
	public void testIDE() throws InterruptedException {
	 
		driver.get("http://localhost:"+portNumber+"/TTT/?jabberwocky=2BFBA931BA4D2C77DCD13155647AA&id=red");
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[contains(text(), 'CREATE A BOT')]")).click();
		Thread.sleep(7000);
		
		assertEquals( "This should be red'email address", "red", driver.findElement(By.id("selenium_IDE")).getAttribute("value") );
	}

	@After
	public void tearDown() {

		driver.close();
		driver2.close();
	}
}