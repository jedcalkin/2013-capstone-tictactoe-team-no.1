package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;

/**
 * Servlet for Getting a Game
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class GetGame extends HttpServlet {

	/** The lobby */
	private Lobby lobby;

	/** Gson Object */
	private Gson gson;

	/** Game ID Parameter */
	private static final String PARAM_GAMEID = "gameId";

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = -734943904279240447L;

	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		/* Get Game */
		final Game game = lobby.getGameByGameId( request.getParameter( PARAM_GAMEID ) );

		/* Write Game as JSON */
		gson.toJson( game, response.getWriter() );
	}

	@Override
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize gson */
		gson = new Gson();

		/* Initialize lobby */
		lobby = ( Lobby ) getServletContext().getAttribute( Application.LOBBY );
	}
}
