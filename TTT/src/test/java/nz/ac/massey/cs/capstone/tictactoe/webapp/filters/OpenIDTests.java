package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;
import static org.mockito.Mockito.when;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.OpenID;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.ParameterList;

import com.j256.ormlite.dao.Dao;

/**
 * @author Li Sui
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class OpenIDTests {

	/**
	 * Filter Under test
	 */
	@Spy
	private OpenID filter = new OpenID();
	
	private User user;
	
	/**
	 * OpenID Identity
	 */
	private final String IDENTITIY = "openid.identity";

	/**
	 * OpenID Email
	 */
	private final String EMAIL = "openid.ext1.value.email";

	/**
	 * OpenID First Name
	 */
	private final String FIRST_NAME = "openid.ext1.value.firstName";
	

	@Mock
	private FilterChain chain;

	/**
	 * Mock HTTP Request
	 */
	@Mock
	private HttpServletRequest request;

	/**
	 * Mock HTTPResponse
	 */
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private HttpSession session;
	
	@Mock
	private  ServletContext context;
	
	@Mock
	private Dao<User,String> userDao;
	
	@Mock
	private ConsumerManager manager; 
	
	@Mock
	private FilterConfig fConfig;
	
	@Mock
	private VerificationResult verification;
	
	@Mock
	private Identifier verified;
	
	@Mock
	private DiscoveryInformation discovered ;
	
	@Mock
	private ParameterList responseList;
		
	@Before
	public void setUp() throws Exception {
		
		
		user=new User();
		manager = new ConsumerManager();
		
		when( request.getSession() ).thenReturn( session );
		when( context.getAttribute( Application.USER_DAO ) ).thenReturn( userDao );
		when(fConfig.getServletContext()).thenReturn( context );
	    
		user.setIdentity("123");
		user.setDisplayName("Li");
		user.setEmail("email");

		filter.init(fConfig);
	}

	@Test
	public void test() throws Exception {
		
		
//		when (request.getSession().getAttribute("openid")).thenReturn(discovered);
//		when (manager.verify("url",responseList,discovered)).thenReturn (verification);
//		when(verification.getVerifiedId()).thenReturn(verified);
//		
//		when( request.getParameter( IDENTITIY  ) ).thenReturn( user.getIdentity() );
//		when( request.getParameter( EMAIL ) ).thenReturn( user.getEmail() );
//		when( request.getParameter( FIRST_NAME  ) ).thenReturn( user.getDisplayName() );
//		when (userDao.queryForId("123")).thenReturn(null);
		
		//filter.doFilter( request, response, chain );
		
		//verify( session).setAttribute("user", user);
		
	}

}
