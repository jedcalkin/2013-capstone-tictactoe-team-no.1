package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.stream.JsonWriter;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

/**
 * Servlet for Creating a Game
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class CreateGame extends HttpServlet {      

	/** isVisible Parameter */
	private static final String PARAM_VISIBLE = "isVisible";

	/** Password Parameter */
	private static final String PARAM_PASSWORD = "password";

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

	/** Serial Version UID */
	private static final long serialVersionUID = -8736841139874577006L;

	@Override
	protected void doPost( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Lobby */
		final Lobby lobby = ( Lobby ) getServletContext().getAttribute( Application.LOBBY );

		/* User */
		final User user = ( User ) request.getSession().getAttribute( Authentication.USER );

		/* Game Password */
		final String password = request.getParameter( PARAM_PASSWORD );

		/* Game Visibility */
		final Boolean visible = Boolean.valueOf( request.getParameter( PARAM_VISIBLE ) );

		/* Create Game */
		final String gameId = lobby.createGame( user.getIdentity(), visible, password );

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		/* JSON Output Stream */
	    final JsonWriter out = new JsonWriter( response.getWriter() );

	    /* Write Output */
	    out.beginObject()
	    	.name( "gameId" ).value( gameId )
	    	.endObject();

	    out.close();
	}
}
