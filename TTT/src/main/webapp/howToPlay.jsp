<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import='com.j256.ormlite.dao.Dao' %>
<%@ page import='nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application' %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );
@SuppressWarnings("unchecked")
final Dao<User, String> userDao = ( Dao<User, String> ) getServletContext().getAttribute( Application.USER_DAO );
userDao.refresh( user );
%>
<%@ include file="page_head_tmp.include" %>
	<a href="profile.jsp?userId=<%=user.getIdentity()%>" title="Profile"><img id="user_image" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="gravatar" /></a>
	<a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank" title="Profile"><img id="profile_link" src="./Media/cog.png" alt="profile_link" /></a>
	<input type="hidden" id ="selenium_Lobby" value="Lobby"/>
</header>
<script data-main="js/howToPlay" src="js/require.js"></script>
<%@ include file="page_menu_tmp.include" %>
<section>
	<div id="player1" class="current-player">
		<b id="selenium_userName">${user.displayName}</b><br>
		<a href="https://en.gravatar.com/" title="Gravatar">
			<img src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=256" alt="gravatar" />
		</a><br>
		<b><%= resourceBundle.getString( "WINS" ) %>:&ensp;</b>${user.numWins }<br>
		<b><%= resourceBundle.getString( "DRAWS" ) %>:&ensp;</b>${user.numDraws}<br>
		<b><%= resourceBundle.getString( "LOSSES" ) %>:&ensp;</b>${user.numLoses}<br>
	</div>

	<div id="player2">
	<b id="opponent">I am a smart Bot</b><br>
	<img src="Media/robot_color.svg" alt="this bot is smarter than you" /><br>
	
	</div>

	<table id="maingame"><tbody><tr><td class="draw"><table class="subgame"><tbody><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table></td><td class="draw"><table class="subgame"><tbody><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table></td><td class="draw"><table class="subgame"><tbody><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table></td></tr><tr><td class="draw"><table class="subgame"><tbody><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table></td><td class="draw"><table class="subgame"><tbody><tr><td class="open"></td><td class="open"></td><td class="open"></td></tr><tr><td class="open"></td><td class=""></td><td class="open"></td></tr><tr><td class="open"></td><td class="open"></td><td class="open"></td></tr></tbody></table></td><td class="draw"><table class="subgame"><tbody><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table></td></tr><tr><td class="draw"><table class="subgame"><tbody><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table></td><td class="draw"><table class="subgame"><tbody><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table></td><td class="draw"><table class="subgame"><tbody><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table></td></tr></tbody></table>
</section>

<div id="tooltip" class="ul-draggable">
	<span>Gameplay Tutorial</span>
	<hr>
	<p></p>
	<div id="buttons">
		<input type="button" id="endtut" name="endtutorial" value="Exit to Lobby" />
		<input type="button" id="nexttut" name="next" value="Next Tip" />
	</div>
</div>

<%@ include file="page_tail_tmp.include" %>