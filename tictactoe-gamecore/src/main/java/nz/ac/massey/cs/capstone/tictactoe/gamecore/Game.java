package nz.ac.massey.cs.capstone.tictactoe.gamecore;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.exceptions.InvalidMoveException;

/**
 * The Game
 *
 * @author Li Sui
 */
public class Game implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -5139240443147142879L;

	/**
	 * Board
	 */
	private final PlayerType[][] board;

	/**
	 * Nested Boards
	 */
	private final PlayerType[][][] nestedBoards;

	/**
	 * Moves
	 */
	private final List<Move> moves;

	/**
	 * Visible
	 */
	private final Boolean isVisible;

	/**
	 * Game State
	 */
	private GameState state;

    /**
     * Creator Id
     */
    private String creatorId;

    /**
     * Opponent Id
     */
    private String opponentId;

	/**
	 * Open
	 */
	private Boolean isOpen;

	/**
	 * password
	 */
	private String password;

	/**
	 * Constructs a new open Game visible game
	 */
	protected Game() {

		this( null, true ,null);
	}

	/**
	 * Constructs a new Game with a specified creator and visibility
	 *
	 * @param creatorId the creator id
	 * @param isVisible true if game is visible
	 */
	protected Game( final String creatorId, final Boolean isVisible, final String password ) {

		this.creatorId = creatorId;
		this.isVisible = isVisible;
		this.board = new PlayerType[3][3];
		this.nestedBoards = new PlayerType[9][3][3];
		this.moves = Lists.newArrayList();
		this.isOpen = true;
		this.state = GameState.INCOMPLETE;
		this.password=password;
	}

	/**
	 * Gets the password
	 *
	 * @return the password
	 */
	public String getPassword() {

		return this.password;
	}

	/**
     * Gets the creatorId
     *
     * @return the creatorId
     */
    public String getCreatorId() {

            return this.creatorId;
    }


    /**
     * Sets the creatorId
     *
     * @param creatorId the creatorId to set
     */
    protected void setCreatorId( final String creatorId ) {

            this.creatorId = creatorId;
    }

    /**
     * Gets the opponentId
     *
     * @return the opponentId
     */
    public String getOpponentId() {

            return opponentId;
    }

    /**
     * Sets the opponentId
     *
     * @param opponentId the opponentId to set
     */
    protected void setOpponentId( final String opponentId ) {

            this.opponentId = opponentId;
    }

	/**
	 * Gets the board
	 *
	 * @return a copy of the board
	 */
	public PlayerType[][] getBoard() {

		PlayerType[][] board = new PlayerType[ 3 ][ 3 ];

		for( int i = 0 ; i < this.board.length ; i++ ) {
			board[ i ] = Arrays.copyOf( this.board[ i ], this.board[ i ].length );
		}

		return board;
	}

	/**
	 * Gets a nested board
	 *
	 * @param board the nested board to get
	 *
	 * @return a copy of the nested board
	 */
	public PlayerType[][] getNestedBoard( final int board ) {

		PlayerType[][] nestedBoard = new PlayerType[ 3 ][ 3 ];

		for( int i = 0 ; i < nestedBoards[ board ].length ; i++ ) {
			nestedBoard[ i ] = Arrays.copyOf( nestedBoards[ board ][ i ], this.nestedBoards[ board ][ i ].length );
		}

		return nestedBoard;
	}

	/**
	 * Adds a move
	 *
	 * @param move the move to add
	 * @throws InvalidMoveException if the position is not available
	 */
	public void addMove( final Move move ) throws InvalidMoveException {

		final CoordinateWrapper coordinates = new CoordinateWrapper( move );
		final Coordinate coordinate = coordinates.getCoordinate();
		final Coordinate nestedCoordinate = coordinates.getNestedCoordinate();
		final PlayerType[][] subBoard = nestedBoards[ move.nestedGame ];

		if( state.equals( GameState.INCOMPLETE ) ){

			if( isVaildMove( move ) ) {

				PlayerType currentPlayer;

				if ( moves.size() % 2 == 0 ) {				
					currentPlayer =PlayerType.CROSS;
    			}

				else {				
					currentPlayer=PlayerType.NOUGHT;
				}

				nestedBoards[ move.nestedGame ][ nestedCoordinate.getRow() ][ nestedCoordinate.getColumn() ] = currentPlayer;

				if( hasWon( subBoard, currentPlayer, nestedCoordinate ) ) {
					board[ coordinate.getRow() ][ coordinate.getColumn() ] = currentPlayer;
				}

				else if( isDraw( subBoard ) ) {
					board[ coordinate.getRow() ][ coordinate.getColumn() ] = PlayerType.EMPTY;
				}

				if( hasWon( board, currentPlayer, coordinate) ) {
					state=GameState.WIN;
				}

				else if( isDraw( board ) ) {
					state=GameState.DRAW;
				}

				else {
					state = GameState.INCOMPLETE;

				}

				moves.add( move );
			}

			else {
    			throw new InvalidMoveException();
    		}
		}
	}

	/**
	 * Gets the moves
	 *
	 * @return the positions as an unmodifiable list
	 */
	public List<Move> getMoves() {

		return ImmutableList.copyOf( moves );
	}

	/**
	 * Gets isVisible
	 *
	 * @return true if the game is visible
	 */
	public Boolean getIsVisible() {

		return isVisible;
	}

	/**
	 * Gets the state
	 *
	 * @return the state
	 */
	public GameState getState() {

		return state;
	}

	/**
	 * Sets the state
	 *
	 * @param state the state to set
	 */
	protected void setState( final GameState state ) {

		this.state = state;
	}

	/**
	 * Gets isOpen
	 *
	 * @return true if the game is open
	 */
	public Boolean getIsOpen() {

		return isOpen;
	}

	/**
	 * Sets isOpen
	 *
	 * @param isOpen true if the game is open
	 */
	protected void setIsOpen( final Boolean isOpen ) {

		this.isOpen = isOpen;
	}

	public Boolean isVaildMove( final Move move ) {

		if( moves.isEmpty() ) {   

			if ( move.nestedGame == 4 && move.position == 4 ) {
				return false;
			}

			if( move.nestedGame != 4 ) {
				return false;
			}

		}

		else {

			final Move lastMove = moves.get( moves.size() - 1 );
			final CoordinateWrapper coordinates = new CoordinateWrapper( lastMove );
			final Coordinate lastMetaCoord = coordinates.getNestedCoordinate();
			final CoordinateWrapper currentCoord = new CoordinateWrapper( move );
			final Coordinate currentMetaCoord = currentCoord.getCoordinate();

			if( moves.contains( move ) ) {
				return false;
			}			      

			if( board[ lastMetaCoord.getRow() ][ lastMetaCoord.getColumn() ] == null ) {

				if(  lastMove.position != move.nestedGame ) {
					return false;
				}
			}

			if( board[ currentMetaCoord.getRow() ][ currentMetaCoord .getColumn() ] != null ) {
				return false;
			}
		}

		return true;
	}

	private boolean isDraw( final PlayerType[][] board ) {

		for ( int row = 0; row < 3; ++row) {

			for ( int col = 0; col < 3; ++col) {

				if ( board[ row ][ col ] == null ) {           	
					return false;
				}
			}
		}

		return true; 
	}

	private boolean hasWon( final PlayerType[][] board, final PlayerType player, final Coordinate coordinate ) {

      return ( board [ coordinate.getRow() ] [ 0 ] == player  
            && board [ coordinate.getRow() ] [ 1 ] == player
            && board [ coordinate.getRow() ] [ 2 ] == player
       ||  	   board [ 0 ] [ coordinate.getColumn() ] == player     
            && board [ 1 ] [ coordinate.getColumn() ] == player
            && board [ 2 ] [ coordinate.getColumn() ] == player
       || coordinate.getRow() == coordinate.getColumn()          
            && board [ 0 ] [ 0 ] == player
            && board [ 1 ] [ 1 ] == player
            && board [ 2 ] [ 2 ] == player
       || coordinate.getRow() + coordinate.getColumn() == 2 
            && board [ 0 ] [ 2 ] == player
            && board [ 1 ] [ 1 ] == player
            && board [ 2 ] [ 0 ] == player);
   }
}
