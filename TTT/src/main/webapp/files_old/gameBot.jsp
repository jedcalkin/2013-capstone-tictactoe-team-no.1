<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import='nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application' %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );

%>
<%@ include file="page_head.include" %>
<div id="login_info">
	<a href="https://en.gravatar.com/">
		<img class="glossy image-wrap" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="gravatar" />
	</a>
	<%= resourceBundle.getString( "CURRENTLY_LOGIN" ) %>:<br>
	<b>${user.email}</b><br>
	<b><a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank"><%= resourceBundle.getString( "PROFILE" ) %></a></b>
</div>
</header>
<script data-main="js/gameBot" src="js/require.js"></script>
<%@ include file="page_menu.include" %>
<section>
	<div id="player1">
		<b>${user.displayName} </b><br>
		<a href="https://en.gravatar.com/">
			<img src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=156" alt="gravatar" />
		</a><br>
	</div>

	<div id="player2">
	<b id="opponent"><%= resourceBundle.getString( "SMART_BOT" ) %></b><br>
	<img src="Media/rebot.jpg" alt="this bot is smarter than you" /><br>
	
	</div>

	<table id="maingame">
	<tr>
	<td>
		<img id="spinner" src="./Media/loading.gif" alt="Loading...">
	</td>
	</tr>
	</table>
</section>

<%@ include file="page_tail.include" %>
