package nz.ac.massey.cs.capstone.tictactoe.gamecore;

/**
 * Describes a Coordinate as a row and column
 *
 * @author Colin Campbell
 * @author Tom Magson
 * @author Li Sui
 */
public interface Coordinate {

	/**
	 * Gets the row
	 *
	 * @return the row
	 */
	public int getRow();

	/**
	 * Gets the column
	 *
	 * @return the column
	 */
	public int getColumn();

}
