requirejs.config({
		paths : {
			jquery : '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
			i18n : '//cdn.jsdelivr.net/i18next/1.7.1/i18next.min',
		    ui : '//code.jquery.com/ui/1.10.3/jquery-ui'

	},
	shim: {
			jquery : {
					exports: "jQuery"
			},
			i18n : {
				exports: "i18n"
			},
			ui : {
				deps: ['jquery']
			},
		

	}
});

require( ['jquery', 'i18n', 'ui'], function( $, i18n ) {
	tipcount = 0;
	var tips = [
	  function() { addToolTip( 
	  	"This short tutorial will teach you the basics of nested tic tac toe",
	  	function() {
	  		console.log("Tip 1");
	  		$("td").removeClass("open");
	  	}
	  ); },
	  function() { addToolTip( 
	  	"Nested tic tac toe is like normal tic tac toe only each tile of the main board represents a game of its own",
	  	function() {
	  		console.log("Tip 2");
	  		$('#tooltip').animate({ top: '+=200' }, 1000);
	  		$("table").eq(1).css({"box-shadow": "0 0px 10px red"});
	  		$("table").eq(2).css({"box-shadow": "0 0px 10px green"});
	  		$("table").eq(3).css({"box-shadow": "0 0px 10px blue"});
	  		$("table").eq(4).css({"box-shadow": "0 0px 10px yellow"});
	  		$("table").eq(5).css({"box-shadow": "0 0px 10px white"});
	  		$("table").eq(6).css({"box-shadow": "0 0px 10px black"});
	  	}
	  ); },
	  function() { addToolTip( 
	  	"Win a sub game and you win that tile",
	  	function() {
	  		console.log("Tip 3");
	  		$("table").eq(1).css({"box-shadow": "0 0px 0px red"});
	  		$("table").eq(2).css({"box-shadow": "0 0px 0px green"});
	  		$("table").eq(3).css({"box-shadow": "0 0px 0px blue"});
	  		$("table").eq(4).css({"box-shadow": "0 0px 0px yellow"});
	  		$("table").eq(5).css({"box-shadow": "0 0px 0px white"});
	  		$("table").eq(6).css({"box-shadow": "0 0px 0px black"});
	  		$('#tooltip').animate({ top: '-=140', left: '-250' }, 1000);
	  		
	  		setTimeout(function() { $("td").eq(1).css({"box-shadow": "0 5px 10px red"}); }, 0500);
	  		setTimeout(function() { $("td").eq(5).css({"box-shadow": "0 5px 10px red"}); }, 1500);
	  		setTimeout(function() { $("td").eq(9).css({"box-shadow": "0 5px 10px red"}); }, 3000);
	  		setTimeout(function() { $("table").eq(1).css({"box-shadow": "0 0px 10px red"}); }, 4500);
	  	}
	  ); },
	  function() { addToolTip( 
	  	"Win three tiles in a line and you win the game",
	  	function() {
	  		console.log("Tip 4");
	  		$('#tooltip').animate({ left: '0' }, 1000);
	  		
	  		setTimeout(function() { $("td").eq(11).css({"box-shadow": "0 5px 10px red"}); }, 0500);
	  		setTimeout(function() { $("td").eq(15).css({"box-shadow": "0 5px 10px red"}); }, 1000);
	  		setTimeout(function() { $("td").eq(19).css({"box-shadow": "0 5px 10px red"}); }, 1500);
	  		setTimeout(function() { $("table").eq(2).css({"box-shadow": "0 0px 10px red"}); }, 2000);
	  		
	  		setTimeout(function() { $("td").eq(21).css({"box-shadow": "0 5px 10px red"}); }, 2500);
	  		setTimeout(function() { $("td").eq(25).css({"box-shadow": "0 5px 10px red"}); }, 3000);
	  		setTimeout(function() { $("td").eq(29).css({"box-shadow": "0 5px 10px red"}); }, 3500);
	  		setTimeout(function() { $("table").eq(3).css({"box-shadow": "0 0px 10px red"}); }, 4000);
	  		
	  		setTimeout(function() { $("table").eq(0).css({"box-shadow": "0 0px 10px red"}); }, 5000);
	  	}
	  ); },
	  function() { addToolTip( 
	  	"To begine with you can play in the any of the middle tiles outer squares, thses correlate to the outer tiles",
	  	function() {
	  		console.log("Tip 5");
	  		$("table").eq(1).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(1).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(5).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(9).css({"box-shadow": "0 0px 0px red"});
	  		$("table").eq(2).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(11).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(15).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(19).css({"box-shadow": "0 0px 0px red"});
	  		$("table").eq(2).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(21).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(25).css({"box-shadow": "0 0px 0px red"});
	  		$("td").eq(29).css({"box-shadow": "0 0px 0px red"});
	  		$("table").eq(3).css({"box-shadow": "0 0px 0px red"});
	  		$("table").eq(0).css({"box-shadow": "0 0px 0px red"});
	  		
	  		$('#tooltip').animate({ top: '+=150' }, 1000);
	  		$("td").eq(41).addClass("open");
	  		$("td").eq(42).addClass("open");
	  		$("td").eq(43).addClass("open");
	  		$("td").eq(44).addClass("open");
	  		$("td").eq(46).addClass("open");
	  		$("td").eq(47).addClass("open");
	  		$("td").eq(48).addClass("open");
	  		$("td").eq(49).addClass("open");
	  	}
	  ); },
	  function() { addToolTip( 
	  	"So chosing the top left square (green) will mean you opponent will next have to play in the top left tile (red)",
	  	function() {
	  		console.log("Tip 6");
	  		$('#tooltip').animate({ left: '-260', top: '+340' }, 1000);
	  		$("td").eq(41).css({"box-shadow": "inset 0 0px 10px green"});
	  		$("table").eq(1).css({"box-shadow": "0 5px 10px red"});
	  	}
	  ); },
	  function() { addToolTip( 
	  	"This rule applies anywhere one the board unless the tile you (or your opponent) are foced to move it is already won, then you can chose any open square",
	  	function() {
	  		console.log("Tip 6");
	  		$("td").eq(41).css({"box-shadow": "inset 0 0px 0px green"});
	  		$("table").eq(1).css({"box-shadow": "0 0px 0px red"});
	  	}
	  ); },
	  function() { addToolTip( 
	  	"You should be ready to play now! Try honing your skills agains't a bot, or challenge a real player to a match. Good luck!",
	  	function() {
	  		console.log("End of Tips");
	  		$('#tooltip').animate({ top: '230', left: '0', right: '0' }, 1000);
	  		$("#tooltip #nexttut").hide();
	  	}
	  ); },
	];
	function addToolTip(text, callback) {
		$( "#tooltip" ).show();
		$( "#tooltip p" ).html(text);
		callback();
	}
	$('body').ready( function() {
		$("#tooltip").draggable();
		$( "#tooltip #nexttut" ).click(function() { tipcount += 1; tips[tipcount](); });
		$( "#tooltip #endtut" ).click(function() { document.location.href = "index.jsp"; });
		tips[tipcount]();
	});
	
});