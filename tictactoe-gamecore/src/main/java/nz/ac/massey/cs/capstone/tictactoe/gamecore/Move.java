package nz.ac.massey.cs.capstone.tictactoe.gamecore;

import java.io.Serializable;

/**
 * Representation of a Move for the Nested TicTacToe game
 *
 * @author Colin Campbell
 * @author Tom Magson
 * @author Li Sui
 * 
 */
public class Move implements Serializable {

	/**
	 * Nested Game
	 */
	public final Integer nestedGame;

	/**
	 * Position
	 */
	public final Integer position;

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -103799444772201929L;

	/**
	 * Constructs a new Move
	 *
	 * @param nestedGame the nested game
	 * @param position the position
	 *
	 * @throws IllegalArgumentException if the values are outside the scope of the game
	 */
	public Move( final Integer nestedGame, final Integer position ) throws IllegalArgumentException {

		this.nestedGame = nestedGame;
		this.position = position;

		if ( nestedGame < 0 ) {

			throw new IllegalArgumentException( "Nested game below zero is not allowed" );
		}

		if ( position < 0 ) {

			throw new IllegalArgumentException( "Position below zero is not allowed" );
		}

		if ( nestedGame > 8 ) {

			throw new IllegalArgumentException( "Nested game above eight is not allowed" );
		}

		if ( position > 8 ) {

			throw new IllegalArgumentException( "Position above eight is not allowed" );
		}
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ( (nestedGame == null ) ? 0 : nestedGame.hashCode() );
		result = prime * result
				+  ( ( position == null ) ? 0 : position.hashCode() );

		return result;
	}

	@Override
	public boolean equals( final Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final Move other = ( Move ) obj;
		if ( nestedGame == null ) {
			if ( other.nestedGame != null ) {
				return false;
			}
		} else if ( ! nestedGame.equals( other.nestedGame ) ) {
			return false;
		}
		if ( position == null ) {
			if ( other.position != null ) {
				return false;
			}
		} else if ( !position.equals( other.position ) ) {
			return false;
		}
		return true;
	}
}
