package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.CreateGame;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Test cases for the CreateGame servlet
 *
 * @author Li Sui
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateGameTests {

	private Lobby lobby;

	private CreateGame servlet;

	private User user;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpSession session;

	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;

	@Before
	public void setUp() throws ServletException {

		lobby = new Lobby();
		user = new User();
		user.setIdentity( "12345" );

		when( context.getAttribute( Application.LOBBY ) ).thenReturn( lobby );
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession() ).thenReturn( session );

		servlet = new CreateGame();
		servlet.init( config );
	}

	/**
	 * Test that create game returns a json object with a gameId
	 */
	@Test
	public void createGameTest() throws Exception {

		final StringWriter input = new StringWriter();

		when( request.getSession().getAttribute("user")).thenReturn( user );
		when( request.getParameter( "isVisible") ).thenReturn( "true" );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doPost( request, response );

		final JsonObject result = new JsonParser().parse( input.toString() ).getAsJsonObject();

		verify( response ).setContentType( "application/json" );

		assertNotNull( "A game id should be returned", result.get( "gameId" ) );
	}

	/**
	 * Test that the public game is added to the lobby
	 */
	@Test
	public void createPublicGameInLobby() throws Exception {

		final StringWriter input = new StringWriter();

		when( request.getSession().getAttribute("user") ).thenReturn( user );
		when( request.getParameter( "isVisible" ) ).thenReturn( "true" );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doPost( request, response );

		final String gameId = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject()
			.get( "gameId" )
			.getAsString();

		assertTrue( "The game should be pending in the lobby", lobby.getPendingGames().containsKey( gameId ) );
	}
	
	/**
	 * Test that the private game is added to the lobby
	 */
	@Test
	public void createPrivateGameInLobby() throws Exception {

		final StringWriter input = new StringWriter();

		when( request.getSession().getAttribute("user")).thenReturn( user );
		when( request.getParameter( "isVisible") ).thenReturn( "false" );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doPost( request, response );

		final String gameId = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject()
			.get( "gameId" )
			.getAsString();

		assertTrue( "The game should be pending in the lobby", lobby.getPendingGames().containsKey( gameId ) );
	}
}
