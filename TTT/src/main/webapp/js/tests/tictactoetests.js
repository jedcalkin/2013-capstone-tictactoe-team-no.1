"use strict";
define(
    //change this module to what I need to test
    ['tictactoe','Sinon'],
    function(TTT,Sinon) {
	
        var run = function() {
		
			var uid = "TestUser";
			var gid = "TestGame";


            test('Test Initilization of TicTacToe object', function() {
				var tttobj = new TTT(gid,uid);
                equal(tttobj.gameId,gid,'TicTacToe GameId should equal TestGame but it is ' + tttobj.gameId );
            });

            
            test('onUpdate Test', function() {
                var tttobj = new TTT(gid,uid).onUpdate(function(game){
                    return true;
                });
                
                notEqual(tttobj.update({ nestedGame : 4, position : 3 }),null);
                
            });

        };
        return {run: run}
    }
);