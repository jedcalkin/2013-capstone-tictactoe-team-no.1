define( /** @lends Robot */ ['jquery', 'tictactoe'], function ( $, TicTacToe ) {

	Robot.prototype = new TicTacToe();

	/**
	 * @class Robot
	 * @extends TicTacToe
	 *
	 * @author Colin Campbell, Li Sui
	 *
	 * @example
	 * require(['robot'], function( Robot ) {
	 *
	 * 		var robot = new Robot( 'gameId', 'userId' )
	 * 			.onUpdate( function( game ) {
	 * 				console.log( game );
	 * 			});
	 * 
	 * 		robot.update( { nestedGame : 4, position 3 } );
	 * });
	 *
	 * @constructor
	 * @param {String} gameId - the gameId
	 */
	function Robot() {

		TicTacToe.apply( this, arguments );
	}

	/**
	 * Updates the game with a new move
	 *
	 * @param {Object} move - Move
	 * @param {Number} move.nestedGame - the nestedGame
	 * @param {Number} move.position - the position
	 */
	Robot.prototype.update = function( move ) {
				$.ajax( {
							type: 'PUT',
							url: 'PlayBot?' + $.param( { 'gameId' : this.gameId } ),
							contentType: "application/json",
							data: JSON.stringify( move )
						});

				return this;
			}

	return Robot;

	}
);