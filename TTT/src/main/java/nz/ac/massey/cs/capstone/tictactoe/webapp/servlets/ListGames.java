package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.google.gson.Gson;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

/**
 * Gets all the open visible games
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class ListGames extends HttpServlet {      

	/** Gson Object */
	private Gson gson;

	/** The lobby */
	private Lobby lobby;

	/** Game timeouts */
	private Map< Game, Long[] > timeouts;

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = 310917706475719141L;

	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		/* Get User */
		final User user = ( User ) request.getSession().getAttribute( Authentication.USER );

		/* Write Games as JSON */
		gson.toJson( Maps.filterValues( lobby.getPendingGames() , new Predicate<Game>() {

			@Override
			public boolean apply( final Game game ) {

				/* Filter out games user has created */
				if ( game.getCreatorId().equals( user.getIdentity() ) ) {

					return false;
				}

				/* Sanity Check */
				if ( timeouts.get( game ) != null ) {

					/* Filter out dead games */
					final Long age = System.currentTimeMillis() - timeouts.get( game )[ 0 ];
					return ! (  age > 10000 );
				}

				return true;
			}

		}), response.getWriter() );
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize gson */
		gson = new Gson();

		/* Initialize lobby */
		lobby = ( Lobby ) config.getServletContext().getAttribute( Application.LOBBY );

		/* Initialize timeouts */
		timeouts = ( Map<Game,Long[]> ) getServletContext().getAttribute( Application.TIMEOUTS );
	}
}
