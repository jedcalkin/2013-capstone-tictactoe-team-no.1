requirejs.config({
    paths : {
    	jquery : '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
    	i18n : '//cdn.jsdelivr.net/i18next/1.7.1/i18next.min'
	},
	shim: {
	    jquery : {
	        exports: "jQuery"
	    },
	    i18n : {
			exports: "i18n"
		}
	}
});

require( ['jquery', 'i18n', 'tictactoe', 'toast', 'menu'], function( $, i18n, TicTacToe, Toast ) {

	i18n.init( function( translate ) {
	
	var div = $('#open_games');
	var oldL = -1;
	var updatelist = function( games ) {
		count = 0
		for(i in games){count+=1;}
		if(count!=oldL){
			oldL = count;
			div.empty();
			for( var gameId in games ) {
				var game = games[gameId]
				if(game.isVisible==true){

					var heading = $('<div>');
					heading.attr('id', gameId );
					heading.attr('class', 'pending_game' );

					var button = $( '<button>' );
					button.attr( 'type', 'button' );
					button.attr('class','public_button');
				
				var buttonChangePublic = function( button ) {
					return function( user ) {
						button.text( user.displayName+' '+translate( 'S_GAME' ) );
						heading.append( button );
						
						heading.append( "<br>" );
						div.append( heading );
					};
				};

				$.get("GetUser", {"userId":game.creatorId }, buttonChangePublic( button ) );

				button.click( function( gameId ) {
					return function() {
						TicTacToe.joinGame(gameId,"public", function( game ) {
						window.location.href='game.jsp?gameId='+gameId
					});
					}
				}( gameId ) );

				}else{
					var heading = $('<div>');
					heading.attr('id', gameId );
					heading.attr('class', 'pending_game' );

					var button = $( '<button/>' );
					button.attr( 'type', 'button' );
					button.attr('class','private_game_button');
					var buttonChangePrivate = function( button ) {
					return function( user ) {
						button.text( user.displayName+' '+translate( 'S_GAME' ) );
						heading.append( button );
						heading.append( "<br>" );
						div.append( heading );
					};
				};
				$.get("GetUser", {"userId":game.creatorId }, buttonChangePrivate( button ) );
					button.click( function( gameId ) {

						return function() {
							var password=prompt( translate( 'SET_PASSWORD' ),translate( 'ENTER_PASSWORD' ) );
							 if(password){
									TicTacToe.joinGame(gameId,password, function( game ) {
										window.location.href='game.jsp?gameId='+gameId
									}, function() {
										Toast.makeText( translate( 'INCORRECT' ) , 2000 ).show();//TODO: change
									});
							 }
						}
					}( gameId ) );
					
				} 
			}
		}
	}
	TicTacToe.pendingGames( updatelist );
});
});