package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.stream.JsonWriter;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

/**
 * Servlet for Getting the aliveness of a Game
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class GetAliveness extends HttpServlet {

	/** The lobby */
	private Lobby lobby;

	/** Game timeouts */
	private Map< Game, Long[] > timeouts;

	/** Time To Live */
	private static final Long TTL = 10000L;

	/** Game ID Parameter */
	private static final String PARAM_GAMEID = "gameId";

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

	/** Serial Version UID */
	private static final long serialVersionUID = -6219585157680375786L;

	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Get Game */
		final Game game = lobby.getGameByGameId( request.getParameter( PARAM_GAMEID ) );

		/* Get User */
		final User currentUser = ( User ) request.getSession().getAttribute( Authentication.USER );

		/* Sanity Check */
		if ( !timeouts.containsKey( game ) ) {
			timeouts.put( game, new Long[2] );
		}

		/* Users time index */
		int index = ( currentUser.getIdentity().equals( game.getCreatorId() )) ? 0 : 1;

		/* Reset timeout */
		timeouts.get( game )[ index ] = System.currentTimeMillis();

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		/* JSON Output Stream */
		final JsonWriter out = new JsonWriter( response.getWriter() );

		/* Write Alive Key */
		out.beginObject()
			.name( "alive" );

		/* Sanity Check */
		if ( timeouts.get( game )[ index ^ 1 ] != null ) {

			/* Write Aliveness state */
			out.value( ! (Math.abs( timeouts.get(game)[0] - timeouts.get(game)[1] ) > TTL ) )
				.endObject();

			out.close();
			return;
		}

		/* Game is Alive */
		out.value( true );
		out.endObject();
		out.close();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init( ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize lobby */
		lobby = ( Lobby ) config.getServletContext().getAttribute( Application.LOBBY );

		/* Initialize timeouts */
		timeouts = ( Map<Game,Long[]> ) getServletContext().getAttribute( Application.TIMEOUTS );
	}
}
