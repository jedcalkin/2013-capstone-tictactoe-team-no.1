requirejs.config({
		paths : {
			jquery : '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
			i18n : '//cdn.jsdelivr.net/i18next/1.7.1/i18next.min',
		    purl : '//cdn.jsdelivr.net/jquery.url.parser/2.2.1/purl'
	},
	shim: {
			jquery : {
					exports: "jQuery"
			},
			i18n : {
				exports: "i18n"
			},
			purl : {
				deps: ['jquery']
			}	
	}
});

require( [ 'jquery', 'i18n', 'toast', 'purl', 'menu'], function( $, i18n, Toast ) {

	i18n.init( function( translate ) {

		var userId = $.url().param('userId');
		var profile = $('#updateprofile');
		var editiable = $('#editiable').val();

		$.get( "GetUser", { "userId" : userId }, function( user ) {

			var readMode = function() {

				var dom = $( '<fieldset/>' );
				var  me=$('#me');

				var labelName = $('<label/>');
				dom.append( labelName.text( translate('LABEL_NAME') ) );
				var name = $( '<b/>' );
				labelName.append( name );

				var labelEmail = $('<label/>')
				dom.append( labelEmail.text( translate('EMAIL') ) );
				var email = $( '<b/>' );
				email.attr("id", "user_email");
				labelEmail.append( email );

				var labelAge = $('<label/>');
				dom.append( labelAge.text( translate('AGE') ) );
				var age = $( '<b/>' );
				labelAge.append(age);

				var labelGender = $('<label/>');
				dom.append( labelGender.text( translate('GENDER') ) );
				var gender = $( '<b/>' );
				labelGender.append(gender);

				var labelLocation = $('<label/>');
				dom.append( labelLocation.text( translate('LOCATION') ) );
				var location = $( '<b/>' );
				labelLocation.append(location);

				var labelAboutMe = $('<label/>');
				dom.append( labelAboutMe.text( translate('ABOUT_ME') ) );
				var aboutMe = $( '<b/>' );
				labelAboutMe.append( aboutMe );

				var editButton = $( '<button/>' )
					.attr( 'type', 'button' )
					.text( translate('EDIT') );

				dom.append( editButton );

				return function() {

					name.text( user.displayName );
					email.text(user.email);
					age.text( user.age );
					gender.text( user.gender );
					location.text( user.location );
					aboutMe.text( user.aboutMe );
					
					me.text(user.displayName);
					
					editButton.css( 'display', editiable )
						.click( editMode );

					profile.empty();
					
					profile.append( dom );
				};
			}();

			var editMode = function() {

				var dom = $( '<fieldset/>' );
				
				dom.append( $('<label/>').text( translate('LABEL_NAME') ) );
				var inputDisplayName = $( '<input/>' );
				dom.append(inputDisplayName);
				
				dom.append( $('<label/>').text( translate('AGE') ) );
				var inputAge = $( '<input/>' );
				dom.append(inputAge);
				
				dom.append( $('<label/>').text( translate('GENDER' ) ) );
				var selectGender = $( '<select/>' );
				selectGender.append($('<option/>').text( translate('MALE' ) ));
				dom.append(selectGender);
				selectGender.append($('<option/>').text( translate('FEMALE' ) ));
				dom.append(selectGender);
				
				dom.append( $('<label/>').text( translate('LOCATION' ) ) );
				var inputLocation = $( '<input/>' );
				dom.append(inputLocation);
				
				dom.append( $('<label/>').text( translate('ABOUT_ME' ) ) );
				var inputAboutMe = $( '<input/>' );
				dom.append( inputAboutMe );

				buttonSave = $( '<button/>' )
					.attr( 'type', 'button' )
					.text( translate('SAVE' ) );

				dom.append( buttonSave );

				buttonCancel = $( '<button/>' )
					.attr( 'type', 'button' )
					.text( translate('CANCEL' ) );

				dom.append( buttonCancel );

				var doSave = function() {

					// Update User Object
					user.displayName=inputDisplayName.val();
					user.age = inputAge.val();
					user.gender = selectGender.val();
					user.location=inputLocation.val();
					user.aboutMe = inputAboutMe.val();
					

					$.ajax( { url: 'UpdateUser?' + $.param( user ), type: 'PUT' 
					}).done(function(){
						Toast.makeText( translate( 'SAVE_SUCCESSFUL' ), 1500 ).show();
						readMode();
					});
				};

				return function() {

					inputDisplayName.attr( 'value',user.displayName );
					inputAge.attr( 'value',user.age );
					selectGender.attr( 'value', user.gender );
					inputLocation.attr( 'value', user.location );
					inputAboutMe.attr( 'value',user.aboutMe );
					
					buttonSave.click( doSave );
					buttonCancel.click( readMode );
					profile.empty();
					profile.append( dom );
				};
			}();

			readMode();

		});	
	});

	
	
});