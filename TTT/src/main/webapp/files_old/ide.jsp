<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%		
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );	

%>
<%@ include file="page_head.include" %>
<div id="login_info">
	<img class="glossy image-wrap" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="user" />
	<%= resourceBundle.getString( "CURRENTLY_LOGIN" ) %>:<br>
	<b id="me">${user.email}</b><br>
	<b><a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank"><%= resourceBundle.getString( "PROFILE" ) %></a></b>
</div>
</header>

<%@ include file="page_menu.include" %>
<section>
<iframe src="Ace/index.html" width="100%" height="98%"></iframe>
</section>

<%@ include file="page_tail.include" %>
