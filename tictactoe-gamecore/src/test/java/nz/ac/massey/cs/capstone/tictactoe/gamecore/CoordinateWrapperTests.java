package nz.ac.massey.cs.capstone.tictactoe.gamecore;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 * 
 * @author Li Sui
 *
 */
public class CoordinateWrapperTests {
	
   private CoordinateWrapper coordinateWraper;
   
	@Test
	public void testGetCoordinate1() {
		coordinateWraper=new CoordinateWrapper(new Move(0,0));
		assertTrue(coordinateWraper.getCoordinate().getColumn()==0&&coordinateWraper.getCoordinate().getRow()==0);
	}

	@Test
	public void testGetCoordinate2() {
		
		coordinateWraper=new CoordinateWrapper(new Move(2,4));
		assertTrue(coordinateWraper.getNestedCoordinate().getColumn()==1&&coordinateWraper.getNestedCoordinate().getRow()==1);
	}

	@Test
	public void testGetCoordinate3() {
		
		coordinateWraper=new CoordinateWrapper(new Move(8,8));
		assertTrue(coordinateWraper.getCoordinate().getColumn()==2&&coordinateWraper.getCoordinate().getRow()==2);
	}
	
	@Test
	public void testGetMove() {
		
		final Move move =new Move(8,8);
		coordinateWraper=new CoordinateWrapper(move);
		
		assertTrue(coordinateWraper.getMove().equals(move));
	}

}
