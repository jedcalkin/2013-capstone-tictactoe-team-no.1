package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.commons.compiler.CompileException;

import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.j256.ormlite.dao.Dao;

/**
 * @author Li Sui
 */
public class UpdateBot extends HttpServlet {
	      
	/** Gson Object */
	private Gson gson;

	/** The BotCode Data Access Object */
	private Dao<BotCode, Integer> botDao;

	/** The User Data Access Object */
	private Dao<User, String> userDao;

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

    /**
	 * Serial Version
	 */
	private static final long serialVersionUID = 4748170065183338871L;

	@Override
	protected void doPut( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		/* Get User */
		final User user = ( User ) request.getSession().getAttribute( Authentication.USER );

		/* Get BotCode */
		final BotCode botCode = gson
				.fromJson(
					new JsonReader(
						request.getReader()
					),  BotCode.class
				);

		/* Ensure Correct User is Set */
		botCode.setUser( user );

		try {

			/* Persist BotCode */
			botCode.compile();
			botDao.createOrUpdate( botCode );
			userDao.refresh( user );

			/* Reply with BotCode */
			gson.toJson( new BotCodeWrapper( botCode ), response.getWriter() );

		} catch ( SQLException e ) {

			throw new IOException( e );

		} catch ( ClassNotFoundException e ) {

			throw new ServletException( e );

		} catch ( CompileException e ) {

			throw new ServletException( e );
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init( final ServletConfig config ) throws ServletException {

		super.init ( config );

		/* Initialize gson */
		gson = new Gson();

		/* Initialize User DAO */
		botDao = ( Dao<BotCode, Integer> ) config.getServletContext().getAttribute( Application.BOT_DAO );

		/* Initialize User DAO */
		userDao = ( Dao<User, String> ) config.getServletContext().getAttribute( Application.USER_DAO );
	}

	/**
	 * BotCodeWrapper defines the object returned
	 *
	 * @author Colin Campbell
	 */
	private class BotCodeWrapper {

		@SuppressWarnings("unused")
		public final Integer identity;

		@SuppressWarnings("unused")
		public final String name;

		@SuppressWarnings("unused")
		public final String sourceCode;

		@SuppressWarnings("unused")
		public final UserWrapper user;

		@SuppressWarnings("unused")
		public final Boolean verified;

		public BotCodeWrapper( final BotCode botCode ) {

			identity = botCode.getIdentity();
			name = botCode.getName();
			sourceCode = botCode.getSourceCode();
			user = new UserWrapper( botCode.getUser() );
			verified = botCode.getVerified();
		}

		private class UserWrapper {

			@SuppressWarnings("unused")
			public final String identity;

			public UserWrapper( final User user ) {

				identity = user.getIdentity();
			}
		}
	}
}
