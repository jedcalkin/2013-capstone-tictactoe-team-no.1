<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import='com.j256.ormlite.dao.Dao' %>
<%@ page import='nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application' %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );
@SuppressWarnings("unchecked")
final Dao<User, String> userDao = ( Dao<User, String> ) getServletContext().getAttribute( Application.USER_DAO );
userDao.refresh( user );
%>
<%@ include file="page_head_tmp.include" %>
	<a href="profile.jsp?userId=<%=user.getIdentity()%>" title="Profile"><img id="user_image" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="gravatar" /></a>
	<a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank" title="Profile"><img id="profile_link" src="./Media/cog.png" alt="profile_link" /></a>
	<input type="hidden" id ="selenium_Lobby" value="Lobby"/>
</header>

<script data-main="js/lobby" src="js/require.js"></script>

<%@ include file="page_menu_tmp.include" %>
<section>
	<div id="player1" class="current-player">
		<b id="selenium_userName">${user.displayName}</b><br>
		<a href="https://en.gravatar.com/" title="Gravatar">
			<img src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=256" alt="gravatar" />
		</a><br>
		<b><%= resourceBundle.getString( "WINS" ) %>:&ensp;</b>${user.numWins }<br>
		<b><%= resourceBundle.getString( "DRAWS" ) %>:&ensp;</b>${user.numDraws}<br>
		<b><%= resourceBundle.getString( "LOSSES" ) %>:&ensp;</b>${user.numLoses}<br>
	</div>

	<div id="pending_games">
		<h2><%= resourceBundle.getString( "PENDING_GAMES" ) %></h2>
		<hr>
		<div id="open_games">
			<img id="spinner" src="./Media/loading.gif" alt="Loading...">
		</div>
	</div>
</section>
<%@ include file="page_tail_tmp.include" %>
