package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;

import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

/**
 *@author Li Sui
 */
public class GetBots extends HttpServlet {
       
    /**
	 * version
	 */
	private static final long serialVersionUID = 7895727111261834159L;

	private static final Collection<BotCode> EMPTY_LIST = new ImmutableList.Builder<BotCode>().build();

	/** Gson Object */
	private Gson gson;

	/**
     * list of robots 
     */
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		final User currentUser=( User ) request.getSession().getAttribute( Authentication.USER );
		
		response.setContentType( "application/json" );

		Collection<BotCode> botCodes = currentUser.getBots();

		/* Sanity Check */
		if ( botCodes == null ) {

			botCodes = EMPTY_LIST;
		}

		gson.toJson(
			Collections2.transform(
					botCodes, new Function<BotCode, BotCodeWrapper>() {

						@Override
						public BotCodeWrapper apply( final BotCode botCode ) {

							return new BotCodeWrapper( botCode );
						}

					}),
			response.getWriter()
		);
	}

	@Override
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize gson */
		gson = new Gson();
	}

	/**
	 * BotCodeWrapper defines the object returned
	 *
	 * @author Colin Campbell
	 */
	private class BotCodeWrapper {

		@SuppressWarnings("unused")
		public final Integer identity;

		@SuppressWarnings("unused")
		public final String name;

		@SuppressWarnings("unused")
		public final String sourceCode;

		@SuppressWarnings("unused")
		public final UserWrapper user;

		@SuppressWarnings("unused")
		public final Boolean verified;

		public BotCodeWrapper( final BotCode botCode ) {

			identity = botCode.getIdentity();
			name = botCode.getName();
			sourceCode = botCode.getSourceCode();
			user = new UserWrapper( botCode.getUser() );
			verified = botCode.getVerified();
		}

		private class UserWrapper {

			@SuppressWarnings("unused")
			public final String identity;

			public UserWrapper( final User user ) {

				identity = user.getIdentity();
			}
		}
	}
}
