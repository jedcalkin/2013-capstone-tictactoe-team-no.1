package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.commons.compiler.CompileException;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.dao.Dao;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.GameState;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;
import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.exceptions.InvalidMoveException;
import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Session;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

/**
 * Servlet for playing a bot
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class PlayBot extends HttpServlet {

	/** Gson Object */
	private Gson gson;

	/** The lobby */
	private Lobby lobby;
	
	/** The BotCode Data Access Object */
	private Dao<BotCode, Integer> botCodeDao;

	/** Game ID Parameter */
	private static final String PARAM_GAMEID = "gameId";
	
	/** bot ID Parameter */
	private static final String PARAM_BOTID="botId";

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = 6432144524156749960L;

	@SuppressWarnings("unchecked")
	@Override
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize gson */
		gson = new Gson();

		/* Initialize lobby */
		lobby = ( Lobby ) getServletContext().getAttribute( Application.LOBBY );
		
		/* Initialize bot code data access object */
		botCodeDao = ( Dao<BotCode, Integer> ) getServletContext().getAttribute( Application.BOT_DAO );

		/* Initialize robot */
		//robot = ( Robot ) getServletContext().getAttribute( Application.BOT );
	}

	@SuppressWarnings({ "unchecked", "resource" })
	@Override
	protected void doPost( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

        final Integer botId = Integer.parseInt( request.getParameter( PARAM_BOTID ) );

		/* JSON Output Stream */
		final JsonWriter out = new JsonWriter( response.getWriter() );
		
		Map<Integer,Robot> robots=(Map<Integer,Robot>) request.getSession().getAttribute(Session.LISTBOTS);

		try {

			if(  !robots.containsKey( botId ) ) {
	
				BotCode botCode=botCodeDao.queryForId(botId);
				if( !botCode.isCompiled() ){
					
					botCode.compile();
				}
				
				robots.put(botId, botCode.newBot());
	
			}

			/* Get User */
			final User user = ( User ) request.getSession().getAttribute( Authentication.USER );
	 
			/* Create Game */
			final String gameId = lobby.createGame( user.getIdentity(), false, "123" );

			/* Join bot to Game */
			lobby.joinGame( gameId, botId.toString() );

			/* Write gameID as JSON */
			out.beginObject()
				.name( "gameId" )
				.value( gameId )
				.endObject();
			out.close();

		} catch ( final SQLException e ) {

			throw new ServletException( e );

		} catch ( final ClassNotFoundException e ) {

			throw new ServletException( e );

		} catch (final InstantiationException e) {

			throw new ServletException( e );

		} catch ( final IllegalAccessException e ) {

			throw new ServletException( e );
			
		} catch (final CompileException e) {
			
			throw new ServletException( e );
			
		} catch (final BotCode.NonCompiledException e) {
			
			throw new ServletException( e );
			
		} catch (final BotCode.NonVerifiedException e) {
			
			throw new ServletException( e );
		} 
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doPut( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		Map<Integer,Robot> robots=(Map<Integer,Robot>) request.getSession().getAttribute(Session.LISTBOTS);
	
		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		/* Get input as JSON */
		final JsonReader input = new JsonReader( request.getReader() );

		/* Get input as Move */
		final Move move = ( new Gson() ).fromJson( input, Move.class );

		/* Get GameID */
		final String gameId = request.getParameter( PARAM_GAMEID );
		
		/* Close input */
		input.close();

		try {

			/* Update game */
			if ( lobby.updateGame( gameId, move ) ) {

					/* If game is incomplete */
					if( lobby.getGameByGameId(gameId).getState().equals( GameState.INCOMPLETE ) ) {
						
						final Robot robot=robots.get(Integer.parseInt(lobby.getGameByGameId(gameId).getOpponentId()));

						/* Get bots move */
						final Move botMove = robot.nextPosition( lobby.getGameByGameId( gameId ) );

						try {

							/* Update game */
							lobby.updateGame( gameId, botMove );

						} catch ( InvalidMoveException e ) {

							throw new ServletException( e );
						}

						/* Write move as JSON */
						gson.toJson( botMove, response.getWriter() );
					}
			}else{
				
				response.sendError( HttpServletResponse.SC_GONE );
			}

		} catch ( InvalidMoveException e ) {

			response.sendError( HttpServletResponse.SC_BAD_REQUEST );
		}

	}
}
