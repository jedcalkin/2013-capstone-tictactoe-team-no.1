package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.j256.ormlite.dao.Dao;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.GameState;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.exceptions.InvalidMoveException;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;

/**
 * Servlet for updating a game
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class UpdateGame extends HttpServlet {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -4274880167439385979L;

	/** The lobby */
	private Lobby lobby;

	/** The User Data Access Object */
	private Dao<User, String> userDao;

	/** Game ID Parameter */
	private static final String PARAM_GAMEID = "gameId";

	@Override
	protected void doPut( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Get input as JSON */
		final JsonReader input = new JsonReader( request.getReader() );

		/* Get input as Move */
		final Move move = ( new Gson() ).fromJson( input, Move.class );

		/* Get GameID */
		final String gameId = request.getParameter( PARAM_GAMEID );

		/* Get Game */
		final Game game = lobby.getGameByGameId( gameId );

		final User currentUser = ( User ) request.getSession().getAttribute( Authentication.USER );

		/* Close input */
		input.close();

		try {

			/* Update Game */
			if ( ! lobby.updateGame( gameId, move ) ) {

				response.sendError( HttpServletResponse.SC_GONE );
			}

		} catch ( InvalidMoveException e ) {

			response.sendError( HttpServletResponse.SC_BAD_REQUEST );
		}
		if((game!=null)&&!game.getState().equals(GameState.INCOMPLETE)){
			
			try {
				final User user = userDao.queryForId( currentUser.getIdentity() );
				final User otherUser;
				if(currentUser.getIdentity().equals(game.getCreatorId())){
					
					 otherUser = userDao.queryForId( game.getOpponentId() );
					
				}else{
					
					 otherUser = userDao.queryForId( game.getCreatorId() );
					 
				}
				
				if(game.getState().equals(GameState.WIN)){
					user.setNumWins( user.getNumWins() + 1 );
					otherUser.setNumLoses( otherUser.getNumLoses() + 1 );				
				}
				if(game.getState().equals(GameState.DRAW)){
					
					user.setNumDraws( user.getNumDraws() + 1 );
					otherUser.setNumDraws( otherUser.getNumDraws() + 1 );
				}			

				userDao.update(otherUser);
				userDao.update(user);
				
				
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
	
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize lobby */
		lobby = ( Lobby ) getServletContext().getAttribute( Application.LOBBY );
		
		/* Initialize User DAO */
		userDao = ( Dao<User, String> ) config.getServletContext().getAttribute( Application.USER_DAO );
		
	}
}
