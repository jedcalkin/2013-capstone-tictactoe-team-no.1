package nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.apache.commons.codec.digest.DigestUtils;
import org.openid4java.OpenIDException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.ParameterList;
import org.openid4java.message.ax.FetchRequest;

import com.j256.ormlite.dao.Dao;

/**
 * Servlet Filter implementation class OpenID
 */
public class OpenID implements Authentication {

	/**
	 * Consumer Manager
	 */
	private ConsumerManager manager;

	/**
	 * User Data Access Object
	 */
	private Dao<User, String> userDao;

	/**
	 * Google End Point URL
	 */
	private static final String GOOGLE_ENDPOINT="https://www.google.com/accounts/o8/id";

	/**
	 * OpenID Identity
	 */
	private static final String IDENTITIY = "openid.identity";

	/**
	 * OpenID Email
	 */
	private static final String EMAIL = "openid.ext1.value.email";

	/**
	 * OpenID First Name
	 */
	private static final String FIRST_NAME = "openid.ext1.value.firstName";

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {

		/* Sanity Check */
		if ( request instanceof HttpServletRequest && response instanceof HttpServletResponse ) {

			HttpServletRequest httpRequest = ( HttpServletRequest ) request;
			HttpServletResponse httpResponse = ( HttpServletResponse ) response;

			/* OpenID redirect */
			if ( httpRequest.getParameter( IDENTITIY ) != null ) {

				/* OpenID Identifier */
				Identifier identifier = verifyResponse( httpRequest );

				/* Check verification */
				if ( identifier == null ) {

					httpResponse.sendError( HttpServletResponse.SC_BAD_REQUEST );
					return;
				}

				/* User Details */
				String identity = DigestUtils.md5Hex(httpRequest.getParameter( IDENTITIY ));
				String email = httpRequest.getParameter( EMAIL );
				String firstName = httpRequest.getParameter( FIRST_NAME );

				try {

					/* Query for user */
					User user = userDao.queryForId( identity );

					/* Create user */
					if ( user == null ) {

						user = new User();
						user.setIdentity( identity );
						user.setDisplayName( firstName );
						user.setEmail( email );
						user.setAboutMe( "none" );
						user.setAge( "none" );
						user.setGender( "none" );
						user.setLocation("none");
						user.setNumDraws(0);
						user.setNumLoses(0);
						user.setNumWins(0);


						userDao.create( user );
					}

					httpRequest.getSession().setAttribute( Authentication.USER, user );
					httpResponse.sendRedirect( httpResponse.encodeURL( httpRequest.getRequestURL().toString() ) );
					return;

				} catch ( SQLException e ) {

					throw new IOException( e );
				}
			}

			/* User Not Logged in */
			else if ( httpRequest.getSession().getAttribute( Authentication.USER ) == null ) {

				authRequest( httpRequest, httpResponse );
				httpResponse.flushBuffer();
				return;
			}
		}

		chain.doFilter( request, response );
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init( FilterConfig fConfig ) throws ServletException {

		manager = new ConsumerManager();
		userDao = ( Dao<User, String> ) fConfig.getServletContext().getAttribute( Application.USER_DAO );
	}

	@Override
	public void destroy() {

		manager = null;
		userDao = null;
	}

	private String authRequest( final HttpServletRequest httpReq, final HttpServletResponse httpResp ) throws IOException {

		try {
				
				// configure the return_to URL where your application will receive
				// the authentication responses from the OpenID provider
				
				final String returnToUrl = httpReq.getRequestURL().toString();
				//System.out.println(returnToUrl);
			 
				// --- Forward proxy setup (only if needed) ---
				// ProxyProperties proxyProps = new ProxyProperties();
				// proxyProps.setProxyName("proxy.example.com");
				// proxyProps.setProxyPort(8080);
				// HttpClientFactory.setProxyProperties(proxyProps);
	 
				// perform discovery on the user-supplied identifier
				final List<?> discoveries = manager.discover(GOOGLE_ENDPOINT);
			  
				// attempt to associate with the OpenID provider
				// and retrieve one service endpoint for authentication
				final DiscoveryInformation discovered = manager.associate(discoveries);
			   
				// store the discovery information in the user's session
				httpReq.getSession().setAttribute("openid", discovered);
				  
				// obtain a AuthRequest message to be sent to the OpenID provider
				final AuthRequest authReq = manager.authenticate(discovered, returnToUrl);
	 
				final FetchRequest fetch = FetchRequest.createFetchRequest();
				
					fetch.addAttribute("email",
							"http://axschema.org/contact/email", true);			  
					fetch.addAttribute("firstName",
							"http://axschema.org/namePerson/first", true);
					fetch.addAttribute("lastName",
							"http://axschema.org/namePerson/last", true);					 

	 
				// attach the extension to the authentication request
				authReq.addExtension(fetch);
			  
				httpResp.sendRedirect(authReq.getDestinationUrl(true));
	 
			} catch (OpenIDException e) {
				e.printStackTrace();
			}
	 
			return null;
		}

		private Identifier verifyResponse(final HttpServletRequest httpReq) {
			try {
				// extract the parameters from the authentication response
				// (which comes in as a HTTP request from the OpenID provider)
				final ParameterList response = new ParameterList(
						httpReq.getParameterMap());
	 
				// retrieve the previously stored discovery information
				final DiscoveryInformation discovered = (DiscoveryInformation) httpReq
						.getSession().getAttribute("openid");
			  
				// extract the receiving URL from the HTTP request
				final StringBuffer receivingURL = httpReq.getRequestURL();
			   
				final String queryString = httpReq.getQueryString();
				if (queryString != null && queryString.length() > 0){
					
				   receivingURL.append("?").append(httpReq.getQueryString());
				   
				}
				
				// verify the response; ConsumerManager needs to be the same
				// (static) instance used to place the authentication request
			   final  VerificationResult verification = manager.verify(
						receivingURL.toString(), response, discovered);
			   
				// examine the verification result and extract the verified
				// identifier
			   final Identifier verified = verification.getVerifiedId();
			   
				if (verified != null) {
					
					return verified; // success
					
				}
			} catch (OpenIDException e) {
				// present error to the user
			}
	 
			return null;
		}
}
