package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.Logout;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * Test cases for the Logout servlet
 *
 * @author Colin Campbell
 */
@RunWith(MockitoJUnitRunner.class)
public class LogoutTests {

	private Logout servlet;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private HttpSession session;

	@Before
	public void setUp() throws ServletException{

		servlet = new Logout();
		when( request.getSession() ).thenReturn( session );
	}

	/**
	 * Test that Logout invalidates the session
	 */
	@Test
	public void testInvalidate() throws Exception {

		servlet.doGet( request, response );

		verify( session ).invalidate();
	}

	/**
	 * Test that Logout redirects to the home page
	 */
	@Test
	public void testRedirection() throws Exception {

		servlet.doGet( request, response );

		verify( response ).sendRedirect( "." );
	}
}
