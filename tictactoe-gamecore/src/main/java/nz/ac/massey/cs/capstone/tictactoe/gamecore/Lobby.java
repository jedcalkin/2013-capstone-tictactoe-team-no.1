package nz.ac.massey.cs.capstone.tictactoe.gamecore;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.exceptions.InvalidMoveException;

/**
 * Lobby
 *
 * @author Colin Campbell
 * @author Li Sui
 */
public class Lobby {

	/**
	 * Gamers
	 */
	private final Map<String, String> gamers = new ConcurrentHashMap<String,String>();

	/**
	 * Games
	 */
	private final Map<String, Game> games = new ConcurrentHashMap<String,Game>();

	/**
	 * Constructs a new Lobby
	 */
	public Lobby() {

		// Do nothing
	}

	/**
	 * Creates a new Game with a specified creatorId and visibility
	 *
	 * @param creatorId the creators id
	 * @param isVisible if the game is visible
	 *
	 * @return gameId the game id
	 */
	public String createGame( final String creatorId, final Boolean isVisible,final String password ) {
		
		final String gameId = UUID.randomUUID().toString();
		
		if( isVisible ){
			
			final Game game = new Game( creatorId, isVisible,"public" );

			gamers.put(  creatorId, gameId );
			games.put( gameId, game );

			return gameId;
		}else{
			
			final Game game = new Game( creatorId, isVisible,password );
			
			gamers.put(  creatorId, gameId );
			games.put( gameId, game );

			return gameId;
		}
		
	}

	/**
	 * Joins a user to a game
	 *
	 * @param gameId the game to join
	 * @param gamerTag the user joining the game
	 * 
	 * @return true if the user joined the game
	 */
	public boolean joinGame( final String gameId, final String gamerTag ) {

		/* Get the game */
		final Game game = games.get( gameId );

		if ( game != null ) {
			synchronized( game ) {
					if ( game.getIsOpen() ) {
						gamers.put(gamerTag, gameId);
						game.setOpponentId( gamerTag );
						game.setIsOpen( false );
						return true;
				}
			}
		}

		return false;
	}
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public Game removeGame(final String userId){
		
		if(gamers.containsKey(userId)){
			
			final String gameId=gamers.get(userId);
			final Game game=getGameByGameId(gameId);
			
			gamers.remove(userId);
			games.remove(gameId);
			
			return game;
			
		}else{
			
			return null;
			
		}
	   
    }


	/**
	 * Updates a Game by adding a move
	 *
	 * @param gamerTag the user updating the game
	 * @param move the move to add
	 *
	 * @return true if the game was updated
	 *
	 * @throws InvalidMoveException if the move has already been performed
	 */
	public boolean updateGame( final String gameId, final Move move ) throws InvalidMoveException {


		if ( gameId == null || !games.containsKey( gameId ) ) {
			return false;
		}

		final Game game = games.get( gameId );

		synchronized( game ) {

			if ( game != null ) {

				game.addMove( move );
				return true;
			}
		}

		return false;
	}

	/**
	 * Gets a game by gameId
	 *
	 * @param gameId the game to get
	 *
	 * @return the game or null if a game cannot be found
	 */
	public Game getGameByGameId( final String gameId ) {

		return games.get( gameId );
	}

	/**
	 * Gets a game by userId
	 *
	 * @param userId the userId
	 *
	 * @return the game or null if a game cannot be found
	 */
	public Game getGameByUser( final String userId ) {

		final String gameId = gamers.get( userId );

		return getGameByGameId( gameId );
	}

	/**
	 * Gets the pending games
	 *
	 * @return the pending games
	 */
	 public Map<String, Game> getPendingGames() {

         return Maps.filterValues( games, new Predicate<Game>() {

			@Override
			public boolean apply( final Game game ) {

				return game.getIsOpen();
			}
         } );
	 }
}
