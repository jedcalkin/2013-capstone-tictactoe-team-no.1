define( ['jquery'], function( $ ) {

	var body = $( 'body' );

	return {

		makeText : function( message, duration ) {

			var toast = $( '<div/>' )
				.attr( 'class', 'toast' )
				.css( 'position', 'fixed' )
				.text( message )
				.click( function() {
					toast.remove();
				});

			return {

				show : function() {

					body.append( toast );

					setTimeout( function() {

						toast.fadeOut( duration, function() {
							toast.remove();
						} );

					}, duration );
				}
			};
		}
	};
});
