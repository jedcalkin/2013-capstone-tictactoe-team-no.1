package nz.ac.massey.cs.capstone.tictactoe.gamecore;

/**
 * Describes the Player Type
 *
 * @author Colin Campbell
 * @author Tom Magson
 * @author Li Sui
 *
 */
public enum PlayerType {

	CROSS, NOUGHT, EMPTY
}
