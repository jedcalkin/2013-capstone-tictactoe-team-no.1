/**
 * 
 */
package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author Li Sui
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class BrowserFilterTests {

	/**
	 * Filter Under test
	 */
	private BrowserFilter filter;
	
	@Mock
	private FilterChain chain;

	/**
	 * Mock HTTP Request
	 */
	@Mock
	private HttpServletRequest request;

	/**
	 * Mock HTTPResponse
	 */
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private  ServletContext context;
	
	@Mock
	private FilterConfig fConfig;
	
	@Mock
	private RequestDispatcher rd;
	
		
	@Before
	public void setUp() throws Exception {
		
		filter=new BrowserFilter();
		
		when(fConfig.getServletContext()).thenReturn( context );
		
	}

	@Test
	public void testGoodBrowsers() throws Exception {
		
		when(fConfig.getInitParameter("badBrowser")).thenReturn("browsers.jsp");
		when (request.getRequestDispatcher("browsers.jsp")).thenReturn(rd);
		when (request.getHeader("User-Agent")).thenReturn("Chrome");
		
		filter.init(fConfig);
		filter.doFilter( request, response, chain );
		
		verify(chain).doFilter(request, response);
	}	
	
	@Test
	public void testBadBrowsers() throws Exception{
		
		when(fConfig.getInitParameter("badBrowser")).thenReturn("browsers.jsp");
		when (request.getRequestDispatcher("browsers.jsp")).thenReturn(rd);
		when (request.getHeader("User-Agent")).thenReturn("IE");
		
		filter.init(fConfig);
		filter.doFilter( request, response, chain );
		
		verify(rd).forward(request, response);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIllegalArgument() throws Exception{
		
		when(fConfig.getInitParameter("badBrowser")).thenReturn(null);
		when (request.getRequestDispatcher("browsers.jsp")).thenReturn(rd);
		when (request.getHeader("User-Agent")).thenReturn("Chrome");
		
		filter.init(fConfig);
		filter.doFilter( request, response, chain );
	}

}
