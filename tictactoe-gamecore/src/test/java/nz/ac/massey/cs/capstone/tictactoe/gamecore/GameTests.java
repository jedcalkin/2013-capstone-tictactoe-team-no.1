package nz.ac.massey.cs.capstone.tictactoe.gamecore;

import static org.junit.Assert.*;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.exceptions.InvalidMoveException;

import org.junit.Before;
import org.junit.Test;

/**
 * Test cases for the Game class
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class GameTests {
	
	private Game game;

	@Before
	public void setUp() throws Exception {

		game = new Game();
	}

	/**
	 * Test that adding a move is added to moves
	 */
	@Test
	public void testAddMove() throws Exception {

		final Move move = new Move( 4, 3);

		game.addMove( move );

		assertTrue( "Moves should contain the move ( 4, 3 )", game.getMoves().contains( new Move( 4, 3) ) );
	}

	/**
	 * Test for the get state method
	 */
	@Test
	public void testGetState() {

		assertEquals( "The state should be INCOMPLETE", GameState.INCOMPLETE, game.getState() );
	}

	/**
	 * Test for the set state method
	 */
	@Test
	public void testSetStateDraw() {

		game.setState( GameState.DRAW );

		assertEquals( "The state should be DRAW", GameState.DRAW, game.getState() );
	}

	/**
	 * Test for the set state method
	 */
	@Test
	public void testSetStateWin() {

		game.setState( GameState.WIN );

		assertEquals( "The state should be WIN", GameState.WIN, game.getState() );
	}

	/**
	 * Test for the set state method
	 */
	@Test
	public void testSetStateIncomplete() {

		game.setState( GameState.INCOMPLETE );

		assertEquals( "The state should be INCOMPLETE", GameState.INCOMPLETE, game.getState() );
	}

	/**
	 * Test for the get is open method
	 */
	@Test
	public void testGetIsOpen() {

		assertTrue( "The game should be open", game.getIsOpen() );
	}

	@Test
	public void testSetIsOpen() {

		game.setIsOpen( false );

		assertFalse( "The game should be closed", game.getIsOpen() );
	}
	

	/**
	 * Test for the get is visible method
	 */
	@Test
	public void testGetPublicGame(){

		assertTrue( "The game should be public", game.getIsVisible() );
	}/**
	 * Test for the get is visible method
	 */
	@Test
	public void testGetPrivateGame(){
		
		Game privatGame=new Game("Li",false,"123");

		assertFalse( "The game should be private", privatGame.getIsVisible() );
	}
	/**
	 * 
	 */
	@Test
	public void testGetPassword(){
		
		Game privatGame=new Game("Li",false,"123");

		assertTrue( "The game should be private", privatGame.getPassword().equals("123") );
	}

	/**
	 * Test for the get opponent id method
	 */
	@Test
	public void testGetOpponentId() {

		assertNull( "Opponent should not be set", game.getOpponentId() );
	}

	/**
	 * Test for the set opponent id method
	 */
	@Test
	public void testSetOppoenetId() {

		game.setOpponentId( "opponentId" );

        assertEquals( "OppenentId should be opponentId", "opponentId", game.getOpponentId() );
	}

	/**
	 * Test for the get creator id method
	 */
	@Test
	public void testGetCreatorId() {

		assertNull( "CreatorId should not be set", game.getCreatorId() );
	}

	/**
	 * Test for the set creator id method
	 */
	@Test
	public void testSetCreatorId() {
		
		 game.setCreatorId( "creatorId" );

         assertEquals( "CreatorId should be creatorId", "creatorId", game.getCreatorId() );
	}

	/**
	 * Test that the move has been performed on the nested board
	 */
	@Test
	public void testAddMoveGetNestedBoard() throws Exception {

		game.addMove( new Move( 4, 3 ) );

		assertEquals( "(1, 0) of nested board 4 should be CROSS", PlayerType.CROSS, game.getNestedBoard( 4 )[ 1 ][ 0] );
	}

	/**
	 * Test for reference leak on board
	 */
	@Test
	public void testGetBoard() {

		final PlayerType[][] board = game.getBoard();

		board[ 0 ][ 0 ] = PlayerType.NOUGHT;

		assertNull( "Position ( 0, 0 ) should not be set", game.getBoard()[ 0 ][ 0 ] );
	}

	/**
	 * Test for reference leak on nested board
	 */
	@Test
	public void testGetNestedBoard() {

		final PlayerType[][] nestedBoard = game.getNestedBoard( 2 );

		nestedBoard[ 2 ][ 1 ] = PlayerType.CROSS;

		assertNull( "Position ( 2, 1 ) should not be set", game.getNestedBoard( 2 )[ 2 ][ 1 ] );
	}

	/**
	 * Test that creator id is set via the constructor
	 */
	@Test
	public void testGetCreatorIdConstructor() {

		 final Game game = new Game( "creatorId", false,"123" );

         assertEquals( "CreatorId should be creatorId", game.getCreatorId(), game.getCreatorId() );
	}

	/**
	 * Test that isVisible is set via the constcutor
	 */
	@Test
	public void testGetIsVisibleConstructor() {

		final Game game = new Game( "creatorId", false ,"123");

        assertEquals( "Visibilty should be false", game.getIsVisible(), game.getIsVisible() );
	}

	@Test( expected = InvalidMoveException.class )
	public void testAddMoveIllegalMove() throws Exception {

		game.addMove( new Move( 4, 6 ) );
		game.addMove( new Move( 8, 6 ) );
	}
	
	@Test( expected = InvalidMoveException.class )
	public void testAddFirstInvaildMove() throws Exception {

		game.addMove( new Move( 5, 6 ) );
		
	}
	
	@Test( expected = InvalidMoveException.class )
	public void testAddSameMove() throws Exception {

		game.addMove( new Move( 4, 6 ) );
		game.addMove( new Move( 4, 6 ) );
	}
	
	@Test( expected = InvalidMoveException.class )
	public void testFirstInvaildMove() throws Exception {

		game.addMove( new Move( 4, 4 ) );
	}

	@Test
	public void testAddMoveTwice() throws Exception {

		game.addMove( new Move( 4, 2 ) );
		game.addMove( new Move( 2, 4 ) );

		final PlayerType[][] board = game.getNestedBoard( 2 );
		assertEquals( "Position should be NOUGHT", PlayerType.NOUGHT, board[ 1 ][ 1 ] );
	}
	
	@Test( expected = InvalidMoveException.class )
	public void testAddInvaildMove() throws Exception{
		
		game.addMove(new Move(4,0));
		game.addMove(new Move(0,4));
		game.addMove(new Move(4,1));
		game.addMove(new Move(1,4));
		game.addMove(new Move(4,2));
		game.addMove(new Move(2,4));
		game.addMove(new Move(4,3));
		
	}
	
	@Test
	public void testGameIncomplete() throws Exception{
		
		game.addMove( new Move( 4, 2 ) );
		game.addMove( new Move( 2, 4 ) );
		
		assertEquals( "GameState should be incomplete", GameState.INCOMPLETE, game.getState() );
	}
	
	@Test
	public void testNestGameWin() throws Exception{
		
		game.addMove( new Move( 4, 0 ) );
		game.addMove( new Move( 0, 4 ) );
		game.addMove( new Move( 4, 1 ) );
		game.addMove( new Move( 1, 4 ) );
		game.addMove( new Move( 4, 2 ) );
		
		
		
		assertEquals( "cross win nestGame 4", game.getBoard()[1][1],PlayerType.CROSS );
	}
	
	@Test( expected = InvalidMoveException.class )
	public void testMoveEveryWhere() throws Exception{
		
		game.addMove( new Move( 4, 0 ) );
		game.addMove( new Move( 0, 4 ) );
		game.addMove( new Move( 4, 1 ) );
		game.addMove( new Move( 1, 4 ) );
		game.addMove( new Move( 4, 2 ) );
		game.addMove( new Move( 2, 4 ) );
		game.addMove( new Move( 4, 3 ) );
		
	}
	
	@Test
	public void  testGameFinished() throws Exception{
		
		game.addMove( new Move( 4, 0 ) );	
		game.addMove( new Move( 0, 4 ) );
		game.addMove( new Move( 4, 4 ) );
		game.addMove( new Move( 4, 3 ) );
		game.addMove( new Move( 3, 4 ) );
		game.addMove( new Move( 4, 1 ) );
		game.addMove( new Move( 1, 6 ) );
		game.addMove( new Move( 6, 4 ) );
		game.addMove( new Move( 4, 8 ) );
		game.addMove( new Move( 8, 3 ) );
		game.addMove( new Move( 3, 0 ) );
		game.addMove( new Move( 0, 3 ) );
		game.addMove( new Move( 3, 8 ) );
		game.addMove( new Move( 8, 5 ) );
		game.addMove( new Move( 5, 4 ) );
		game.addMove( new Move( 2, 5 ) );
		game.addMove( new Move( 5, 5 ) );
		game.addMove( new Move( 5, 7 ) );
		game.addMove( new Move( 7, 8 ) );
		game.addMove( new Move( 8, 4 ) );
		game.addMove( new Move( 5, 3 ) );
		
		game.addMove( new Move( 3, 3 ) );
        
		assertEquals( "GameState should be finished, no move should be recieved", 21, game.getMoves().size() );
	}
	@Test
	public void  testCrossGame() throws Exception{
		
		game.addMove( new Move( 4, 0 ) );	
		game.addMove( new Move( 0, 4 ) );
		game.addMove( new Move( 4, 4 ) );
		game.addMove( new Move( 4, 3 ) );
		game.addMove( new Move( 3, 4 ) );
		game.addMove( new Move( 4, 1 ) );
		game.addMove( new Move( 1, 6 ) );
		game.addMove( new Move( 6, 4 ) );
		game.addMove( new Move( 4, 8 ) );
		game.addMove( new Move( 8, 3 ) );
		game.addMove( new Move( 3, 0 ) );
		game.addMove( new Move( 0, 3 ) );
		game.addMove( new Move( 3, 8 ) );
		game.addMove( new Move( 8, 5 ) );
		game.addMove( new Move( 5, 4 ) );
		game.addMove( new Move( 2, 5 ) );
		game.addMove( new Move( 5, 5 ) );
		game.addMove( new Move( 5, 7 ) );
		game.addMove( new Move( 7, 8 ) );
		game.addMove( new Move( 8, 4 ) );
		game.addMove( new Move( 5, 3 ) );
        
		assertEquals( "GameState should be win", GameState.WIN, game.getState() );
	}
	
	@Test
	public void  testGameNoughtWin() throws Exception{
		
		game.addMove( new Move( 4, 0 ) );	
		game.addMove( new Move( 0, 1 ) );
		game.addMove( new Move( 1, 0 ) );
		game.addMove( new Move( 0, 2 ) );
		game.addMove( new Move( 2, 0 ) );
		game.addMove( new Move( 0, 0 ) );
		game.addMove( new Move( 5, 3 ) );
		game.addMove( new Move( 3, 1 ) );
		game.addMove( new Move( 1, 3 ) );
		game.addMove( new Move( 3, 4 ) );
		game.addMove( new Move( 4, 3 ) );
		game.addMove( new Move( 3, 7 ) );
		game.addMove( new Move( 7, 6 ) );
		game.addMove( new Move( 6, 8 ) );
		game.addMove( new Move( 8, 6 ) );
		game.addMove( new Move( 6, 5 ) );
		game.addMove( new Move( 5, 6 ) );
		game.addMove( new Move( 6, 2 ) );
		
		assertEquals( "GameState should be win", GameState.WIN, game.getState() );
	}
	

	@Test
	public void testForDraw() throws Exception {
		
		game.addMove( new Move( 4, 0 ) );
		game.addMove( new Move( 0, 0 ) );
		game.addMove( new Move( 0, 8 ) );
		game.addMove( new Move( 8, 0 ) );
		game.addMove( new Move( 0, 1 ) );
		game.addMove( new Move( 1, 0 ) );	
		game.addMove( new Move( 0, 2 ) );
		game.addMove( new Move( 2, 0 ) );
		game.addMove( new Move( 0, 3 ) );
		game.addMove( new Move( 3, 0 ) );
		game.addMove( new Move( 0, 6 ) );
		game.addMove( new Move( 6, 1 ) );
		game.addMove( new Move( 1, 4 ) );
		game.addMove( new Move( 4, 4 ) );
		game.addMove( new Move( 4, 1 ) );
		game.addMove( new Move( 1, 8 ) );
		game.addMove( new Move( 8, 8 ) );
		game.addMove( new Move( 8, 4 ) );
		game.addMove( new Move( 4, 7 ) );
		game.addMove( new Move( 7, 8 ) );
		game.addMove( new Move( 8, 1 ) );
		game.addMove( new Move( 1, 6 ) );
		game.addMove( new Move( 6, 0 ) );
		game.addMove( new Move( 0, 7 ) );
		game.addMove( new Move( 7, 0 ) );
		game.addMove( new Move( 0, 5 ) );
		game.addMove( new Move( 5, 0 ) );
		game.addMove( new Move( 0, 4 ) );
		game.addMove( new Move( 4, 5 ) );
		game.addMove( new Move( 5, 3 ) );
		game.addMove( new Move( 3, 5 ) );	
		game.addMove( new Move( 5, 8 ) );
		game.addMove( new Move( 8, 5 ) );
		game.addMove( new Move( 5, 5 ) );
		game.addMove( new Move( 5, 7 ) );
		game.addMove( new Move( 7, 1 ) );
		game.addMove( new Move( 1, 7 ) );
		game.addMove( new Move( 7, 3 ) );
		game.addMove( new Move( 3, 2 ) );
		game.addMove( new Move( 2, 1 ) );
		game.addMove( new Move( 1, 3 ) );
		game.addMove( new Move( 3, 1 ) );
		game.addMove( new Move( 1, 5 ) );
		game.addMove( new Move( 5, 6 ) );
		game.addMove( new Move( 6, 4 ) );
		game.addMove( new Move( 4, 2 ) );
		game.addMove( new Move( 2, 2 ) );
		game.addMove( new Move( 2, 5 ) );
		game.addMove( new Move( 5, 4 ) );
		game.addMove( new Move( 4, 8 ) );
		game.addMove( new Move( 8, 3 ) );
		game.addMove( new Move( 3, 3 ) );
		game.addMove( new Move( 3, 6 ) );
		game.addMove( new Move( 6, 3 ) );
		game.addMove( new Move( 3, 4 ) );
		game.addMove( new Move( 4, 6 ) );	
		game.addMove( new Move( 6, 5 ) );
		game.addMove( new Move( 5, 1 ) );
		game.addMove( new Move( 5, 2 ) );
		game.addMove( new Move( 2, 6 ) );
		game.addMove( new Move( 6, 7 ) );
		game.addMove( new Move( 7, 2 ) );
		game.addMove( new Move( 2, 3 ) );
		game.addMove( new Move( 8, 2 ) );
		game.addMove( new Move( 2, 8 ) );
		game.addMove( new Move( 8, 7 ) );
		game.addMove( new Move( 7, 5 ) );
		game.addMove( new Move( 7, 7 ) );
		game.addMove( new Move( 7, 6 ) );
		game.addMove( new Move( 6, 8 ) );
		game.addMove( new Move( 8, 6 ) );
		game.addMove( new Move( 6, 6 ) );
		game.addMove( new Move( 6, 2 ) );
		game.addMove( new Move( 2, 7 ) );
		game.addMove( new Move( 7, 4 ) );
		game.addMove( new Move( 2, 4 ) );
		

		assertTrue("game state should return draw",game.getState()==GameState.DRAW);
	}
}
