package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import com.j256.ormlite.dao.Dao;

/**
 * Servlet for Updating a User
 *
 * @author Li Sui
 */
public class UpdateUser extends HttpServlet {

	/** The User Data Access Object */
	private Dao<User, String> userDao;

	private static final String PARAM_USERNAME = "displayName";
	private static final String PARAM_GENDER = "gender";
	private static final String PARAM_LOCATION= "location";
	private static final String PARAM_ABOUTME= "aboutMe";
	private static final String PARAM_AGE= "age";

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 3375314273171786064L;

	protected void doPut( final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		try {

			/* Get the User */
			final User user = ( User ) request.getSession().getAttribute( "user" );

			/* Get the new User Information */
			final String userName = request.getParameter( PARAM_USERNAME );
			final String gender = request.getParameter( PARAM_GENDER );
			final String location = request.getParameter( PARAM_LOCATION );
			final String aboutMe = request.getParameter( PARAM_ABOUTME );
			final String age = request.getParameter( PARAM_AGE );

			user.setDisplayName( userName );
			user.setGender(gender);
			user.setLocation(location);
			user.setAboutMe(aboutMe);
			user.setAge(age);

			userDao.update( user );

		} catch ( SQLException e ) {

			throw new IOException( e );
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize User DAO */
		userDao = ( Dao<User, String> ) config.getServletContext().getAttribute( Application.USER_DAO );
	}
}
