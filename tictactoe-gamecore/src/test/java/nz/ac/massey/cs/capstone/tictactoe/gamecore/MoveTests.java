package nz.ac.massey.cs.capstone.tictactoe.gamecore;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test cases for the Position object
 *
 * @author Colin Campbell
 * @author Tom Magson
 * @author Li Sui
 *
 * TODO: Test cases for equals and hash code
 */
public class MoveTests {

	/**
	 * Test that the nestedGame field is set via the constructor
	 */
	@Test
	public void testNestedGame() {

		final Move move = new Move( 2, 4 );

		assertEquals( "NestedGame should be two", Integer.valueOf( 2 ), move.nestedGame );
	}

	/**
	 * Test that the move field is set via the constrcutor
	 */
	@Test
	public void testPostion() {

		final Move move = new Move( 2, 4 );

		assertEquals( "Postion should be four", Integer.valueOf( 4 ), move.position );
	}

	/**
	 * Test that a nested position less that zero throws
	 * and illegal argument exception
	 */
	@Test( expected = IllegalArgumentException.class )
	public void testNegitiveMetaMove() {

		new Move( -1, 7 );
	}

	/**
	 * Test that a position less that zero throws
	 * and illegal argument exception
	 */
	@Test( expected = IllegalArgumentException.class )
	public void testNegitiveSubMove() {

		new Move( 2, -8 );
	}

	/**
	 * Test that a nestedPostion greater than eight throws
	 * and illegal argument exception
	 */
	@Test( expected = IllegalArgumentException.class )
	public void testTooLargeMetaMove() {

		new Move( 9, 7 );
	}

	/**
	 * Test that a postion greater than eight throws
	 * and illegal argument exception
	 */
	@Test( expected = IllegalArgumentException.class )
	public void testTooLargeSubMove() {

		new Move( 2, 9 );
	}

	/**
	 * Test that moves with the same nestedGame and position are equals
	 */
	@Test
	public void testEquals() {

		for ( int nestedGame = 0 ; nestedGame < 9 ; nestedGame++ ) {
			for ( int position = 0 ; position < 9 ; position++ ) {
				assertTrue( "Moves should be equal", new Move( nestedGame, position ).equals( new Move( nestedGame, position ) ) );
			}
		}
	}

	/**
	 * Test that a move is equal to itself
	 */
	@Test
	public void testEqualsWithItSelf() {

		final Move move = new Move( 4, 6 );

		assertTrue( "Move should be equal to itself", move.equals( move ) );
	}

	/**
	 * Test that moves with different nested game and position are not equals
	 */
	@Test
	public void testNotEquals() {

		for ( int nestedGame = 0 ; nestedGame < 9 ; nestedGame++ ) {
			for ( int position = 0 ; position < 9 ; position++ ) {
				for ( int otherNestedGame = 0 ; otherNestedGame < 9 ; otherNestedGame++ ) {
					for ( int otherPosition = 0 ; otherPosition < 9 ; otherPosition++ ) {
						if ( nestedGame != otherNestedGame && position != otherPosition ) {
							assertFalse( "Moves should not be equals", new Move( nestedGame, position ).equals( new Move( otherNestedGame, otherPosition ) ) );
						}
					}
				}
			}
		}
	}

	/**
	 * Test that a move is not equal to null
	 */
	@Test
	public void testNotEqualToNull() {

		assertNotNull( "Move should not be equal to null", new Move( 4, 2 ) );
	}

	/**
	 * Test that a move is not equal to an object
	 */
	@Test
	public void testNotEqualsToObject() {

		assertFalse( new Move( 4, 2 ).equals( new Object() ) );
	}
}
