<!DOCTYPE html>
<html>

	<head>
		<link type="text/css" rel="stylesheet" href="css/errorstyles.css">
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,900,300' rel='stylesheet' type='text/css'>
		<title> Browser Not Supported </title>
	</head>

	<body>
		<section>
			<h1> :( </h1>
			<h2> It looks like you are using an unsupported web browser. </h2>
			<p>  Play Tic-Tac-Toe With Me probably wont work with your web browser, maybe you should consider upgrading it?</p> <p> Check out some of the links below and if you decide to upgrade we will be right here waiting to play tic tac toe! </p>
				<div id="browser_links">
					<a href="http://www.mozilla.org/en-US/firefox/fx/#desktop"> Try FireFox! </a>
					<h1> Or </h1>
					<a href="https://www.google.com/intl/en/chrome/browser/"> Try Chrome! </a>
				</div>
		</section>	

	</body>
</html>