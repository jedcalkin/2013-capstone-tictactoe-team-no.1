package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for killing a session
 *
 * @author Colin Campbell
 */
public class Logout extends HttpServlet {

	/**
	 * Serial Version 
	 */
	private static final long serialVersionUID = 4616356236825258249L;

	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		request.getSession().invalidate();
		response.sendRedirect( "." );
	}
}
