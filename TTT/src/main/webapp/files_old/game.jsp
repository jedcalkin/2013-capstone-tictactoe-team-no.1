<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.gamecore.Game" %>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby" %>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import='com.j256.ormlite.dao.Dao' %>
<%@ page import='nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application' %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%
Lobby lobby = ( Lobby ) application.getAttribute( Application.LOBBY );
@SuppressWarnings("unchecked")
final Dao<User, String> userDao = ( Dao<User, String> ) getServletContext().getAttribute( Application.USER_DAO );
userDao.refresh( user );
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );
Game game = lobby.getGameByUser( user.getIdentity() );
User creatorUser=userDao.queryForId(game.getCreatorId());
%>
<%@ include file="page_head.include" %>
<div id="login_info">
	<a href="https://en.gravatar.com/">
		<img class="glossy image-wrap" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="gravatar" />
	</a>
	<%= resourceBundle.getString( "CURRENTLY_LOGIN" ) %>:<br>
	<b>${user.email}</b><br>
	<b><a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank"><%= resourceBundle.getString( "PROFILE" ) %></a></b>
	<input type="hidden" id ="me" value="<%= user.getDisplayName()%>"/>
</div>
</header>
<script data-main="js/game" src="js/require.js"></script><!-- HOPE THIS WORKS -->
<%@ include file="page_menu.include" %>
<section>
	<div id="player1">
		<b id="creator"><%=creatorUser.getDisplayName()%></b><br>
		<a href="profile.jsp?userId=<%=creatorUser.getIdentity()%>" target="_blank">
			<img src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( creatorUser.getEmail() ) %>?s=156" alt="gravatar" />
		</a>
	</div>

	<div id="player2">
	<b id="opponent"><%= resourceBundle.getString( "WAITING_OPPONENT" ) %></b><br>
	<img src="Media/nought.svg" alt="waiting" /><br>
	<b><%= resourceBundle.getString( "WINS" ) %>:&ensp;</b>__<br>
	<b><%= resourceBundle.getString( "DRAWS" ) %>:&ensp;</b>__<br>
	<b><%= resourceBundle.getString( "LOSSES" ) %>:&ensp;</b>__<br>
	</div>

	<table id="maingame">
	<tr>
	<td>
		<img id="spinner" src="./Media/loading.gif" alt="Loading...">
	</td>
	</tr>
	</table>
	<div id="savebutton" class="savebuttonINvisable">
			<button id="saveGame">Save Game?</button>
	</div>
</section>
<%@ include file="page_tail.include" %>
