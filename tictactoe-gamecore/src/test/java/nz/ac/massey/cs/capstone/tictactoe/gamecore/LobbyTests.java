package nz.ac.massey.cs.capstone.tictactoe.gamecore;

import static org.junit.Assert.*;
import static org.junit.Assume.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

/**
 * Lobby test cases
 * 
 * @author Colin Campbell
 * @author Li Sui
 */
public class LobbyTests {

	private Lobby lobby;

	@Before
	public void setUp() throws Exception {

		lobby = new Lobby();
	}

	/**
	 * Test that creating a public game
	 */
	@Test
	public void testCreatePublicGame() {
		
		final String gamerTag = "guysmiley";
        final String gameId = lobby.createGame( gamerTag, true,null );

        assertNotNull( "Should of got a game", lobby.getGameByGameId( gameId ) );
	}

	/**
	 * Test that creating a private game
	 */
	@Test
	public void testCreatePrivateGame() {
		
		final String gamerTag = "guysmiley";
        final String gameId = lobby.createGame( gamerTag, false,"123" );

        assertNotNull( "Should of got a game", lobby.getGameByGameId( gameId ) );
	}
	
	@Test
	public void testCreateGameIsOpen() {

		 final String gamerTag = "guysmiley";
         final String gameId = lobby.createGame( gamerTag, true,null );

         assertTrue( "Game should be open", lobby.getGameByGameId( gameId ).getIsOpen() );
	}

	/**
	 * Test that joining a game returns true 
	 */
	@Test
	public void testJoinGame() {
		
		final String playerOne = "Alex";
        final String playerTwo = "John";
        final String gameId = lobby.createGame( playerOne, true,null);

        assertTrue( "true should have been returned", lobby.joinGame( gameId, playerTwo ) );
	}

	/**
	 * Test that joining a game that does not exist returns false
	 */
	@Test
	public void testJoinGameNotExist() {

		 assertFalse( "false should have been returned", lobby.joinGame( "f8743h87" , "Li") );
	}

	/**
	 * Test that joining a closed game returns false
	 */
	@Test
	public void testJoinClosedGame() {

		final String gameId = lobby.createGame( "creator", true ,null);

        assumeTrue( "It is assumed that opponent joined the game", lobby.joinGame( gameId, "opponent" ) );
        assertFalse( "false should have been returned", lobby.joinGame( gameId, "otherGuy" ) );
	}

	/**
	 * Test that joining a game sets the opponent
	 */
	@Test
	public void testJoinGameGetsOpponent() {

		final String gameId = lobby.createGame( "creator", true,null );
        assumeTrue( "It is assumed that the opponent joined the game", lobby.joinGame( gameId, "opponent" ) );

        final Game game = lobby.getGameByGameId( gameId );
        assertEquals( "opponent id should be set", "opponent", game.getOpponentId() );
	}

	/**
	 * Test that updating a game returns true
	 */
	@Test
	public void testUpdateGame() throws Exception {

		final String gamerTag = "Will";

        final String gameId=lobby.createGame( gamerTag, true,null );

        assertTrue( "true should have been returned", lobby.updateGame( gameId, new Move( 4, 6 ) ) );
	}

	/**
	 * Test that updating a game that does not exist returns false
	 */
	@Test
	public void testUpdateGameFails() throws Exception {
		
		assertFalse( "false should have been returned ", lobby.updateGame( "Li" , new Move( 4, 3 ) ) );
	}

	/**
	 * Test that get game by id returns the game
	 */
	@Test
	public void testGetGameById() {

		final String gamerTag = "Aaron";

        final String gameId = lobby.createGame( gamerTag, true,null );

        assertEquals( "Aaron should have been returned", "Aaron", lobby.getGameByGameId( gameId ).getCreatorId() );
	}
	
	/**
	 * Test that get game by userId returns the game
	 */
	@Test
	public void testGetGameByUserId() {

		final String gamerTag = "Aaron";

        lobby.createGame( gamerTag, true,null );

        assertEquals( "Aaron should have been returned", "Aaron", lobby.getGameByUser( gamerTag ).getCreatorId() );
	}

	/**
	 * Test that get game by id return null if the game does not exist
	 */
	@Test
	public void testGetGameByIdFails() {

		assertNull( "null should be returned", lobby.getGameByGameId( "no-game" ) );
	}

	/**
	 * Test that get pending games returns visible open games
	 */
	@Test
	public void testGetPendingGames() {

		final ArrayList<String> openGames = new ArrayList<String>();
        openGames.add( lobby.createGame( "a", true,null ) );
        openGames.add( lobby.createGame( "b", true,null ) );
        openGames.add( lobby.createGame( "c", true,null ) );

        assertTrue( "All of the gameIds should be returned", lobby.getPendingGames().keySet().containsAll( openGames ) );
	}

	

	/**
	 * Test that closed games are not returned
	 */
	@Test
	public void testGetPendingGamesOpen() {
		
		 final String gameId = lobby.createGame( "a", true,null );
         final Game game = lobby.getGameByGameId( gameId );
         game.setIsOpen( false );

         assertFalse( "gameId should not be returned", lobby.getPendingGames().containsKey( gameId ) );
	}
	/**
	 * Test that removed game should not be in the list
	 */
	@Test
	public void testRemoveGame(){
		
	    lobby.createGame( "b", true, null );
		
	    lobby.removeGame("b");
	    
	    assertTrue("pending games should be empty ", lobby.getPendingGames().isEmpty() );
		
	}
	/**
	 * Test that removed game that not exist
	 */
	@Test
	public void testRemoveGameNotExist(){

	   assertNull( lobby.removeGame("b"));
		
	}
}
