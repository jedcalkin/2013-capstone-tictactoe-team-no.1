package nz.ac.massey.cs.capstone.tictactoe.webapp.listeners;

import static org.mockito.Mockito.*;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationTests {

	/**
	 * TEMP SQLLite
	 */
	private final String DB_PATH = System.getProperty( "java.io.tmpdir" ) + System.getProperty( "file.separator" ) + "TTT.db";

	private Application contextListener;

	@Mock
	ServletContextEvent contextEvent;

	@Mock
	private ServletContext context;

	@Before
	public void setUp() {

		contextListener = new Application();
		when( contextEvent.getServletContext() ).thenReturn( context );

		when( context.getRealPath( anyString() ) ).thenReturn( DB_PATH );
	}

	@After
	public void tearDown() {

		new File( DB_PATH ).delete();
	}

	/**
	 * Test that application initialized the lobby
	 */
	@Test
	public void testContextInitialized() {

		contextListener.contextInitialized( contextEvent );

		verify( context ).setAttribute( eq( Application.LOBBY ), any( Lobby.class ) );
	}

	/**
	 * Test that application destroys the lobby
	 */
	@Test
	public void testContextDestroyed() {

		contextListener.contextDestroyed( contextEvent );

		verify( context ).removeAttribute( Application.LOBBY );
	}
}
