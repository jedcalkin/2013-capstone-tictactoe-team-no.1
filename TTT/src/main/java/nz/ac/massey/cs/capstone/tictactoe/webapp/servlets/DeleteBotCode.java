package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import com.j256.ormlite.dao.Dao;

/**
 * @author Li Sui
 */
public class DeleteBotCode extends HttpServlet {
	
	/* version*/
	private static final long serialVersionUID = -3314002704587956544L;
       
	/** The BotCode Data Access Object */
	private Dao<BotCode, Integer> botDao;

	/** The User Data Access Object */
	private Dao<User, String> userDao;
	
	/** bot ID Parameter */
	private static final String PARAM_BOTID="botId";
	
	protected void doDelete(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		
		final Integer botId = Integer.parseInt( request.getParameter( PARAM_BOTID ) );

		/* Get User */
		final User user = ( User ) request.getSession().getAttribute( Authentication.USER );
		
		try {
			
			botDao.deleteById(botId);
			
			userDao.refresh( user );
			
		} catch ( final SQLException e) {
		
			throw new ServletException( e );
		}
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init( final ServletConfig config ) throws ServletException {

		super.init ( config );

		/* Initialize User DAO */
		botDao = ( Dao<BotCode, Integer> ) config.getServletContext().getAttribute( Application.BOT_DAO );

		/* Initialize User DAO */
		userDao = ( Dao<User, String> ) config.getServletContext().getAttribute( Application.USER_DAO );
	}

}
