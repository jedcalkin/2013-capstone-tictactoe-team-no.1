package nz.ac.massey.cs.capstone.tictactoe.webapp.models;

import java.util.HashSet;
import java.util.Set;

public class MySecurityManager extends SecurityManager {

	private Set<Thread> restrictedThreads = new HashSet<Thread>();
	
	public synchronized void restrict(Thread t) {
		
		restrictedThreads.add(t);
		
	}
	
	public synchronized void unrestrict(Thread t) {
		
		restrictedThreads.remove(t);
		
	}

}
