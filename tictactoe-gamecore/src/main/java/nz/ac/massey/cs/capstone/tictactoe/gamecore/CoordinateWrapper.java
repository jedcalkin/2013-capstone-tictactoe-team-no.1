package nz.ac.massey.cs.capstone.tictactoe.gamecore;

/**
 * Wraps a Move as a pair of Coordinates
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class CoordinateWrapper {

	/**
	 * Move
	 */
	public final Move move;

	/**
	 * Constructs a new CoordinateWrapper
	 *
	 * @param move the position to wrap
	 */
	public CoordinateWrapper( final Move move ) {

		this.move = move;
	}

	/**
	 * Gets the coordinate
	 *
	 * @return the coordinate
	 */
	public Coordinate getCoordinate() {

	  return convert( move.nestedGame );
	}

	/**
	 * Gets the coordinate for the nested game
	 *
	 * @return the coordinate
	 */
	public Coordinate getNestedCoordinate() {

	  return convert( move.position );
	}

	/**
	 * Converts an integer representation of position on the tictactoe board to
	 * a coordinate
	 *
	 * @param position the position to convert
	 * @return the coordinate
	 */
	private final Coordinate convert( final int position ) {

		return new Coordinate() {

			@Override
			public int getRow() {

				if ( position < 3 ) {
					return 0;
				}
				else if ( position < 6 ) {
					return 1;
				}
				else {
					return 2;
				}
			}

			@Override
			public int getColumn() {

				return position % 3;
			}
		};
	}

	/**
	 * Gets the move
	 *
	 * @return the move
	 */
	public Move getMove(){

		return move;
	}
}
