<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%	
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );

%>
<%@ include file="page_head.include" %>
<div id="login_info">
	<a href="https://en.gravatar.com/">
		<img class="glossy image-wrap" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="gravatar" />
	</a>
	<%= resourceBundle.getString( "CURRENTLY_LOGIN" ) %>:<br>
	<b id="me">${user.email}</b><br>
	<b><a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank"><%= resourceBundle.getString( "PROFILE" ) %></a></b>
</div>
</header>
<script data-main="js/replay" src="js/require.js"></script>
<%@ include file="page_menu.include" %>
<section>
	<div id="player1">
		<b id="creator">${user.displayName}</b><br>
		<a href="https://en.gravatar.com/">
			<img src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=156" alt="gravatar" />
		</a><br>
		<b><%= resourceBundle.getString( "WINS" ) %>:&ensp;</b>${user.numWins }<br>
		<b><%= resourceBundle.getString( "DRAWS" ) %>:&ensp;</b>${user.numDraws}<br>
		<b><%= resourceBundle.getString( "LOSSES" ) %>:&ensp;</b>${user.numLoses}<br>
	</div>

	<div id="player2">
		<b id="opponent"><%= resourceBundle.getString( "LIST_GAMES" ) %></b><br>
		<ul id="listOfOldGames">
		</ul>
	</div>

	<table id="maingame">
		<tr><td><table class="subgame">
			<tr><td id="00"></td><td id="01"></td><td id="02"></td></tr>
			<tr><td id="03"></td><td id="04"></td><td id="05"></td></tr>
			<tr><td id="06"></td><td id="07"></td><td id="08"></td></tr>
			</table></td><td><table class="subgame">
			<tr><td id="10"></td><td id="11"></td><td id="12"></td></tr>
			<tr><td id="13"></td><td id="14"></td><td id="15"></td></tr>
			<tr><td id="16"></td><td id="17"></td><td id="18"></td></tr>
			</table></td><td><table class="subgame">
			<tr><td id="20"></td><td id="21"></td><td id="22"></td></tr>
			<tr><td id="23"></td><td id="24"></td><td id="25"></td></tr>
			<tr><td id="26"></td><td id="27"></td><td id="28"></td></tr>
		</table></td></tr>
		<tr><td><table class="subgame">
			<tr><td id="30"></td><td id="31"></td><td id="32"></td></tr>
			<tr><td id="33"></td><td id="34"></td><td id="35"></td></tr>
			<tr><td id="36"></td><td id="37"></td><td id="38"></td></tr>
			</table></td><td><table class="subgame">
			<tr><td id="40"></td><td id="41"></td><td id="42"></td></tr>
			<tr><td id="43"></td><td id="44"></td><td id="45"></td></tr>
			<tr><td id="46"></td><td id="47"></td><td id="48"></td></tr>
			</table></td><td><table class="subgame">
			<tr><td id="50"></td><td id="51"></td><td id="52"></td></tr>
			<tr><td id="53"></td><td id="54"></td><td id="55"></td></tr>
			<tr><td id="56"></td><td id="57"></td><td id="58"></td></tr>
		</table></td></tr>
		<tr><td><table class="subgame">
			<tr><td id="60"></td><td id="61"></td><td id="62"></td></tr>
			<tr><td id="63"></td><td id="64"></td><td id="65"></td></tr>
			<tr><td id="66"></td><td id="67"></td><td id="68"></td></tr>
			</table></td><td><table class="subgame">
			<tr><td id="70"></td><td id="71"></td><td id="72"></td></tr>
			<tr><td id="73"></td><td id="74"></td><td id="75"></td></tr>
			<tr><td id="76"></td><td id="77"></td><td id="78"></td></tr>
			</table></td><td><table class="subgame">
			<tr><td id="80"></td><td id="81"></td><td id="82"></td></tr>
			<tr><td id="83"></td><td id="84"></td><td id="85"></td></tr>
			<tr><td id="86"></td><td id="87"></td><td id="88"></td></tr>
		</table></td></tr>
	
	</table>
</section>
<%@ include file="page_tail.include" %>
