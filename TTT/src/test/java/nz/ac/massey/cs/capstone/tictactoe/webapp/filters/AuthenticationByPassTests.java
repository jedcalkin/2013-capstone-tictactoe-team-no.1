/**
 * 
 */
package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.AuthenticationByPass;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.j256.ormlite.dao.Dao;

/**
 * @author Li Sui
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationByPassTests {

	/**
	 * Filter Under test
	 */
	private AuthenticationByPass filter;
	
	/**
	 * Jabber Wocky Parameter
	 */
	private static final String PARAM_JABBERWOCKY = "jabberwocky";

	/**
	 * ID Parameter
	 */
	private static final String PARAM_ID = "id";

	/**
	 * ByPass Key
	 */
	private static final String SECRET_KEY = "2BFBA931BA4D2C77DCD13155647AA";

	@Mock
	private FilterChain chain;

	/**
	 * Mock HTTP Request
	 */
	@Mock
	private HttpServletRequest request;

	/**
	 * Mock HTTPResponse
	 */
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private HttpSession session;
	
	@Mock
	private  ServletContext context;
	
	@Mock
	private Dao<User,String> userDao;
	
	@Mock
	private FilterConfig fConfig;
		
	@Before
	public void setUp() throws Exception {
		
		filter=new AuthenticationByPass();
		
		when( request.getSession() ).thenReturn( session );
		when( context.getAttribute( Application.USER_DAO ) ).thenReturn( userDao );
		when(fConfig.getServletContext()).thenReturn( context );

		filter.init(fConfig);
	}

	@Test
	public void testBackDoorUserBlue() throws Exception {
		
		final User blue=new User();
		
		blue.setIdentity( "blue" );
		blue.setDisplayName( "blue" );
		blue.setEmail( "644022590@qq.com" );
		blue.setAge( "31" );
		blue.setAboutMe( "I'm test user blue" );
		blue.setGender( "Female" );
		blue.setLocation( "Bluevile" );
		blue.setNumDraws( 3 );
		blue.setNumLoses( 2 );
		blue.setNumWins( 4 );

		when( request.getParameter( PARAM_JABBERWOCKY  ) ).thenReturn( SECRET_KEY );
		when( request.getParameter( PARAM_ID ) ).thenReturn( "blue" );
		
		filter.doFilter( request, response, chain );
		verify( session).setAttribute("user", blue);
		
	}
	
	@Test
	public void testBackDoorUserRed() throws Exception{
		
		final User red=new User();
		
		red.setIdentity( "red" );
		red.setDisplayName( "red" );
		red.setEmail( "644022590@qq.com" );
		red.setAge( "13" );
		red.setAboutMe( "I'm test user red" );
		red.setGender( "Male" );
		red.setLocation( "Red Town" );
		red.setNumDraws( 3 );
		red.setNumLoses( 4 );
		red.setNumWins( 2 );
		
		when( request.getParameter( PARAM_JABBERWOCKY  ) ).thenReturn( SECRET_KEY );
		when( request.getParameter( PARAM_ID ) ).thenReturn( "red" );
		
		filter.doFilter( request, response, chain );
		verify( session).setAttribute("user", red);
		
	}
	
	@Test
	public void testDestory() throws Exception{
		
		filter.destroy();
		
		assertTrue(userDao.queryForAll().size()==0);
	}

}
