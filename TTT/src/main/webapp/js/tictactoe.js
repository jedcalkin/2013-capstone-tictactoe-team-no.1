define( /** @lends TicTacToe */ ['jquery'],

	function ( $ ) {

		var timer0 = '';
		var timer1 = '';
		var timer2 = '';

		/**
		 * Default Polling Timeout
		 */
		var timeout = 2000;

		/**
		 * @class TicTacToe
		 * @author Colin Campbell
		 *
		 * @example
		 * require(['tictactoe'], function( TicTacToe ) {
		 * 		var tictactoe = new TicTacToe( 'gameId', 'userId' )
		 * 			.onUpdate( function( game ) {
		 * 				console.log( game );
		 * 			});
		 * 
		 * 		tictactoe.update( { nestedGame : 4, position 3 } );
		 *
		 *		TicTacToe.pendingGames( function( games ) {
		 *			console.log( games )
		 *		});
		 *
		 *		TicTacToe.createGame( 'bob', true, function( gameId ) {
		 *			console.log( gameId );
		 *		});
		 * });
		 *
		 * @constructor
		 * @param {String} gameId - the gameId
		 */
		var TicTacToe = function( gameId ) {
	
			this.gameId = gameId;

		};

		TicTacToe.prototype = {

				/**
				 * On Update
				 *
				 * @param callback - the callback
				 */
				onUpdate : function( callback ) {

					var that = this;

					/* On Poll Function */
					var onPoll = function() {

						/**
						 * Data from last poll
						 */
						var previousData;

						return function( data ) {

							/* First Poll */
							if ( !previousData ) {
								previousData = data;
								callback( data );
								return;
							};

							/* New Poll */
							if ( previousData.moves.length != data.moves.length || previousData.opponentId != data.opponentId ) {

								previousData = data;
								callback( data );
							};
						};
					}();

					/* Start Polling Function */
					var startPolling = function() {
						TicTacToe.getGame( that.gameId, onPoll );
						setTimeout( startPolling, timeout );
					};

					/* Begin polling */
					startPolling();

					return this;
				},

				/**
				 * Updates the game with a new move
				 *
				 * @param {Object} move - Move
				 * @param {Number} move.nestedGame - the nestedGame
				 * @param {Number} move.position - the position
				 */
				update : function( move ) {
					$.ajax( {
						type: 'PUT',
						url: 'Move?' + $.param( { 'gameId' : this.gameId } ),
						contentType: "application/json",
						data: JSON.stringify( move )
					});

					return this;
				},

				/**
				 * Keeps the game alive and executes the callback
				 * with the aliveness state of the game
				 *
				 * @param callback - the callback
				 */
				isAlive : function( callback ) {

					var that = this;
					timer1 = setTimeout( function() {
						$.get( 'GetAliveness', { 'gameId' : that.gameId }, callback );
						that.isAlive( callback );
					}, timeout);

					return this;
				}
		};

		/**
		 * Retrieves and passes Pending Games to the callback
		 *
		 * @param callback - the callback
		 */
		TicTacToe.pendingGames = function( callback ) {
			var that = this;
			timer2 = setTimeout( function() {
				$.get( 'ListGames', callback );
				that.pendingGames( callback );
			}, timeout );
			return this;
		};

		/**
		 * Retrieves and passes List of Bots to the callback
		 *
		 * @param callback - the callback
		 */
		TicTacToe.listBots = function( callback ) {

			$.get( 'ListBots', callback );

			return this;
		};

		/**
		 * Create a new Bot
		 *
		 * @TODO: Add Parameters
		 */
		TicTacToe.createOrUpdateBot = function( botCode, callback ) {

			$.ajax( {
				type: 'PUT',
				url: 'UpdateBot',
				contentType: "application/json",
				data: JSON.stringify( botCode )
			}).done( callback );

			return this;
		};

		TicTacToe.getBots = function( callback ) {

			$.get( 'GetBots', callback );

			return this;
		};
		
		TicTacToe.deleteBot = function( botId, callback ) {

			$.ajax( {
				type: 'DELETE',
				url: 'DeleteBotCode?' + $.param( { 'botId' : botId } ),
			}).done( callback );

			return this;
		};
		
		TicTacToe.verifyBotCode =function( botId, callback ) {
			
			$.post( 'VerifyBotCode', { 'botId' : botId }, callback );
			
			return this;
		};

		/**
		 * Creates a new Game
		 *
		 * @param {Boolean} visible - true if game should be visible
		 * @param {String} password - the password for the game
		 * @param callback - the callback
		 */
		TicTacToe.createGame = function(  visible, password, callback ) {

			$.post( 'CreateGame', { 'isVisible' : visible, 'password' : password }, callback );

			return this;
		};

		TicTacToe.playBot = function( botId, callback ) {
			$.post( 'PlayBot', { 'botId' : botId }, callback );
			return this;
		};

		/**
		 * Joins a Game
		 *
		 * @param {String} gameId - the gameId
		 * @param callback - the callback
		 */
		TicTacToe.joinGame = function( gameId, password, callback, onError ) {

			$.ajax({
				type: 'GET',
				url: 'JoinGame?' + $.param( { 'gameId' : gameId , 'password' : password } ),
				success: callback,
				error: onError
			});
			return this;
		};

		/**
		 * Retrieves and passes the Game to the callback
		 *
		 * @param {String} gameId - the gameId
		 * @param callback - the callback
		 */
		TicTacToe.getGame = function( gameId, callback ) {
			$.get( 'GetGame', { 'gameId' : gameId }, callback );
		};

		/**
		 * TODO: CamelCase Method Name
		 * TODO: Document Method
		 */
		TicTacToe.ListOfBots = function( callback ){
			$.get( 'ListOfBots', {}, callback );
			return this;
		};

		/**
		 * TODO: Document Method
		 * TODO: CamelCase Method
		 * TODO: Is this required?
		 */
		TicTacToe.stoppolling = function(){
			clearTimeout(timer0);
			clearTimeout(timer2);
		};

		return TicTacToe;
	}
);
