package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.Marker;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.MarkerCollector;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.dao.Dao;


/**
 * @author Li Sui
 */
public class VerifyBotCode extends HttpServlet {
	
	/**
	 * version
	 */
	private static final long serialVersionUID = 2727950276795041021L;
	
	/** The BotCode Data Access Object */
	private Dao<BotCode, Integer> botCodeDao;
	
	/** Gson Object */
	private Gson gson;
	
	/** bot ID Parameter */
	private static final String PARAM_BOTID="botId";

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";
       
   
	@SuppressWarnings("unchecked")
	@Override
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );
		
		/* Initialize gson */
		gson = new Gson();
		
		/* Initialize bot code data access object */
		botCodeDao = ( Dao<BotCode, Integer> ) getServletContext().getAttribute( Application.BOT_DAO );
		
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* Content Type */
		response.setContentType( CONTENT_TYPE );

        final Integer botId = Integer.parseInt( request.getParameter( PARAM_BOTID ) );

		/* JSON Output Stream */
		final List<Marker> markers = new ArrayList<Marker>();
		
		try {

				if( botCodeDao.idExists( botId )) {
					
					/* JSON Output Stream */
				    final JsonWriter out = new JsonWriter( response.getWriter() );
				    
					BotCode botCode=botCodeDao.queryForId( botId );

					final Boolean status = botCode.compile(new MarkerCollector() {

						@Override
						public void addMarker( Marker marker ) {
						
							markers.add( marker );
						}
					});
						
					out.beginObject()
				    .name( "status" ).value( status ).name("markers").value(gson.toJson(markers))
				    .endObject();
					
					out.close();
					
								
				}else{

				   response.sendError(HttpServletResponse.SC_BAD_REQUEST);
					
				}
			

		} catch ( final SQLException e ) {

			throw new IOException( e );

		} 

		/* Write errorMessage as JSON */
		
		

	}


}
