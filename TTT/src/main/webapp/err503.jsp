<!DOCTYPE html>
<html>

	<head>
		<link type="text/css" rel="stylesheet" href="css/errorstyles.css">
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,900,300' rel='stylesheet' type='text/css'>
		<title> 503 - Server Error </title>
	</head>

	<body>
		<section>
			<h1> :( </h1>
			<h2> It seems that something has gone wrong on our side.</h2>
			<p>  Something has broken and the code monkeys are working on fixing it.</p> 
			<p>  Please check back later. Click 
			<a href="http://www.playtictactoewith.me"> here </a> to return home.</p>
		</section>	

	</body>
</html>