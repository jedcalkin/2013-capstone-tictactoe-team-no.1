package nz.ac.massey.cs.capstone.tictactoe.gamecore.exceptions;

/**
 * An exception that is thrown when a action is performed on a 
 * game where it transition the game to an illegal state
 *
 * @author Colin Campbell
 * @author Tom Magson
 * @author Li Sui
 *
 * TODO: Create Constructors
 * TODO: Create Test cases
 *
 */
public class GameStateException extends Exception {

	private static final long serialVersionUID = -6399701073793507837L;

}
