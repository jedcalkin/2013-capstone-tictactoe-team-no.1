package nz.ac.massey.cs.capstone.tictactoe.webapp.listeners;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;


/**
 * Session Listener
 *
 * @author Colin Campbell
 * @author Li Sui
 */
public class Session implements HttpSessionListener {

	public static final String LISTBOTS = "_LISTBOTS";
	
	@Override
    public void sessionCreated( HttpSessionEvent se ) {
		
		final HttpSession session = se.getSession();
		
		session.setAttribute(LISTBOTS, new ConcurrentHashMap<Integer, Robot>() );
		
    }

	@SuppressWarnings("unchecked")
	@Override
    public void sessionDestroyed( HttpSessionEvent se ) {

		final HttpSession session = se.getSession();

		final Lobby lobby = ( Lobby ) session.getServletContext().getAttribute( Application.LOBBY );
	    final User user=( User ) session.getAttribute("user");
		final Map< Game, Long[] > timeouts= ( Map<Game,Long[]> ) session.getServletContext().getAttribute( Application.TIMEOUTS );
		final Game game=lobby.removeGame(user.getIdentity());
		
		if( game!=null ){
			
			timeouts.remove(game);
			
		}
		
		
    }
}
