package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;

import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test cases for the NoAccess Filter
 *
 * @author cncampbe
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NoCacheTests {

	/**
	 * Filter Under test
	 */
	private NoCache filter;

	/**
	 * Mock Filter Chain
	 */
	@Mock
	private FilterChain chain;

	/**
	 * Mock HTTP Request
	 */
	@Mock
	private HttpServletRequest request;

	/**
	 * Mock HTTPResponse
	 */
	@Mock
	private HttpServletResponse response;

	@Before
	public void setUp() throws Exception{

		filter = new NoCache();
		
	}

	/**
	 * Test that the Header of response
	 */
	@Test
	public void testSetCacheHeader() throws IOException, ServletException {

		filter.doFilter( request, response, chain );

		verify( response ).setHeader("Cache-Control", "no-cache");
	}
	/**
	 * Test that data header of response
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void testSetDataHeader() throws IOException, ServletException {

		filter.doFilter( request, response, chain );

		verify( response ).setDateHeader("Expires", 0);
	}
	/**
	 * Test that prama header of response
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void testSetPramaHeader() throws IOException, ServletException {

		filter.doFilter( request, response, chain );

		verify( response ).setHeader("Pragma", "No-cache");
	}
}
