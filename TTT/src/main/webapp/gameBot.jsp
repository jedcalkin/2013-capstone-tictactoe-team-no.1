<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import='nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application' %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );

%>
<%@ include file="page_head_tmp.include" %>
	<a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank" title="Profile"><img id="user_image" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="gravatar" /></a>
	<a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank" title="Profile"><img id="profile_link" src="./Media/cog.png" alt="profile_link" /></a>
</header>

<script data-main="js/gameBot" src="js/require.js"></script>
<%@ include file="page_menu_tmp.include" %>
<section>
	<div id="player1">
		<b>${user.displayName} </b><br>
		<a href="https://en.gravatar.com/" title="Gravatar">
			<img src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=156" alt="gravatar" />
		</a><br>
	</div>

	<div id="player2">
	<b id="opponent"><%= resourceBundle.getString( "SMART_BOT" ) %></b><br>
	<img src="Media/robot_color.svg" alt="this bot is smarter than you" /><br>
	
	</div>

	<table id="maingame">
	<tr>
	<td>
		<img id="spinner" src="./Media/loading.gif" alt="Loading...">
	</td>
	</tr>
	</table>
	<script>
	bob = 0
	</script>
</section>

<%@ include file="page_tail_tmp.include" %>
