requirejs.config({
    paths : {
    	jquery : '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
    	i18n : '//cdn.jsdelivr.net/i18next/1.7.1/i18next.min'
	},
	shim: {
	    jquery : {
	        exports: "jQuery"
	    },
	    i18n : {
			exports: "i18n"
		}
	}
});

require( ['jquery', 'i18n', 'tictactoe', 'toast', 'menu'], function( $, i18n, TicTacToe, Toast ) {

	var div = $('#bots');

	i18n.init( function( translate ) {

		TicTacToe.listBots( function( bots ) {

			var list = $('<ul/>');
			var li = $('<li/>');
			div.empty();
			for ( var  i in bots ) {
				
				var bot = bots[ i ];
				
				var button = $( '<button>' );
				button.attr( 'type', 'button' );
				button.attr('class','public_button');
				
				button.text(  bot.name+' ' +translate( 'BOT' ) )
					.click( function(i) {

						return function() {

							TicTacToe.playBot( bot.identity, function( game) {

								window.location.href='gameBot.jsp?gameId=' + game.gameId;
							} );
						}

					}(i) );
				
				li.append(button)
				
				list.append(li);
			};

			div.append( list );
		});
	});
});