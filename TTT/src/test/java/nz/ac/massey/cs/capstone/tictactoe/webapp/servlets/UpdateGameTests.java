package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.StringReader;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.UpdateGame;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.j256.ormlite.dao.Dao;

/**
 * Test cases for the CreateGame servlet
 *
 * @author Li Sui
 * @author Tom Magson
 * @author Colin Campbell
 */
@RunWith(MockitoJUnitRunner.class)
public class UpdateGameTests {

	private Lobby lobby;

	private User creator;

	private User opponent;
	
    private UpdateGame servlet;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpSession session;

	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	
	@Mock
	private Dao<User,String> userDao;

	@Before
	public void setUp() throws ServletException {

		lobby = new Lobby();

		creator = new User();
		creator.setIdentity( "creator" );
		creator.setNumWins(0);
		creator.setNumLoses(0);
		creator.setNumDraws(0);

		opponent = new User();
		opponent.setIdentity( "opponent" );
		
		opponent.setNumWins(0);
		opponent.setNumLoses(0);
		opponent.setNumDraws(0);
		
		when( context.getAttribute( Application.USER_DAO ) ).thenReturn( userDao );
		when( context.getAttribute( Application.LOBBY ) ).thenReturn( lobby );
		when( config.getServletContext() ).thenReturn( context );
		when(request.getSession()).thenReturn(session);
		
		servlet = new UpdateGame();
		servlet.init(config);
		
	}

	/**
	 * Test that update game updates the moves
	 */
	@Test
	public void testUpdatesLobby() throws Exception {
			
		final String gameId = lobby.createGame( creator.getIdentity(), false,"123" );
		       
		when( request.getSession().getAttribute("user") ).thenReturn( creator );	      
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' : 3 }" ) ) );
	
		servlet.doPut( request, response );

		assertTrue( "Game should contain the move", lobby.getGameByGameId( gameId ).getMoves().contains( new Move( 4, 3 ) ) );
	}

	/**
	 * Test that if a userId that does not belong to a game
	 * tried to update a game an error is sent.
	 */
	@Test
	public void testBadUserUpdatesGame() throws Exception {

		final String gameId = lobby.createGame( creator.getIdentity(), true ,null);
		
		when( request.getParameter( "gameId") ).thenReturn( "123");
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' : 3 }" ) ) );
		
		servlet.doPut( request, response );

		verify( response ).sendError( HttpServletResponse.SC_GONE );

		assertFalse( "Game should not contain the move", lobby.getGameByGameId( gameId ).getMoves().contains( new Move( 4, 3 ) ) );
	}
	/**
	 * Test for first invalid move (4,4)
	 * @throws Exception
	 */
	@Test
	public void testFirstInvaildMove() throws Exception {

		final String gameId = lobby.createGame( creator.getIdentity(), false ,"123");
		
		when( request.getSession().getAttribute("user") ).thenReturn( creator );	      
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' : 4 }" ) ) );
		 
		servlet.doPut( request, response );
		
		verify( response ).sendError( HttpServletResponse.SC_BAD_REQUEST );
		assertFalse( "Game should not contain the move", lobby.getGameByGameId( gameId ).getMoves().contains( new Move( 4, 4 ) ) );
	}
	/**
	 * Test for invalid moves
	 * @throws Exception
	 */
	@Test
	public void testInvaildMoves() throws Exception {
		
		final String gameId = lobby.createGame( creator.getIdentity(), false,"123" );
		
		lobby.updateGame(gameId, new Move(4,3));
			      
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' : 5 }" ) ) );
		 
		servlet.doPut( request, response );
		
		verify( response ).sendError( HttpServletResponse.SC_BAD_REQUEST);
		assertFalse( "Game should not contain the move", lobby.getGameByGameId( gameId ).getMoves().contains( new Move( 4, 5 ) ) );
	}
	
	/**
	 * Test for the move has contained
	 * @throws Exception
	 */
	@Test
	public void testContainedMove() throws Exception {
		
		final String gameId = lobby.createGame( creator.getIdentity(), false,"123" );
		
		lobby.updateGame(gameId, new Move(4,3));
			      
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' : 3 }" ) ) );
		 
		servlet.doPut( request, response );
		
		verify( response ).sendError( HttpServletResponse.SC_BAD_REQUEST);
		assertTrue( "Game already contained the move", lobby.getGameByGameId( gameId ).getMoves().contains( new Move( 4, 3 ) ) );
	}
	/**
	 * Test for the creator won the game therefore opponent lose
	 * @throws Exception
	 */
	@Test
	public void testCreatorWonGame() throws Exception{
		
		final String gameId = lobby.createGame( creator.getIdentity(), true, null );
		final Game game=lobby.getGameByGameId(gameId);
		
		lobby.joinGame( gameId, opponent.getIdentity() );
		
		game.addMove( new Move( 4, 0 ) );	
		game.addMove( new Move( 0, 4 ) );
		game.addMove( new Move( 4, 4 ) );
		game.addMove( new Move( 4, 3 ) );
		game.addMove( new Move( 3, 4 ) );
		game.addMove( new Move( 4, 1 ) );
		game.addMove( new Move( 1, 6 ) );
		game.addMove( new Move( 6, 4 ) );
		game.addMove( new Move( 4, 8 ) );
		game.addMove( new Move( 8, 3 ) );
		game.addMove( new Move( 3, 0 ) );
		game.addMove( new Move( 0, 3 ) );
		game.addMove( new Move( 3, 8 ) );
		game.addMove( new Move( 8, 5 ) );
		game.addMove( new Move( 5, 4 ) );
		game.addMove( new Move( 2, 5 ) );
		game.addMove( new Move( 5, 5 ) );
		game.addMove( new Move( 5, 7 ) );
		game.addMove( new Move( 7, 8 ) );
		game.addMove( new Move( 8, 4 ) );
		
		when( request.getSession().getAttribute("user") ).thenReturn( creator );
		when( userDao.queryForId( "creator" ) ).thenReturn( creator);
		when( userDao.queryForId( "opponent" ) ).thenReturn( opponent);
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 5, 'position' : 3 }" ) ) );
		
		servlet.doPut( request, response );
		
		assertTrue("Number of wins for creator is 1, Number of loses for opponent is 1",creator.getNumWins()==1&&opponent.getNumLoses()==1);
	}
	/**
	 * Test for the opponent won the game therefore creator lose
	 * @throws Exception
	 */
	@Test
	public void testOpponentWonGame() throws Exception{
		
		final String gameId = lobby.createGame( creator.getIdentity(), true, null );
		final Game game=lobby.getGameByGameId(gameId);
		
		lobby.joinGame( gameId, opponent.getIdentity() );
		
		game.addMove( new Move( 4, 0 ) );	
		game.addMove( new Move( 0, 1 ) );
		game.addMove( new Move( 1, 0 ) );
		game.addMove( new Move( 0, 2 ) );
		game.addMove( new Move( 2, 0 ) );
		game.addMove( new Move( 0, 0 ) );
		game.addMove( new Move( 5, 3 ) );
		game.addMove( new Move( 3, 1 ) );
		game.addMove( new Move( 1, 3 ) );
		game.addMove( new Move( 3, 4 ) );
		game.addMove( new Move( 4, 3 ) );
		game.addMove( new Move( 3, 7 ) );
		game.addMove( new Move( 7, 6 ) );
		game.addMove( new Move( 6, 8 ) );
		game.addMove( new Move( 8, 6 ) );
		game.addMove( new Move( 6, 5 ) );
		game.addMove( new Move( 5, 6 ) );
		
		when( request.getSession().getAttribute("user") ).thenReturn( opponent );
		when( userDao.queryForId( "creator" ) ).thenReturn( creator);
		when( userDao.queryForId( "opponent" ) ).thenReturn( opponent);
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 6, 'position' : 2 }" ) ) );
		
		servlet.doPut( request, response );
		
		assertTrue("Number of wins for opponent is 1, Number of loses for creator is 1",creator.getNumLoses()==1&&opponent.getNumWins()==1);
	}
	/**
	 * Test for the draw
	 * @throws Exception
	 */
	@Test
	public void testDrawGame() throws Exception{
		
		final String gameId = lobby.createGame( creator.getIdentity(), true, null );
		final Game game=lobby.getGameByGameId(gameId);
		
		lobby.joinGame( gameId, opponent.getIdentity() );
		
		game.addMove( new Move( 4, 0 ) );
		game.addMove( new Move( 0, 0 ) );
		game.addMove( new Move( 0, 8 ) );
		game.addMove( new Move( 8, 0 ) );
		game.addMove( new Move( 0, 1 ) );
		game.addMove( new Move( 1, 0 ) );	
		game.addMove( new Move( 0, 2 ) );
		game.addMove( new Move( 2, 0 ) );
		game.addMove( new Move( 0, 3 ) );
		game.addMove( new Move( 3, 0 ) );
		game.addMove( new Move( 0, 6 ) );
		game.addMove( new Move( 6, 1 ) );
		game.addMove( new Move( 1, 4 ) );
		game.addMove( new Move( 4, 4 ) );
		game.addMove( new Move( 4, 1 ) );
		game.addMove( new Move( 1, 8 ) );
		game.addMove( new Move( 8, 8 ) );
		game.addMove( new Move( 8, 4 ) );
		game.addMove( new Move( 4, 7 ) );
		game.addMove( new Move( 7, 8 ) );
		game.addMove( new Move( 8, 1 ) );
		game.addMove( new Move( 1, 6 ) );
		game.addMove( new Move( 6, 0 ) );
		game.addMove( new Move( 0, 7 ) );
		game.addMove( new Move( 7, 0 ) );
		game.addMove( new Move( 0, 5 ) );
		game.addMove( new Move( 5, 0 ) );
		game.addMove( new Move( 0, 4 ) );
		game.addMove( new Move( 4, 5 ) );
		game.addMove( new Move( 5, 3 ) );
		game.addMove( new Move( 3, 5 ) );	
		game.addMove( new Move( 5, 8 ) );
		game.addMove( new Move( 8, 5 ) );
		game.addMove( new Move( 5, 5 ) );
		game.addMove( new Move( 5, 7 ) );
		game.addMove( new Move( 7, 1 ) );
		game.addMove( new Move( 1, 7 ) );
		game.addMove( new Move( 7, 3 ) );
		game.addMove( new Move( 3, 2 ) );
		game.addMove( new Move( 2, 1 ) );
		game.addMove( new Move( 1, 3 ) );
		game.addMove( new Move( 3, 1 ) );
		game.addMove( new Move( 1, 5 ) );
		game.addMove( new Move( 5, 6 ) );
		game.addMove( new Move( 6, 4 ) );
		game.addMove( new Move( 4, 2 ) );
		game.addMove( new Move( 2, 2 ) );
		game.addMove( new Move( 2, 5 ) );
		game.addMove( new Move( 5, 4 ) );
		game.addMove( new Move( 4, 8 ) );
		game.addMove( new Move( 8, 3 ) );
		game.addMove( new Move( 3, 3 ) );
		game.addMove( new Move( 3, 6 ) );
		game.addMove( new Move( 6, 3 ) );
		game.addMove( new Move( 3, 4 ) );
		game.addMove( new Move( 4, 6 ) );	
		game.addMove( new Move( 6, 5 ) );
		game.addMove( new Move( 5, 1 ) );
		game.addMove( new Move( 5, 2 ) );
		game.addMove( new Move( 2, 6 ) );
		game.addMove( new Move( 6, 7 ) );
		game.addMove( new Move( 7, 2 ) );
		game.addMove( new Move( 2, 3 ) );
		game.addMove( new Move( 8, 2 ) );
		game.addMove( new Move( 2, 8 ) );
		game.addMove( new Move( 8, 7 ) );
		game.addMove( new Move( 7, 5 ) );
		game.addMove( new Move( 7, 7 ) );
		game.addMove( new Move( 7, 6 ) );
		game.addMove( new Move( 6, 8 ) );
		game.addMove( new Move( 8, 6 ) );
		game.addMove( new Move( 6, 6 ) );
		game.addMove( new Move( 6, 2 ) );
		game.addMove( new Move( 2, 7 ) );
		game.addMove( new Move( 7, 4 ) );

		
		when( request.getSession().getAttribute("user") ).thenReturn( opponent );
		when( userDao.queryForId( "creator" ) ).thenReturn( creator);
		when( userDao.queryForId( "opponent" ) ).thenReturn( opponent);
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 2, 'position' : 4 }" ) ) );
		
		servlet.doPut( request, response );
		
		assertTrue("Number of draws for opponent and creator is 1",creator.getNumDraws()==1&&opponent.getNumDraws()==1);
	}
	
}
