package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.ListGames;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Test cases for the ListGames servlet
 *
 * @author Colin Campbell
 */
@RunWith(MockitoJUnitRunner.class)
public class ListGamesTests {

	private ListGames servlet;

	private Lobby lobby;

	private User creator;

	private User opponent;
	
	private Map<Game,Long[]> list;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletContext context;
	@Mock
	private HttpSession session;

	@Mock
	private ServletConfig config;

	@Before
	public void setUp() throws Exception {

		lobby = new Lobby();

		creator = new User();
		creator.setIdentity( "creator" );

		opponent = new User();
		opponent.setIdentity( "opponent" );

		list=new ConcurrentHashMap<Game,Long[]>();
		when( context.getAttribute( Application.TIMEOUTS) ).thenReturn( list );
		when( context.getAttribute( Application.LOBBY ) ).thenReturn( lobby );
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession() ).thenReturn( session );
		servlet = new ListGames();
		servlet.init( config );
	}

	/**
	 * Test that the creator can not see it's own game
	 */
	@Test
	public void testCreatorListPendingGames() throws Exception {

		final StringWriter input = new StringWriter();
		
		final String gameId = lobby.createGame( creator.getIdentity(), true,null );
		 final Game game=lobby.getGameByGameId(gameId);
		    Long[] time=new Long[2];
		    time[0]=System.currentTimeMillis();
		    list.put(game, time);
		    
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getSession().getAttribute("user")).thenReturn(creator);
		servlet.doGet( request, response );

        verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertFalse( "Result should not contain the game", result.has( gameId ) );
	}
	/**
	 * Test that the game in lobby
	 */
	@Test
	public void testListPendingGames() throws Exception {

		final StringWriter input = new StringWriter();
		
		final String gameId = lobby.createGame( creator.getIdentity(), true,null );
		    
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getSession().getAttribute("user")).thenReturn(opponent);
		servlet.doGet( request, response );

        verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertTrue( "Result should contain the game", result.has( gameId ) );
	}

	

	/**
	 * Test that closed games are not returned
	 */
	@Test
	public void testListPendingGamesClosed() throws Exception {

		final StringWriter input = new StringWriter();
		final String gameId = lobby.createGame( creator.getIdentity(), false,"123" );
		lobby.joinGame( gameId, opponent.getIdentity() );

		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getSession().getAttribute("user")).thenReturn(creator);

		servlet.doGet( request, response );

        verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertFalse( "Result should not contain the game", result.has( gameId ) );
	}
	/**
	 * Test that creator left game and game will not be shown in lobby after 10 seconds
	 * @throws Exception
	 */
	@Test
	public void testTimeOut() throws Exception{
		
		final StringWriter input = new StringWriter();
		
		final String gameId = lobby.createGame( creator.getIdentity(), true,null );
		final Game game=lobby.getGameByGameId(gameId);
		    Long[] time=new Long[2];
		    time[0]=( long )2;
		    list.put(game, time);
		    
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getSession().getAttribute("user")).thenReturn(opponent);
		servlet.doGet( request, response );

        verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertFalse( "Result should contain the game", result.has( gameId ) );
	}
}
