requirejs.config({
    paths : {
    	jquery : '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
		i18n : '//cdn.jsdelivr.net/i18next/1.7.1/i18next.min',
		ace : 'lib/ace'
	},
	shim: {
	    jquery : {
	        exports: "jQuery"
	    },
	    i18n : {
			exports: "i18n"
		}
	}
});

require( [ 'jquery', 'i18n', 'tictactoe', 'toast', 'dialog', 'ace/ace', 'menu' ], function( $, i18n, TicTacToe, Toast, Dialog, ace ) {

	var TEMPLATE =  'import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;\n' +
					'import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;\n' +
					'import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;\n\n' +
					'public class CustomBot implements Robot {\n\n' +
					'\t@Override\n' +
					'\tpublic Move nextPosition( final Game game ) {\n\n' +
					'\t\treturn new Move( 1, 0 );\n' +
					'\t}\n' +
					'}\n'

	var currentBot = {};

	/* Create the Editor */
	var editor = ace.edit( 'editor-container' );
	editor.setTheme( 'ace/theme/chrome' );
	editor.getSession().setMode( 'ace/mode/java' );

	/* The Buttons */
	var newButton = $( '#new_button' );
	var openButton = $( '#open_button' );
	var saveButton = $( '#save_button' );
	var verifyButton=$( '#verify_button' );
	var titleBar = $( '#edit-bot-name' );

	i18n.init( function( translate ) {
	
		newButton.click( function() {
	
			editor.setValue( TEMPLATE );
			titleBar.text( translate('UNTITLED') );
			currentBot = {};
		});
	
		openButton.click( function() {
	
			/* GetBots */
			TicTacToe.getBots( function( bots ) {
	
				var table = $( '<table/>' );
				var header = $( '<tr/>' );
				table.append( header );
	
				var headerIcon = $( '<th/>' );
				headerIcon.attr( 'class', 'dialog-table-header-icon' );
				header.append( headerIcon );
	
				var headerName = $( '<th/>' );
				headerName.attr( 'class', 'dialog-table-header-name' );
				header.append( headerName );
	
				var dialog = new Dialog( 'Open', table );
	
				for ( var i in bots ) {
	
					var bot = bots[ i ];
	
					var row = $( '<tr/>' );
	
					var icon = $( '<td/>' );
					icon.attr( 'class', 'dialog-table-icon' );
					row.append( icon );
	
					var name = $( '<td/>' );
					name.attr( 'class', 'dialog-table-name' );
	
					var anchor = $( '<a/>' );
					anchor.text( bot.name );
					anchor.attr( 'link', '#' );
					anchor.click( function( bot ) {
						return function() {
							currentBot = bot;
							titleBar.text( bot.name );
							editor.setValue( bot.sourceCode );
							
							dialog.hide();
						};
					}( bot ));
					name.append( anchor );
					row.append( name );
					
					var iconDelete = $( '<td/>' );
					iconDelete.attr( 'class','dialog-table-iconDelete');
					iconDelete.click( function ( bot ){
						return function() {
							currentBot = bot;
							var botName =bot.name;
							TicTacToe.deleteBot( bot.identity,function( bot ){
								
								Toast.makeText( botName + ' DELETE SUCCESSFUL', 1500 ).show();
								dialog.hide();
							} );							
						};
					}( bot ));
					row.append( iconDelete );
	
					table.append( row );
				};
	
				dialog.show( $( 'body' ) );
			});
		});
	
		saveButton.click( function() {
	
			if ( !currentBot.name ) {
				currentBot.name = window.prompt( translate('NAME_BOT'), '' );
			}
	
			currentBot.sourceCode = editor.getValue();
	
			TicTacToe.createOrUpdateBot( currentBot, function( bot ) {
	
				currentBot = bot;
				titleBar.text( bot.name );
				
				Toast.makeText( bot.name + ' ' + translate( 'SAVE_SUCCESSFUL' ), 1500 ).show();
			});
		});
		
		verifyButton.click( function() {
			
			TicTacToe.verifyBotCode( currentBot.identity, function( error ){
				
				var table = $( '<table/>' );
				var header = $( '<tr/>' );
				table.append( header );
	
				var headerIcon = $( '<th/>' );
				headerIcon.attr( 'class', 'dialog-table-header-icon' );
				header.append( headerIcon );
	
				var headerName = $( '<th/>' );
				headerName.attr( 'class', 'dialog-table-header-name' );
				header.append( headerName );
	
				var dialog = new Dialog( 'Errors', table );
	
					var row = $( '<tr/>' );
	
					var icon = $( '<td/>' );
					icon.attr( 'class', 'dialog-table-icon' );
					row.append( icon );
	
					var marker = $( '<td/>' );
					marker.attr( 'class', 'dialog-table-name' );
					marker.text( 'Verified '+ error.status+"  makers: "+error.markers );
					
					row.append( marker );
					table.append( row );

					if(error.status ){
						$( '#botVerifyStatus' ).text('status: verified');
					}
	
				dialog.show( $( 'body' ) );
				
			});
			
		});
	});
});
