<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%	
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );

%>
<%@ include file="page_head_tmp.include" %>
<div id="login_info">
	<a href="https://en.gravatar.com/">
		<img class="glossy image-wrap" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="gravatar" />
	</a>
	<%= resourceBundle.getString( "CURRENTLY_LOGIN" ) %>:<br>
	<b id="me">${user.email}</b><br>
	<b><a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank"><%= resourceBundle.getString( "PROFILE" ) %></a></b>
</div>
</header>
<!--script src="js/hotSeat.js"></script-->
<script data-main="js/hotSeat" src="js/require.js"></script>
<%@ include file="page_menu_tmp.include" %>
<section>
	<table id="maingame">
		<tr>
			<td id="c0"></td>
			<td id="c1"></td>
			<td id="c2"></td>
		</tr>
		<tr>
			<td id="c3"></td>
			<td id="c4"></td>
			<td id="c5"></td>
		</tr>
		<tr>
			<td id="c6"></td>
			<td id="c7"></td>
			<td id="c8"></td>
		</tr>
	</table>
	<div id="savebutton" class="savebuttonvisable">
		<p id="printline"></p>
	</div>
</section>
<%@ include file="page_tail_tmp.include" %>
