package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;

import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test cases for the NoAccess Filter
 *
 * @author cncampbe
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NoAccessTests {

	/**
	 * Filter Under test
	 */
	private NoAccess filter;

	/**
	 * Mock Filter Chain
	 */
	@Mock
	private FilterChain chain;

	/**
	 * Mock HTTP Request
	 */
	@Mock
	private HttpServletRequest request;

	/**
	 * Mock HTTPResponse
	 */
	@Mock
	private HttpServletResponse response;

	@Before
	public void setUp() {

		filter = new NoAccess();
	}

	/**
	 * Test that the NoAccess filter sends a FORBIDDEN error.
	 */
	@Test
	public void test() throws IOException, ServletException {

		filter.doFilter( request, response, chain );

		verify( response ).sendError( HttpServletResponse.SC_FORBIDDEN );
	}
}
