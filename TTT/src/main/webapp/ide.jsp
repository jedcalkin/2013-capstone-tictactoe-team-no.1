<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%		
ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );	

%>
<%@ include file="page_head_tmp.include" %>
	<a href="https://en.gravatar.com/" title="Gravatar"><img id="user_image" src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( user.getEmail() ) %>?s=80" alt="gravatar" /></a>
	<a href="profile.jsp?userId=<%=user.getIdentity()%>" target="_blank" title="Profile"><img id="profile_link" src="./Media/cog.png" alt="profile_link" /></a>
	<input type="hidden" id ="selenium_IDE" value="<%= user.getIdentity()%>"/>
</header>
<script data-main="js/ide" src="js/require.js"></script>

<%@ include file="page_menu_tmp.include" %>
<section>
<div id="edit-bot-name"><%= resourceBundle.getString( "UNTITLED" ) %></div>
<div id="control">
<b id="control_name"><%= resourceBundle.getString( "CONTROL_PANEL" ) %></b><br>
<button type="button" id="new_button" class="ideui"><%= resourceBundle.getString( "NEW" ) %></button>
<button type="button" id="open_button" class="ideui"><%= resourceBundle.getString( "OPEN" ) %></button>
<button type="button" id="save_button" class="ideui"><%= resourceBundle.getString( "SAVE" ) %></button>
<button type="button" id="verify_button" class="ideui">Verify</button>
<div id="botVerifyStatus">status: Not Verified</div>
</div>

<div id="editor-container"></div>

</section>
<%@ include file="page_tail_tmp.include" %>
