package nz.ac.massey.cs.capstone.tictactoe.webapp.models;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import org.codehaus.commons.compiler.ErrorHandler;
import org.codehaus.commons.compiler.Location;
import org.codehaus.commons.compiler.CompileException;
import org.codehaus.commons.compiler.WarningHandler;
import org.codehaus.janino.ByteArrayClassLoader;
import org.codehaus.janino.ClassLoaderIClassLoader;
import org.codehaus.janino.Java;
import org.codehaus.janino.Parser;
import org.codehaus.janino.Scanner;
import org.codehaus.janino.UnitCompiler;
import org.codehaus.janino.util.ClassFile;
import org.objectweb.asm.ClassReader;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.j256.ormlite.field.DatabaseField;

import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;

/**
 * BotCode
 *
 * @author Colin Campbell
 */
public class BotCode implements Serializable {

	private static final List<String> WHITE_LIST = new ImmutableList.Builder<String>()
			.add( "CustomBot" )
			.add( "java.lang.Object" )
			.add( "java.lang.Integer" )
			.add( "java.lang.Boolean" )
			.add( "java.util" )
			.add( "nz.ac.massey.cs.capstone.tictactoe.gamecore.Move")
			.add( "nz.ac.massey.cs.capstone.tictactoe.gamecore.Game")
			.add( "nz.ac.massey.cs.capstone.tictactoe.bots.Robot")
			.build();

	/**
	 * A null marker collector
	 */
	private static final MarkerCollector NULL_COLLECTOR = new MarkerCollector() {

		@Override
		public void addMarker( final Marker marker ) {
			// Do nothing
		}
	};

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -376615047496466978L;

	/**
	 * Unique Identity
	 */
	@DatabaseField( generatedId = true )
	private Integer identity;

	/**
	 * User
	 */
	@DatabaseField( foreign = true )
	private User user;

	/**
	 * Verified
	 */
	@DatabaseField
	private Boolean verified;

	/**
	 * Source Code
	 */
	@DatabaseField
	private String sourceCode;

	/**
	 * Name
	 */
	@DatabaseField
	private String name;

	private transient ClassLoader classLoader;

	/**
	 * The Compiled Class
	 */
	private transient Class<?> clazz;

	/**
	 * The default constructor does not do anything
	 */
	protected BotCode() {

		// Do nothing
	}

	/**
	 * Constructs a new BotCode with source code
	 *
	 * @param sourceCode
	 */
	public BotCode( final String name, final String sourceCode ) {

		this.name = name;
		this.sourceCode = sourceCode;
		this.verified = false;
	}

	/**
	 * Gets the Identity
	 *
	 * @return the identity
	 */
	public Integer getIdentity() {

		return identity;
	}
	/**
	 * Sets the Identity
	 *
	 * @param identity the identity to set
	 */
	public void setIdentity( final Integer identity ) {

		this.identity = identity;
	}

	/**
	 * Gets the Name
	 *
	 * @return the name
	 */
	public String getName() {

		return name;
	}

	/**
	 * Sets the name
	 *
	 * @param name the name to set
	 */
	public void setName( final String name) {

		this.name = name;
	}

	/**
	 * Gets the User
	 *
	 * @return the user
	 */
	public User getUser() {

		return user;
	}

	/**
	 * Sets the User
	 *
	 * @param user
	 */
	public void setUser( final User user ) {

		this.user = user;
	}

	/**
	 * Gets Verified
	 *
	 * @return true if verified
	 */
	public Boolean getVerified() {

		return verified;
	}

	/**
	 * Sets Verified
	 *
	 * @param verified true if verified
	 */
	private void setVerified( final Boolean verified ) {

		this.verified = verified;
	}

	/**
	 * Gets the Source Code
	 *
	 * @return the sourceCode
	 */
	public String getSourceCode() {

		return sourceCode;
	}

	/**
	 * Sets the Source Code
	 *
	 * @param sourceCode the sourceCode to set
	 */
	public void setSourceCode( final String sourceCode ) {

		this.sourceCode = sourceCode;
	}

	
	public Boolean compile() throws ClassNotFoundException, CompileException, IOException{
		return compile(NULL_COLLECTOR);
	}

	public Boolean compile( final MarkerCollector collector ) throws  IOException {

		try {

			final ClassLoader parentClassLoader = Thread.currentThread().getContextClassLoader();
	
			final Java.CompilationUnit compilationUnit = new Parser( new Scanner( ( String ) null, new StringReader( sourceCode ) ) ).parseCompilationUnit();
			final UnitCompiler unitCompiler = new UnitCompiler( compilationUnit, new ClassLoaderIClassLoader( parentClassLoader ) );
			unitCompiler.setCompileErrorHandler( new ErrorHandler() {
	
				@Override
				public void handleError(String message, Location location ) throws CompileException {
					
					collector.addMarker( new Marker(message,Marker.Kind.ERROR,location) );
					
				}
				
			});
	
			unitCompiler.setWarningHandler( new WarningHandler() {
	
				@Override
				public void handleWarning(String handle, String message, Location location)
						throws CompileException {
					collector.addMarker( new Marker(message,Marker.Kind.WARNING,location) );
					
				}
				
			});
	
			final ClassFile[] classFiles = unitCompiler.compileUnit( true, true, true );
			final DependencyCollector dependencyCollector = new DependencyCollector();
	
			final Map<String, byte[]> classes = Maps.newHashMap(); 
			for ( final ClassFile file : classFiles ) {
	
				final byte[] contents = file.toByteArray();
				final ClassReader reader = new ClassReader( contents );
				reader.accept( dependencyCollector, 0 );
				classes.put( file.getThisClassName(), contents );
			}
	
			for ( final String ref : dependencyCollector.getReferenced() ) {
	
				if ( !inWhiteList(ref)) {
	
					collector.addMarker( new Marker(ref,Marker.Kind.UNSAFE,null) );
					return false;
				
				}
			}
	
			/**
			 * Set the class loader
			 */
			classLoader = new ByteArrayClassLoader( classes, parentClassLoader );
	
			setVerified( true );
			return true;

		} catch ( final CompileException e ) {

			return false;
		}
	}

	/**
	 * Constructs a new Robot
	 *
	 * @return the robot
	 *
	 * @throws InstantiationException - if the class or its nullary constructor is not accessible.
	 * @throws IllegalAccessException - if this Class represents an abstract class, an interface, an array class, a primitive type, or void; or if the class has no nullary constructor; or if the instantiation fails for some other reason.
	 * @throws ClassNotFoundException 
	 * @throws NonCompiledException 
	 * @throws NonVerifiedException 
	 */
	public Robot newBot() throws InstantiationException, IllegalAccessException, ClassNotFoundException, NonCompiledException, NonVerifiedException {

		/* Compile check */
		if ( classLoader == null ) {

			throw new NonCompiledException();
		}

		/* Verification check */
		if ( !verified ) {
			throw new NonVerifiedException();
		}

		/* Lazy initialization */
		if ( clazz == null ) {
			clazz = classLoader.loadClass( "CustomBot" );
		}

		return ( Robot ) clazz.newInstance();
	}

	public boolean isCompiled() {

		return classLoader != null;
	}

	public class NonCompiledException extends Exception {
		
		private static final long serialVersionUID = 8726207525945135424L;

		public NonCompiledException() {

			super( "BotCode is not compiled" );
		}
	}

	public class NonVerifiedException extends Exception {

		private static final long serialVersionUID = 8992001971676569031L;

		public NonVerifiedException() {

			super( "BotCode is not verified" );
		}
	}
	
	private static Boolean inWhiteList(String s){
		
		for(String w : WHITE_LIST){
			
			if(s.startsWith(w)){
				return true;
			}
				
		}
		
		return false;
		
	}

	/**
	 * BotCode hash
	
code
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identity == null) ? 0 : identity.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((sourceCode == null) ? 0 : sourceCode.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result
				+ ((verified == null) ? 0 : verified.hashCode());
		return result;
	}
	
	/**
	 * BotCode hashequal
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BotCode other = (BotCode) obj;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sourceCode == null) {
			if (other.sourceCode != null)
				return false;
		} else if (!sourceCode.equals(other.sourceCode))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (verified == null) {
			if (other.verified != null)
				return false;
		} else if (!verified.equals(other.verified))
			return false;
		return true;
	}
}
