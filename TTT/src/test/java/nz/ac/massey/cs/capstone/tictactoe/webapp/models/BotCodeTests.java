package nz.ac.massey.cs.capstone.tictactoe.webapp.models;

import static org.junit.Assert.*;

import java.io.StringWriter;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BotCodeTests {

	@Mock Game game;

	@Test
	public void testBotCode() {

		assertNull( "The default constructor should not do anything", new BotCode().getSourceCode() );
	}

	@Test
	public void testBotCodeName() {

		assertEquals( "The name should be equal to the constructor arguments", new BotCode( "MyCode", "This is some code" ).getName(), "MyCode" );
	}

	@Test
	public void testBotCodeSoureCode() {

		assertEquals( "The source code should be equal to the constructor arguments", new BotCode( "MyCode", "This is some code" ).getSourceCode(), "This is some code" );
	}

	@Test
	public void testGetIdentity() {

		assertNull( "The default identity should be null", new BotCode().getIdentity() );
	}

	@Test
	public void testSetIdentity() {

		final Integer identity = new Integer( 2 );
		final BotCode botCode = new BotCode();
		botCode.setIdentity( identity );

		assertSame( "Identity should be identity", identity, botCode.getIdentity() );
	}

	@Test
	public void testGetName() {

		assertNull( "The default name should be null", new BotCode().getName() );
	}

	@Test
	public void testSetName() {

		final String name = new String( "MyCode" );
		final BotCode botCode = new BotCode();
		botCode.setName( name );

		assertSame( "Name should be name", name, botCode.getName() );
	}

	@Test
	public void testGetUser() {

		assertNull( "The default user should be null", new BotCode().getUser() );
	}

	@Test
	public void testSetUser() {

		final User user = new User();
		final BotCode botCode = new BotCode();
		botCode.setUser( user );

		assertSame( "User should be user", user, botCode.getUser() );
	}

	@Test
	public void testGetVerified() {

		assertNull( "The default verified should be null", new BotCode().getVerified() );
	}

	@Test
	public void testSetVerified() {

		final Boolean verified = true;
		final BotCode botCode = new BotCode();
		Whitebox.setInternalState( botCode, "verified", true );

		assertSame( "BotCode should be verified", verified, botCode.getVerified() );
	}

	@Test
	public void testGetSourceCode() {

		assertNull( "The default sourceCode should be null", new BotCode().getSourceCode() );
	}

	@Test
	public void testSetSourceCode() {

		final String sourceCode = "My Awesome Code";
		final BotCode botCode = new BotCode();
		botCode.setSourceCode( sourceCode );

		assertSame( "Source code should be soureCode", sourceCode, botCode.getSourceCode() );
	}

	@Test
	public void testCompile() throws Exception {

		final StringWriter sourceCode = new StringWriter();

		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
		sourceCode.append( "public class CustomBot implements Robot {" );
		sourceCode.append( "public Move nextPosition( final Game game ) {" );
		sourceCode.append( "return new Move( 0, 0 );" );
		sourceCode.append( "}" );
		sourceCode.append( "}" );

		final BotCode botCode = new BotCode( "ZeroBot", sourceCode.toString() );
		botCode.compile() ;
		final Robot robot = botCode.newBot();

		assertEquals( "Move 0,0 should be returned", robot.nextPosition( game ), new Move( 0, 0 ) );
	}


	@Test
	public void testWrongName() throws Exception {

		final StringWriter sourceCode = new StringWriter();

		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
		sourceCode.append( "public class ZeroBot implements Robot {" );
		sourceCode.append( "public Move nextPosition( final Game game ) {" );
		sourceCode.append( "return new Move( 0, 0 );" );
		sourceCode.append( "}" );
		sourceCode.append( "}" );

		final BotCode botCode = new BotCode( "OtherBot", sourceCode.toString() );

		assertFalse( "Class with the wrong name should not compile", botCode.compile() );
	}

	@Test
	public void testSameClassNames() throws Exception {

		final StringWriter zeroBotCode = new StringWriter();

		zeroBotCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
		zeroBotCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
		zeroBotCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
		zeroBotCode.append( "public class CustomBot implements Robot {" );
		zeroBotCode.append( "public Move nextPosition( final Game game ) {" );
		zeroBotCode.append( "return new Move( 0, 0 );" );
		zeroBotCode.append( "}" );
		zeroBotCode.append( "}" );

		new BotCode( "ZeroBot", zeroBotCode.toString() ).compile();

		final StringWriter oneBotCode = new StringWriter();

		oneBotCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
		oneBotCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
		oneBotCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
		oneBotCode.append( "public class CustomBot implements Robot {" );
		oneBotCode.append( "public Move nextPosition( final Game game ) {" );
		oneBotCode.append( "return new Move( 1, 1 );" );
		oneBotCode.append( "}" );
		oneBotCode.append( "}" );

		new BotCode( "ZeroBot", oneBotCode.toString() ).compile();

		final BotCode botCode = new BotCode( "ZeroBot", zeroBotCode.toString() );
		botCode.compile();

		final Robot zeroBot = botCode.newBot();

		assertEquals( new Move( 0, 0 ), zeroBot.nextPosition( game ) );
	}
}
