package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Sets the i18next cookie to the ISO 639 language code
 *
 * @author Colin Campbell
 */
public class I18Next implements Filter {

	@Override
	public void doFilter( final ServletRequest request, final ServletResponse response, final FilterChain chain ) throws IOException, ServletException {

		/* Sanity Check */
		if ( request instanceof HttpServletRequest && response instanceof HttpServletResponse ) {

			HttpServletRequest httpRequest = ( HttpServletRequest ) request;
			HttpServletResponse httpResponse = ( HttpServletResponse ) response;
			
			final String language= httpRequest.getLocale().getLanguage();
             
			final Cookie cookie = new Cookie( "i18next", language );
			
			httpResponse.addCookie( cookie );
		}

		chain.doFilter( request, response );
	}

	@Override
	public void init( final FilterConfig fConfig ) throws ServletException {

		// Do nothing
	}

	@Override
	public void destroy() {

		// Do nothing
	}
}
