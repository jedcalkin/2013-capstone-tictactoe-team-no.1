TicTacToe (Capstone 2013)
=========================

[![Build Status](https://nerd-bd.ci.cloudbees.com/buildStatus/icon?job=capstone-tictactoe-stable)](https://nerd-bd.ci.cloudbees.com/job/capstone-tictactoe-stable/)

Live Demo
---------

For a live demo see [http://www.playtictactoewith.me/](http://www.playtictactoewith.me/)


Building
--------

To build the application run gradle the war task from the TTT directory. For example,

```
#!shell-session
cd TTT/
gradle build
```
