package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;
import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Session;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.PlayBot;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.j256.ormlite.dao.Dao;

/**
 * Test cases for the PlayBot servlet
 *
 * @author Li Sui
 * @author Colin Campbell
 */
@RunWith(MockitoJUnitRunner.class)
public class PlayBotTests {

	private Lobby lobby;

	private User creator;

	private PlayBot servlet;

	private StringWriter sourceCode;
	
	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpSession session;

	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	
	@Mock
	private Dao<BotCode, Integer> botCodeDao;
	
	@Mock
	private Game game;
	

	@Before
	public void setUp() throws ServletException {

		lobby = new Lobby();

		creator = new User();
		creator.setIdentity( "creator" );
		
		sourceCode = new StringWriter();

		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;" );
		sourceCode.append( "import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;" );
		sourceCode.append( "public class CustomBot implements Robot {" );
		sourceCode.append( "public Move nextPosition( final Game game ) {" );
		sourceCode.append( "return new Move( 1, 0 );" );
		sourceCode.append( "}" );
		sourceCode.append( "}" );

		when( context.getAttribute( Application.LOBBY ) ).thenReturn( lobby );
		when( context.getAttribute(Application.BOT_DAO)).thenReturn(botCodeDao);
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession() ).thenReturn( session );

       
		servlet = new PlayBot();
		servlet.init( config );
	}
	/**
	 * Test source gone
	 * @throws Exception
	 */
	@Test
	public void testSourceGone() throws Exception{
		lobby = mock( Lobby.class );
		final String gameId = "1";
		Map<Integer,Robot> robots =new ConcurrentHashMap<Integer,Robot>();
		
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( session.getAttribute(Session.LISTBOTS)).thenReturn(robots);
		when( lobby.updateGame(gameId, new Move(4,3))).thenReturn(false);
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' : 2 }" ) ) );
		 
		servlet.doPut( request, response );
		
		verify( response ).sendError( HttpServletResponse.SC_GONE);
	
	}
	
	/**
	 * Test that a game has been created in the lobby
	 * @throws Exception 
	 */
	@Test
	public void testCreateGame() throws Exception {

		final StringWriter input = new StringWriter();
		
		Map<Integer,Robot> robots =new ConcurrentHashMap<Integer,Robot>();
		BotCode botCode=new BotCode("name",sourceCode.toString());
		
		when( request.getParameter( "botId" ) ).thenReturn("1");
        when( session.getAttribute(Session.LISTBOTS)).thenReturn(robots);
        when( botCodeDao.queryForId(1)).thenReturn(botCode);
        when( request.getSession().getAttribute("user")).thenReturn(creator);
       
       
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doPost( request, response );

		verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertNotNull( "A game id should be returned", result.get( "gameId" ) );
	}
	
	/**
	 * Test that a game has been created in the lobby if the bot code has been compiled
	 * @throws Exception 
	 */
	@Test
	public void testCreateGameCodeHasCompiled() throws Exception {

		final StringWriter input = new StringWriter();
		Map<Integer,Robot> robots =new ConcurrentHashMap<Integer,Robot>();
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		botCode.compile();
		robots.put(1, botCode.newBot());
		when( request.getParameter( "botId" ) ).thenReturn("1");
        when( session.getAttribute(Session.LISTBOTS)).thenReturn(robots);
        when( botCodeDao.queryForId(1)).thenReturn(botCode);
        when( request.getSession().getAttribute("user")).thenReturn(creator);
       
       
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doPost( request, response );

		verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertNotNull( "A game id should be returned", result.get( "gameId" ) );
	}

	/**
	 * Test that the game is added to the lobby
	 * @throws Exception 
	 */
	@Test
	public void testCreateGameInLobby() throws Exception {

		final StringWriter input = new StringWriter();
		Map<Integer,Robot> robots =new ConcurrentHashMap<Integer,Robot>();
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		botCode.compile();
		robots.put(1, botCode.newBot());
		
		when( request.getParameter( "botId" ) ).thenReturn("1");
        when( session.getAttribute(Session.LISTBOTS)).thenReturn(robots);
        when( botCodeDao.queryForId(1)).thenReturn(botCode);
        when( request.getSession().getAttribute("user")).thenReturn(creator);
       
       
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doPost( request, response );

		verify( response ).setContentType( "application/json" );

		final String gameId = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject()
			.get( "gameId" )
			.getAsString();

		assertNotNull( "The game should be in the lobby", lobby.getGameByGameId( gameId ) );
	}

	/**
	 * Test for desired next move return;
	 * @throws Exception 
	 */
	@Test
	public void testUpdateGame() throws Exception{
		
		final String gameId = lobby.createGame(  creator.getIdentity(), false,"123" );
		lobby.joinGame( gameId,"1" );
		final StringWriter input = new StringWriter();
		Map<Integer,Robot> robots =new ConcurrentHashMap<Integer,Robot>();
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		botCode.compile();
		robots.put(1, botCode.newBot());
		
        when( session.getAttribute(Session.LISTBOTS)).thenReturn(robots);
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' : 1 }" ) ) );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		servlet.doPut( request, response );

		assertNotNull( "response should contain a bot move", new JsonParser().parse(input.toString()).getAsJsonObject());
	}
	/**
	 * Test for bot move be contained in game;
	 * @throws Exception 
	 */
	@Test
	public void testGameContainBotMove() throws Exception{

		final String gameId = lobby.createGame(  creator.getIdentity(), false,"123" );
		lobby.joinGame( gameId,"1" );
		final StringWriter input = new StringWriter();
		Map<Integer,Robot> robots =new ConcurrentHashMap<Integer,Robot>();
		
		BotCode botCode=new BotCode("name",sourceCode.toString());
		botCode.compile();
		robots.put(1, botCode.newBot());
		
        when( session.getAttribute(Session.LISTBOTS)).thenReturn(robots);
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' :1 }" ) ) );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		
		servlet.doPut( request, response );

		final Move botMove= new Move(Integer.parseInt(new JsonParser().parse(input.toString()).getAsJsonObject().get("nestedGame").toString()),
				Integer.parseInt(new JsonParser().parse(input.toString()).getAsJsonObject().get("position").toString()));
		
		assertTrue( "Game should contain a bot move", lobby.getGameByGameId(gameId).getMoves().contains(botMove));
	}
	/**
	 * Test InvalidMove
	 * @throws Exception
	 */
	@Test
	public void testBadRequest() throws Exception{
		
		final String gameId = lobby.createGame(  creator.getIdentity(), false,"123" );
		
		lobby.joinGame( gameId,"1" );
		lobby.updateGame(gameId, new Move(4,3));
		
		Map<Integer,Robot> robots =new ConcurrentHashMap<Integer,Robot>();
		BotCode botCode=new BotCode("name",sourceCode.toString());
		botCode.compile();
		robots.put(1, botCode.newBot());
		
		when( session.getAttribute(Session.LISTBOTS)).thenReturn(robots);	      
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( request.getReader() ).thenReturn( new BufferedReader( new StringReader( "{ 'nestedGame' : 4, 'position' : 2 }" ) ) );
		 
		servlet.doPut( request, response );
		
		verify( response ).sendError( HttpServletResponse.SC_BAD_REQUEST);
		assertTrue( "Game already contained the move", lobby.getGameByGameId( gameId ).getMoves().contains( new Move( 4, 3 ) ) );
	}
}
