package nz.ac.massey.cs.capstone.tictactoe.webapp.listeners;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.bots.Robot;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
/**
 * 
 * @author Li Sui
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class SessionTests {

	private Session sessionListenter;
	
	private Lobby lobby;
	
	private User user;
	
	private Map<Game,Long[]> list;

	@Mock
	private HttpSessionEvent  sessionEvent;
	
	@Mock
	private HttpSession session;
	
	@Mock
	private  ServletContext context;

	@Before
	public void setUp() {
		
		lobby = new Lobby();
		user= new User();
		sessionListenter = new Session();
		list=new ConcurrentHashMap<Game,Long[]>();
		when( sessionEvent.getSession() ).thenReturn( session );
		when( session.getServletContext() ).thenReturn( context );
		when(context.getAttribute( Application.LOBBY )).thenReturn(lobby);
		when( context.getAttribute( Application.TIMEOUTS )).thenReturn(list);
		
	}

	/**
	 * Test that application destroys the lobby
	 */
	@Test
	public void testContextDestroyed() {
		
		user.setIdentity("id");
		final String gameId= lobby.createGame(user.getIdentity(), true, null);
		final Game game=lobby.getGameByGameId(gameId);
		Long[] time=new Long[2];
		time[0]=(long) 1;
		list.put(game, time);
		
		when( session.getAttribute("user")).thenReturn(user);
		sessionListenter.sessionDestroyed( sessionEvent );
		
		assertTrue("game been removed",lobby.getPendingGames().size()==0);
		
	}
	
	/**
	 * Test that application destroys the lobby but no games be created
	 */
	@Test
	public void testContextDestroyedIfGameNull() {
		
		user.setIdentity("id");
		
		when( session.getAttribute("user")).thenReturn(user);
		sessionListenter.sessionDestroyed( sessionEvent );
		
		assertTrue("game been removed",lobby.getPendingGames().size()==0);
		
	}
	/**
	 * Test that session be created
	 */
	@Test
	public void testSessionCreated(){
		
		sessionListenter.sessionCreated(sessionEvent);
		
		verify(session).setAttribute(Session.LISTBOTS, new ConcurrentHashMap<Integer, Robot>());
	}
}
