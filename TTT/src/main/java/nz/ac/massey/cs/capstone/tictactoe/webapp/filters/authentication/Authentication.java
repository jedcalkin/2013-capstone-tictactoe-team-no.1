package nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication;

import javax.servlet.Filter;

public interface Authentication extends Filter {

	public static final String USER = "user";
}
