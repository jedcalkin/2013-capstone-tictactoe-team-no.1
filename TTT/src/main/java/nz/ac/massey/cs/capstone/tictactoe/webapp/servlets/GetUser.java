package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;


/**
 * Servlet for Getting a User
 *
 * @author Colin Campbell
 */
public class GetUser extends HttpServlet {

	/** Gson Object */
	private Gson gson;

	/** The User Data Access Object */
	private Dao<User, String> userDao;

	private static final String PARAM_USERID = "userId";

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 6114981225224037392L;

	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		try {

			/* Get user */
			final User user = userDao.queryForId( request.getParameter( PARAM_USERID ) );

			/* Write User as JSON */
			gson.toJson( new UserWrapper( user ), response.getWriter() );

		} catch ( SQLException e ) {

			throw new IOException( e );
		}
	
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize gson */
		gson = new Gson();

		/* Initialize User DAO */
		userDao = ( Dao<User, String> ) config.getServletContext().getAttribute( Application.USER_DAO );
	}
	/**
	 * 
	 * @author Li Sui
	 *
	 */
	@SuppressWarnings("unused")
	private class UserWrapper{
		
		public final String identity;
		public final String displayName;
		public final String email;
		public final Integer numWins;
		public final Integer numDraws;
		public final Integer numLoses;
		public final String gender;
		public final String location;
		public final String aboutMe;
		public final String age;
		
		public UserWrapper(final User user){
			
			identity=user.getIdentity();
			displayName=user.getDisplayName();
			email=user.getEmail();
			numWins=user.getNumWins();
			numDraws=user.getNumDraws();
			numLoses=user.getNumLoses();
			gender=user.getGender();
			location=user.getLocation();
			aboutMe=user.getAboutMe();
			age=user.getAge();
			
		}
		
	}
}
