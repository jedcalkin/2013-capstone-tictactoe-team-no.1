package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.JoinGame;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Test cases for the JoinGame servlet
 *
 * @author Li Sui
 * @author Colin Campbell
 */
@RunWith(MockitoJUnitRunner.class)
public class JoinGameTest {

	private Lobby lobby;

	private User creator;

	private User opponent;

	private JoinGame servlet;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpSession session;

	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	
	@Before
	public void setUp() throws ServletException{

		lobby = new Lobby();

		creator = new User();
		creator.setIdentity( "creator" );

		opponent = new User();
		opponent.setIdentity( "opponent" );

		when( context.getAttribute( Application.LOBBY ) ).thenReturn( lobby );
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession() ).thenReturn( session );

		servlet = new JoinGame();
		servlet.init( config );
	}

	/**
	 * Test join private game returns a gson object with the
	 * expected opponentId
	 */
	@Test
	public void testJoinPrivateGame() throws Exception {

		final StringWriter input = new StringWriter();
		final String gameId = lobby.createGame( creator.getIdentity(), false,"123" );

		when( request.getSession().getAttribute("user")).thenReturn(opponent);
		when( request.getParameter( "gameId" ) ).thenReturn( gameId );
		when( request.getParameter( "password" ) ).thenReturn( "123" );

		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doGet( request, response );

        verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertEquals( "The oppoent id should be opponent", "opponent", ( result.get( "opponentId" )).getAsString() );
	}

	/**
	 * Test that join game updates the game object
	 */
	@Test
	public void testJoinGameUpdatesGame() throws Exception {
		
		final StringWriter input = new StringWriter();
		final String gameId = lobby.createGame( creator.getIdentity(), false,"123" );
		final Game game = lobby.getGameByGameId( gameId );
		when( request.getSession().getAttribute("user")).thenReturn(opponent);
		when( request.getParameter( "gameId" ) ).thenReturn( gameId );
		when( request.getParameter( "password" ) ).thenReturn( "123" );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doGet( request, response );

		verify( response ).setContentType( "application/json" );

		assertEquals( "The oppoent is should be opponent", "opponent", game.getOpponentId() );
	}
	
	/**
	 * Test join public game returns a gson object with the
	 * expected opponentId
	 */
	@Test
	public void testJoinPublicGame() throws Exception {

		final StringWriter input = new StringWriter();
		final String gameId = lobby.createGame( creator.getIdentity(), true,"public" );

		when( request.getSession().getAttribute("user")).thenReturn(opponent);
		when( request.getParameter( "gameId" ) ).thenReturn( gameId );
		when( request.getParameter( "password" ) ).thenReturn( "public" );

		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doGet( request, response );

        verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertEquals( "The oppoent id should be opponent", "opponent", ( result.get( "opponentId" )).getAsString() );
	}
	
	/**
	 * Test join private game returns a gson object with the
	 * expected opponentId
	 */
	@Test
	public void testJoinPrivateGameWithIncorrectPassword() throws Exception {

		final StringWriter input = new StringWriter();
		final String gameId = lobby.createGame( creator.getIdentity(), false,"123" );

		when( request.getSession().getAttribute("user")).thenReturn(opponent);
		when( request.getParameter( "gameId" ) ).thenReturn( gameId );
		when( request.getParameter( "password" ) ).thenReturn( "12" );

		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doGet( request, response );

        verify( response ).sendError(HttpServletResponse.SC_FORBIDDEN );

	}

}
