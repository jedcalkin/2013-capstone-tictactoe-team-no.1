package nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import com.j256.ormlite.dao.Dao;

public class AuthenticationByPass implements Authentication {

	/**
	 * User Data Access Object
	 */
	private Dao<User, String> userDao;

	/**
	 * Jabber Wocky Parameter
	 */
	private static final String PARAM_JABBERWOCKY = "jabberwocky";

	/**
	 * ID Parameter
	 */
	private static final String PARAM_ID = "id";

	/**
	 * ByPass Key
	 */
	private static final String SECRET_KEY = "2BFBA931BA4D2C77DCD13155647AA";

	/**
	 * Test user red
	 */
	private User red;

	/**
	 * Test user blue
	 */
	private User blue;

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {

		/* Sanity Check */
		if ( request instanceof HttpServletRequest ) {

			HttpServletRequest httpRequest = ( HttpServletRequest ) request;

			if ( SECRET_KEY.equals( httpRequest.getParameter( PARAM_JABBERWOCKY ) ) ) {

				if ( "red".equals( httpRequest.getParameter( PARAM_ID ) ) ) {
					
					httpRequest.getSession().setAttribute( Authentication.USER , red );

				}

				if ( "blue".equals( httpRequest.getParameter( PARAM_ID ) ) ) {
					httpRequest.getSession().setAttribute( Authentication.USER , blue );
				}
			}
		}
		chain.doFilter( request, response );
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init( FilterConfig filterConfig ) throws ServletException {

		userDao = ( Dao<User, String> ) filterConfig.getServletContext().getAttribute( Application.USER_DAO );

		red = new User();
		red.setIdentity( "red" );
		red.setDisplayName( "red" );
		red.setEmail( "644022590@qq.com" );
		red.setAge( "13" );
		red.setAboutMe( "I'm test user red" );
		red.setGender( "Male" );
		red.setLocation( "Red Town" );
		red.setNumDraws( 3 );
		red.setNumLoses( 4 );
		red.setNumWins( 2 );

		blue = new User();
		blue.setIdentity( "blue" );
		blue.setDisplayName( "blue" );
		blue.setEmail( "644022590@qq.com" );
		blue.setAge( "31" );
		blue.setAboutMe( "I'm test user blue" );
		blue.setGender( "Female" );
		blue.setLocation( "Bluevile" );
		blue.setNumDraws( 3 );
		blue.setNumLoses( 2 );
		blue.setNumWins( 4 );

		try {

				userDao.createIfNotExists( red );

				userDao.createIfNotExists( blue );


		} catch ( SQLException e ) {

			throw new ServletException( e );
		}
	}

	@Override
	public void destroy() {

		try {

			userDao.delete( red );
			userDao.delete( blue );

		} catch ( SQLException e ) {

			e.printStackTrace();
		}
	}
}
