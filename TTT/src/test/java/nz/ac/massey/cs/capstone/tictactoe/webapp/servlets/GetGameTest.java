package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.GetGame;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Test cases for the GetGame servlet
 *
 * @author Li Sui
 * @author Colin Campbell
 */
@RunWith(MockitoJUnitRunner.class)
public class GetGameTest {

	private Lobby lobby;

	private GetGame servlet;

	private User creator;

	private User opponent;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;

	@Before
	public void setUp() throws ServletException{

		lobby = new Lobby();

		creator = new User();
		creator.setIdentity( "creator" );

		opponent = new User();
		opponent.setIdentity( "opponent" );

		when( context.getAttribute( Application.LOBBY ) ).thenReturn( lobby );
		when( config.getServletContext() ).thenReturn( context );

		servlet = new GetGame();
		servlet.init( config );
	}

	/**
	 * Test that get game returns the game
	 */
	@Test
	public void getGetGame() throws Exception {

		final String gameId = lobby.createGame( creator.getIdentity(), true,null );
		lobby.joinGame( gameId, opponent.getIdentity() );

		final StringWriter input = new StringWriter();

		when( request.getParameter( "gameId" ) ).thenReturn( gameId );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );

		servlet.doGet( request, response );

        verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();
		assertEquals("should get creator id creator","creator", ( result.get( "creatorId" )).getAsString() );
	}
}
