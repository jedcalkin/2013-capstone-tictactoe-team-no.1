package nz.ac.massey.cs.capstone.tictactoe.bots;


import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Move;

/**
 * A Robot can supply its next move
 *
 * @author Colin Campbell
 */
public interface Robot {

	/**
	 * Gets the next position the robot want to play
	 *
	 * @param game the game
	 * @return the position
	 */
	Move nextPosition( Game game );
}
