package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * NoAccess Filter
 *
 * @author Colin Campbell
 */
public final class NoAccess implements Filter {

	@Override
	public void doFilter( final ServletRequest request, final ServletResponse response, final FilterChain chain ) throws IOException, ServletException {

		if ( response instanceof HttpServletResponse ) {

			( ( HttpServletResponse ) response ).sendError( HttpServletResponse.SC_FORBIDDEN );
		}
	}

	@Override
	public void init( final FilterConfig filterConfig ) throws ServletException {

		//Do nothing
	}

	@Override
	public void destroy() {

		// Do nothing
	}
}
