package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.filters.authentication.Authentication;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import com.google.gson.Gson;

/**
 * Servlet for joining a game
 *
 * @author Li Sui
 * @author Colin Campbell
 */
public class JoinGame extends HttpServlet {

	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = 95810419797917245L;

	/** Gson Object */
	private Gson gson;

	/** The lobby */
	private Lobby lobby;

	/** Game ID Parameter */
	private static final String PARAM_GAMEID = "gameId";
	
	/** Password Parameter */
	private static final String PARAM_PASSWORD = "password";

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json";

	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {

		/* Content Type */
		response.setContentType( CONTENT_TYPE );

		/* Get the user */
		final User user = ( User ) request.getSession().getAttribute( Authentication.USER );

		/* Get the GameID */
		final String gameId = request.getParameter( PARAM_GAMEID );
		
		/* Get the password */
		final String password = request.getParameter( PARAM_PASSWORD );
		/* Get the Game */
		final Game game = lobby.getGameByGameId( gameId );
		
			if ( game.getPassword().equals(password) ){
					
				if ( lobby.joinGame( gameId, user.getIdentity() ) ) {
	
					/* Write Result */
					gson.toJson( game, response.getWriter() );
				}
			}else{
				response.sendError( HttpServletResponse.SC_FORBIDDEN );
			}
	}

	@Override
	public void init( final ServletConfig config ) throws ServletException {

		super.init( config );

		/* Initialize gson */
		gson = new Gson();

		/* Initialize lobby */
		lobby = ( Lobby ) getServletContext().getAttribute( Application.LOBBY );
	}
}
