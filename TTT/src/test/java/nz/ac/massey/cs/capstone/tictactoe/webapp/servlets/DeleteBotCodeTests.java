package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.j256.ormlite.dao.Dao;

/**
 * @author Li Sui
 */
@RunWith( MockitoJUnitRunner.class )
public class DeleteBotCodeTests {

	private DeleteBotCode servlet;

	@Mock
    private User user;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	
	@Mock
	private Dao<BotCode, Integer> botDao;
	
	@Mock
    private HttpSession session;
	
	@Mock
	private Dao<User, String> userDao;

	@Before
	public void setUp() throws Exception {

		when( context.getAttribute( Application.USER_DAO ) ).thenReturn( userDao );
		when( context.getAttribute( Application.BOT_DAO ) ).thenReturn( botDao );
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession() ).thenReturn( session );
		when( session.getAttribute( "user" ) ).thenReturn( user );
		when( user.getIdentity() ).thenReturn( "Li" );

		servlet = new DeleteBotCode();
		servlet.init( config );
	}

	/**
	 * Test delete bot code
	 */
	@Test
	public void testDelete() throws Exception {

		when( request.getParameter( "botId") ).thenReturn( "1" );
		
		servlet.doDelete( request, response );

		verify( botDao ).deleteById(1);
	}

	/**
	 * Test exceptions
	 */
	@SuppressWarnings("unchecked")
	@Test(expected= ServletException.class)
	public void testException() throws Exception {

		when( request.getParameter( "botId") ).thenReturn( "1" );
		when( botDao.deleteById(1)).thenThrow(SQLException.class);
		
		servlet.doDelete( request, response );
	}

}
