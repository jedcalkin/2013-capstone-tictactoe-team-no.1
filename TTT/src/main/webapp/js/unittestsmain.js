"use strict";
require.config({
    paths: {
        'Sinon': 'lib/sinon',
        'QUnit': 'lib/qunit',  
    },

    shim: {

      'Sinon' : {
          exports: 'Sinon',
      },

       'QUnit': {
           exports: 'QUnit',
           init: function() {
               QUnit.config.autoload = false;
               QUnit.config.autostart = false;
           }
       }, 
    }
});
// require the unit tests.
require(
    ['Sinon','QUnit','tests/tictactoetests'],
    function(Sinon, QUnit, ttttest) {
        // run the tests.
        ttttest.run();

        // start QUnit.
        QUnit.load();
        QUnit.start();
    }
);