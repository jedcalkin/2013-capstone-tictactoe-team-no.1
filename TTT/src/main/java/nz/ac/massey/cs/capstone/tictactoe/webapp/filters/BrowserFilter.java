package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Li Sui
 */
public class BrowserFilter implements Filter {

	//TODO: improve this filter
    private static final String[] DEFAULT_BROWSERS =
    { "Chrome", "Firefox", "Safari"};
 
    // Filter param keys
    public static final String KEY_BROWSER_IDS = "browserIds";
    public static final String KEY_BAD_BROWSER_URL = "badBrowser";
 
    // Configured params
    private String[] browserIds;
    private String badBrowserUrl;
 
    @Override
    public void init(FilterConfig cfg) throws ServletException
    {
        String ids = cfg.getInitParameter(KEY_BROWSER_IDS);
        this.browserIds = (ids != null)?ids.split(","):DEFAULT_BROWSERS;
 
        badBrowserUrl = cfg.getInitParameter(KEY_BAD_BROWSER_URL);
        if (badBrowserUrl == null)
        {
            throw new IllegalArgumentException("BrowserFilter requires param badBrowserUrl");
        }
    }
 
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
        throws IOException, ServletException
    {
    	final RequestDispatcher rd = ((HttpServletRequest) req).getRequestDispatcher(this.badBrowserUrl);
        String userAgent = ((HttpServletRequest) req).getHeader("User-Agent");
        for (String browser_id : browserIds)
        {
            if (userAgent.contains(browser_id))
            {
            		chain.doFilter(req, resp);
                    return;
            }
        }
        
        rd.forward(req, resp);
    }
 
    @Override
    public void destroy()
    {
        this.browserIds = null;
    }

}
