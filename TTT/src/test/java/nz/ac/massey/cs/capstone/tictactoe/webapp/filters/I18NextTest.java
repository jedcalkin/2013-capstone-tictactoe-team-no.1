package nz.ac.massey.cs.capstone.tictactoe.webapp.filters;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test cases for the I18Next Filter
 *
 * @author Li Sui
 * @author Colin Campbell
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class I18NextTest {

	/**
	 * Filter Under test
	 */
	private I18Next filter;

	/**
	 * Mock Filter Chain
	 */
	@Mock
	private FilterChain chain;

	/**
	 * Mock HTTP Request
	 */
	@Mock
	private HttpServletRequest request;

	/**
	 * Mock HTTPResponse
	 */
	@Mock
	private HttpServletResponse response;

	@Before
	public void setUp() {

		filter = new I18Next();
	}

	/**
	 * Tests that a request with a Chinese Locale sets a
	 * cookie with the value of zh.
	 */
	@Test
	public void testChinese() throws Exception {

		final Locale locale = Locale.CHINESE;
		when( request.getLocale() ).thenReturn( locale );

		final ArgumentCaptor<Cookie> cookieCaptor = ArgumentCaptor.forClass( Cookie.class );
	
		filter.doFilter( request, response, chain );

		verify( response ).addCookie( cookieCaptor.capture() );

		final Cookie cookie = cookieCaptor.getValue();

		assertEquals( "zh", cookie.getValue() );
	}

	/**
	 * Tests that a request with a English Locale sets a
	 * cookie with the value of en.
	 */
	@Test
	public void testEnglish() throws Exception {

		final Locale locale = Locale.ENGLISH;
		when( request.getLocale() ).thenReturn( locale );

		final ArgumentCaptor<Cookie> cookieCaptor = ArgumentCaptor.forClass( Cookie.class );
	
		filter.doFilter( request, response, chain );

		verify( response ).addCookie( cookieCaptor.capture() );

		final Cookie cookie = cookieCaptor.getValue();

		assertEquals( "en", cookie.getValue() );
	}

	@Test
	public void testCookiesName() throws Exception {

		final Locale locale = Locale.ENGLISH;
		when( request.getLocale() ).thenReturn( locale );

		final ArgumentCaptor<Cookie> cookieCaptor = ArgumentCaptor.forClass( Cookie.class );
	
		filter.doFilter( request, response, chain );

		verify( response ).addCookie( cookieCaptor.capture() );

		final Cookie cookie = cookieCaptor.getValue();

		assertEquals( "i18next", cookie.getName() );
	}
}
