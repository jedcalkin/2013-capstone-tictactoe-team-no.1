requirejs.config({
		paths : {
			jquery : '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
			i18n : '//cdn.jsdelivr.net/i18next/1.7.1/i18next.min',
			d3 : '//d3js.org/d3.v3.min',
		    purl : '//cdn.jsdelivr.net/jquery.url.parser/2.2.1/purl',
		    crypto : '//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5'
	},
	shim: {
			jquery : {
					exports: "jQuery"
			},
			i18n : {
				exports: "i18n"
			},
			purl : {
				deps: ['jquery']
			},
			d3 : {
				exports: 'd3'
			},
			crypto : {
				exports: 'CryptoJS'
			}
	}
});

require( ['jquery', 'i18n', 'tictactoe', 'toast', 'd3', 'crypto', 'purl', 'menu'], function( $, i18n, TicTacToe, Toast, d3, CryptoJS ) {

	i18n.init( function( translate ) {
	
	/**
	 * Converts the game to a d3 data type
	 *
	 * TODO: Re-factor
	 */
	function convert( game ) {

		/* Nested Game */
		var nestedGame = 0;

		/* For each row of the board */
		return game.board.map( function( columns, row ) {

			/* For each cell of the board	*/
			return columns.map( function( cell ) {

				/* Increment nested game */
				nestedGame++;

				/* Closure */
				return function( game, nestedGame ) {

					/**
					 * Moves
					 */
					var moves = game.moves;

					/**
					 * Inner board
					 */
					var innerBoard = game.nestedBoards[ nestedGame ];

					/**
					 * New board object
					 */
					return {

						/**
						 * Determines if the inner board can be played on
						 */
						isOpen : function() {

						var yourturn = false;
						if(game.moves.length%2==0){
							
							if(side()=='cross'){
								yourturn = true;
							}
						}else{
							
							if(side()=='nought'){
								yourturn = true;
							}
						}

							if(yourturn){
								/* If game is empty */
								if ( moves.length == 0 ) {
									return nestedGame == 4;
								};
								last_move = game.moves[game.moves.length-1].position
								if(game.board[parseInt(last_move/3)][last_move%3]!=null){
									return game.board[parseInt(nestedGame/3)][nestedGame%3]==null;
								}
								return last_move == nestedGame;
							}
							return false;
						},

						/**
						 * Gets cell corresponding with the inner board
						 */
						getCell : function() {

							return cell;
						},

						/**
						 * Gets the inner board
						 */
						getInnerBoard : function() {

							/**
							 * Board
							 */
							var board = this;

							/**
							 * Position
							 */
							var position = 0;

							/* For each row of the board */
							return innerBoard.map( function( columns, row ) {


								/* For each cell of the board	*/
								return columns.map( function( cell ) {

									/* Increment position */
									position++;

									/* Closure */
									return function( cell, game, board, nestedGame, position ) {

										/**
										 * The moves
										 */
										var moves = game.moves;

										return {

											/**
											 * The value of the cell
											 */
											getValue : function() {

												return cell;
											},

												/**
												 * Determines if the cell can be played on
												 */
												isOpen : function() {

												if ( board.isOpen() ) {
													/* If game is empty */
													if ( moves.length == 0 ) {
														/* All cells except form for are open */
														return position != 4;
													}
													else{
															if(this.getValue()=='NOUGHT'){
																	return false;
															}else{
																	if(this.getValue()=='CROSS'){
																		return false;
																	}else{
																		return true;
																	}
															}
													}
												}return false;
											},

											lastmove : function() {
												return moves[moves.length-1];
											},
											/**
											 * The move corresponding to the cell
											 */
											move : {

													/**
													 * The corresponding inner game
													 */
													nestedGame : nestedGame,

													/**
													 * The position within the inner game
													 */
													position : position
											}
										};
									}( cell, game, board, nestedGame, position - 1 );
								});
							});
						}
					};
				}( game, nestedGame - 1 );
			});
		});
	};

	/**
	 * Game Id
	 */
	var gameId = $.url().param('gameId');

	/**
	 * Table
	 */
	var waiting = true;
	var table = d3.select("#maingame");
	var p2d = $('#player2');
	var tictactoe = new TicTacToe( gameId, "ignoredParamater" )
		.onUpdate( function( game ){
			if ( waiting  ) {
				if(game.opponentId!=null){
					p2d.empty();
					var name=$('<b/>');
					var link=$('<a/>');
					var nextline=$('<br>');
					var image=$('<img/>');
					$.get("GetUser", {"userId":game.opponentId }, function(user){
						name.text(user.displayName);
						link.attr('href','profile.jsp?userId='+game.opponentId);
						link.attr('target','_blank');
						link.attr('title','View Opponent Profile')
						name.attr('id', 'opponent');
						image.attr('src','http://www.gravatar.com/avatar/'+CryptoJS.MD5(user.email)+'?s=156');
			    	  });
					
					p2d.append(name);
					p2d.append(nextline);
					p2d.append(link)
					link.append(image);
					waiting = false;					
				}
				
			}
			display( game );
		})
		.isAlive(function(data){
			if(!data.alive){
				Toast.makeText( translate( 'OPPONENT_LEFT' ), 5000 ).show();
				window.location.href='index.jsp';
			}
			
		});
	
	
	var display = function( game ) {
			$('#maingame').empty();
			if(game.state=="INCOMPLETE"){
				game = convert( game );
				/**
				 * TRs
				 */
				var trs = table.selectAll('tr')
					.data( game )
					.enter()
					.append('tr');

				/**
				 * Inner Tables
				 */
				var innerTables = trs.selectAll('td')
					.data( function ( row ) {
						return row;
					})
					.enter()
					.append('td')
						.attr('class', function( innerGame ){
						s = 'bob';
						if(innerGame.getCell()=='NOUGHT'){
							s = 'nought';
						}
						if(innerGame.getCell()=='CROSS'){
							s = 'cross';
						}
						return s;
					})
					.append( 'table' )
					.attr('class', function( innerGame ){
						if(innerGame.getCell()){
							return 'done subgame';
						}else{
							return 'subgame';
						}
					})
					.attr('background-color', function( innerGame ) {
						if ( innerGame.isOpen() ) {
							return 'blue';
						};
					});

				/**
				 * Inner TRs
				 */
				var innerTrs = innerTables.selectAll('tr')
					.data( function( row ) {
						return row.getInnerBoard();
					})
					.enter()
					.append('tr');

				/**
				 * Inner TDs
				 */
				var innerTds = innerTrs.selectAll('td')
					.data( function( cell ) {
						return cell;
					})
					.enter()
					.append('td');

				innerTds.attr('class', function( cell ) {

					if ( cell.isOpen() ) {
						return 'open'
					}

					if ( cell.getValue() === 'CROSS' ) {
						if((cell.move.nestedGame == cell.lastmove().nestedGame)&&(cell.move.position == cell.lastmove().position)){
							return 'cross_th';
						}else{
							return 'cross_mini';
						}
					}

					if ( cell.getValue() == 'NOUGHT' ) {
						if((cell.move.nestedGame == cell.lastmove().nestedGame)&&(cell.move.position == cell.lastmove().position)){
							return 'nought_th';
						}else{
							return 'nought_mini';
						}
					}

					return '';
				});

				innerTds.on( 'mouseover', function( cell ) {

					if ( cell.isOpen() ) {
						if(side()=='cross'){
							d3.select( this ).style( 'background-image', 'url(Media/cross.svg)' )
							 .style( 'background-size', '44px 44px')
							 .style( 'opacity', '0.8');
						}else{
							d3.select( this ).style( 'background-image', 'url(Media/nought.svg)' )
							 .style( 'background-size', '44px 44px')
							 .style( 'opacity', '0.8');
						}
					};
				});

				innerTds.on( 'mouseout', function( cell ) {

					if ( cell.isOpen() ) {
						d3.select( this ).style( 'background', 'white' )
										 .style( 'opacity', '1');
					};
				});

				/**
				 * Called when a user clicks on a cell
				 */
				innerTds.on('click', function( cell ) {

					/* Can only click if cell is open */
					if ( cell.isOpen() ) {
						d3.select( this )
							.attr('class', side()+'_th');
						tictactoe.update( cell.move );
						cell.isOpen = function() {
							return false;
						};
					}
				});
			}else if (game.state=="DRAW"){
				message = translate( 'DRAW' );
				timeout = 0;
				board = document.getElementById('maingame');
				for(i in game.moves){
					id = '#'+game.moves[i]['nestedGame']+game.moves[i]['position'] 
					window.localStorage.setItem('m'+i, id);
				}
				window.localStorage.setItem('m'+game.moves.length, "end");
				board.innerHTML = '<tr><td class="draw_won">'+message+' <a href="replay.jsp"> <button> ' + translate('REPLAY')+'?</button></a></td></tr>';
				TicTacToe.stoppolling();
			}else{
				board = document.getElementById('maingame')
				timeout = 0;
				if(game.moves.length%2==0){
					winner = 'nought'; 
					message = $('#opponent').text()+' '+translate('WON');
				}else{
					winner = 'cross';
					message = $('#creator').text()+' '+translate('WON');
				}
				TicTacToe.stoppolling();
				for(i in game.moves){
					id = '#'+game.moves[i]['nestedGame']+game.moves[i]['position'] 
					window.localStorage.setItem('m'+i, id);
				}
				window.localStorage.setItem('m'+game.moves.length, "end");
				board.innerHTML = '<tr><td class="'+winner+'_won">'+message+' <a href="replay.jsp"> <button> ' + translate('REPLAY')+'?</button></a></td></tr>';
			}
		}

		/* Am I nought or cross */
		var side = function(){
				if(document.getElementById('me').value==document.getElementById('creatorTrun').value){
					return 'cross';
				}
				else{
					return 'nought';
				}
			};

	TicTacToe.getGame( gameId, function( game ){
			display( game );
		});


	$(window).on('beforeunload', function(){
  		//return 'Leaving this page will end your current game.';
	});

});
});
