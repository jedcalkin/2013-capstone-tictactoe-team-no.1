package nz.ac.massey.cs.capstone.tictactoe.webapp.models;

/**
 * 
 * @author Li Sui
 *
 */
public interface MarkerCollector {
	
	public void addMarker(Marker marker);

}
