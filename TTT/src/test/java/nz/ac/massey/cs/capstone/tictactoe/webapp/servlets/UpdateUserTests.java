package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.UpdateUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.j256.ormlite.dao.Dao;

/**
 * @author Siyan Li
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class UpdateUserTests {
	
	private UpdateUser servlet;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpSession session;
	
	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	
	@Mock
	private Dao<User,String> userDao;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		when( context.getAttribute( Application.USER_DAO ) ).thenReturn( userDao );
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession() ).thenReturn( session );

		servlet = new UpdateUser();
		servlet.init( config );
	}

	/**
	 * Test method for update user {@link nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.UpdateUser#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
	 */
	@Test
	public void testUpdateUser() throws Exception {

		final User user = new User();

		when ( session.getAttribute( "user" ) ).thenReturn( user );
		when( request.getParameter( "displayName" ) ).thenReturn( "tom" );
		when( request.getParameter( "location" ) ).thenReturn( "wellingtion" );
		when( request.getParameter( "gender" ) ).thenReturn( "male" );
		when( request.getParameter( "aboutMe" ) ).thenReturn( "i am a good guy" );
		when( request.getParameter( "age" ) ).thenReturn( "22" );
		
		
        servlet.doPut( request, response );

        assertEquals( "tom"+"wellingtion"+"male"+"i am a good guy"+"22", user.getDisplayName()+user.getLocation()+user.getGender()+user.getAboutMe()+user.getAge() );
	}

}
