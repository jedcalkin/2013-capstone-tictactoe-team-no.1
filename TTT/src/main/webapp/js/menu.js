﻿define( ['jquery', 'i18n', 'tictactoe'], function( $, i18n, TicTacToe ) {
	
	i18n.init( function( translate ) {

	$('#createPublicGame').click( function() {
		TicTacToe.createGame(  true, null, function( game ) {
			window.location.href='game.jsp?gameId=' + game.gameId;
		});
	});

	$('#createPrivateGame').click( function() {
		var password = prompt( translate( 'SET_PASSWORD' ),translate( 'ENTER_PASSWORD' ) );
		if( password ){
			TicTacToe.createGame( false,password, function( game ) {
				window.location.href='game.jsp?gameId=' + game.gameId;
			});
		};
	});

	$('#playBot').click( function() {
		
		window.location.href='listBots.jsp';
		
	});

	$('#replay').click( function() {
			window.location.href='replay.jsp';
	});
});
});