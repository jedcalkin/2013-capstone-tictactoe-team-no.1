package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
/**
 * 
 * @author Li Sui
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GetBotsTests {

	private GetBots servlet;
	
	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;
	
	@Mock
	private HttpSession session;
	
	@Mock
	private User user;

	@Mock
	private ServletConfig config;
	@Before
	public void setUp() throws Exception {
		
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession()).thenReturn(session);
		when( user.getIdentity() ).thenReturn( "Li" );
		
		servlet = new GetBots();
		servlet.init( config );
	}

	@Test
	public void testGetBots()throws Exception {
	 
		final StringWriter input =new StringWriter();
		final Collection<BotCode> botCodes =new ArrayList<BotCode>();
		BotCode botCode=new BotCode( "name", "source code" );
		Whitebox.setInternalState( botCode, "verified", true );
		botCodes.add( botCode );
        botCode.setUser(user);
        
		when( request.getSession().getAttribute( "user" ) ).thenReturn( user );
		when( response.getWriter()).thenReturn(new PrintWriter (input));
		when( user.getBots() ).thenReturn( botCodes );

		servlet.doGet( request, response );

		verify( response ).setContentType( "application/json" );
		
		final JsonArray result=new JsonParser()
				.parse(input.toString())
				.getAsJsonArray();

		assertNotNull("should return bot code ",(new Gson()).fromJson(result.get(0), BotCode.class));
	}
	

	@Test
	public void testGetNullBots()throws Exception {
	 
		final StringWriter input =new StringWriter();
		final Collection<BotCode> botCodes =new ArrayList<BotCode>();
		BotCode botCode=new BotCode( "name", "source code" );
		Whitebox.setInternalState( botCode, "verified", true );
		botCodes.add( botCode );
        botCode.setUser(user);
        
		when( request.getSession().getAttribute( "user" ) ).thenReturn( user );
		when( response.getWriter()).thenReturn(new PrintWriter (input));
		when( user.getBots() ).thenReturn(null );

		servlet.doGet( request, response );

		verify( response ).setContentType( "application/json" );
		
		final JsonArray result=new JsonParser()
				.parse(input.toString())
				.getAsJsonArray();
  
		assertTrue("should return bot code ",result.size()==0);
	}
}
