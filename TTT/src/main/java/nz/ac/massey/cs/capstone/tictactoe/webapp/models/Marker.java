package nz.ac.massey.cs.capstone.tictactoe.webapp.models;


import org.codehaus.commons.compiler.Location;
/**
 * 
 * @author Li Sui
 *
 */
public class Marker {

	public enum Kind {
		ERROR, WARNING, UNSAFE
	}

	private final Location location;
	private final String message;
	private final Kind kind;
	
	public Marker( final String message,final Kind kind, final Location location ){
		
		this.location=location;
		this.message=message;
		this.kind=kind;
		
	}

	public Location getLocation() {
		return location;
	}

	public String getMessage() {
		return message;
	}

	public Kind getKind() {
		return kind;
	}

}
