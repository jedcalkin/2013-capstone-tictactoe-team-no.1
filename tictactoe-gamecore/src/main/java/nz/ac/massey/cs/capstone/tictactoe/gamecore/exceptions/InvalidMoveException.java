package nz.ac.massey.cs.capstone.tictactoe.gamecore.exceptions;

/**
 * An exception that is thrown when a move is performed
 * that is not allowed
 *
 * @author Colin Campbell
 * @author Tom Magson
 * @author Li Sui
 *
 * TODO: Create Constructors
 * TODO: Create Test cases
 */
public class InvalidMoveException extends GameStateException {

	private static final long serialVersionUID = -3508384583793519554L;

}
