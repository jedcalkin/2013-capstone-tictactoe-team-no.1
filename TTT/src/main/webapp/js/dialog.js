define( /** @lends Dialog*/ ['jquery'],

	function( $ ) {

		var Dialog = function( title, contents ) {

			var closeButton = $( '<div/>' );
			closeButton.attr( 'class', 'dialog-close' );

			var that = this;
			closeButton.click( function() {
				that.hide();
			});

			/* The Title Bar*/
			this._titleBar = $( '<div/>' );
			this._titleBar.text( title );
			this._titleBar.append( closeButton );

			/* The Body */
			this._body = $( '<div/>' )
				.append( contents );

			/* The Window */
			this._window = $( '<div/>' )
				.append( this._titleBar )
				.append( this._body );

			/* Set some classes */
			this._window.attr( 'class', 'dialog' );
			this._body.attr( 'class', 'dialog-body' );
			this._titleBar.attr( 'class', 'dialog-titleBar' );
		}

		Dialog.prototype = {

			show : function( parent ) {
				parent.append( this._window );
				return this;
			},

			hide : function() {
				this._window.remove();
				return this;
			},

			click : function( args ) {
				this._window.click( args );
				return this;
			}
		};

		return Dialog;
	}
);
