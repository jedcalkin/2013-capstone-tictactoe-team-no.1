package nz.ac.massey.cs.capstone.tictactoe.gamecore;

/**
 * Describes the current state of the tictactoe game
 *
 * @author Colin Campbell
 * @author Tom Magson
 * @author Li Sui
 *
 */
public enum GameState {

	WIN, DRAW, INCOMPLETE
}
