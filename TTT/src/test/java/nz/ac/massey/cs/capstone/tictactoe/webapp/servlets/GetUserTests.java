/**
 * 
 */
package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.GetUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.j256.ormlite.dao.Dao;

/**
 * @author Siyan Li
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GetUserTests {


	private GetUser servlet;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	

	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	
	@Mock
	private Dao<User,String> userDao;

	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		when( context.getAttribute( Application.USER_DAO ) ).thenReturn( userDao );
		when( config.getServletContext() ).thenReturn( context );

		servlet = new GetUser();
		servlet.init( config );
	}

	/**
	 * Test method for get user{@link nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.GetUser#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
	 */
	@Test
	public void testGetUser() throws Exception {
		
		final User user = new User();
		user.setIdentity( "userId" );

		final StringWriter input = new StringWriter();
		
        when ( userDao.queryForId( "123" ) ).thenReturn( user );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getParameter( "userId" ) ).thenReturn( "123" );
        servlet.doGet( request, response );
        
        verify( response ).setContentType( "application/json" );

		final JsonObject result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonObject();

		assertEquals("should get user","userId", ( result.get( "identity" )).getAsString() );
	}

}
