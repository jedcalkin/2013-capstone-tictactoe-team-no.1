requirejs.config({
	paths : {
		jquery : '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
	},
	shim: {
		jquery : {
			exports: "jQuery"
		},
	}
});

require( ['jquery', 'tictactoe', 'menu'], function( $, TicTacToe, d3 ) {

var get = function(id){
	return document.getElementById(id);
};
var hotseat = {
	turn:1,
	board:[0, 0, 0, 0, 0, 0, 0, 0, 0],
	boxclicked: function(c){
		if(hotseat.board[c]==0){
			moved = hotseat.move(c);
			Q = hotseat.check_for_winner(hotseat.board);
			if(Q==0){
				hotseat.turn+=1;
				if(hotseat.turn==3){
					hotseat.turn=1;
				}
			}
			if((Q==1)||(Q==2))
			{
				s = 
				printline.innerHTML='player ' + hotseat.turn + ' has won.'
				reset()
			}
			if(Q>2){
				reset()
			}
		}
	},
	reset: function(){
		var pos = 'c00'
		var box = get(pos)
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				pos = 'c' + i + j
				box = get(pos)
				box.src=''
				box.alt=''
			}
		}
	},
	move:function(x){
    var pos = 'c' + x
    var box = get(pos)
    hotseat.board[x] = hotseat.turn
    if(hotseat.turn==1){
        box.className='cross'
        box.alt='X'
    }
    if(hotseat.turn==2){
        box.className='nought'
        box.alt='O'
    }
	},
	check_for_winner:function(array){
		x = 3; // if full
		for(i=0;i<8;i++){
			if(array[i]==0){
				x = 0 // more moves
			}
		}
		// hoz
		if((array[0]==array[1])&&(array[1]==array[2]))
				{x = array[1]}
		if((array[3]==array[4])&&(array[4]==array[5]))
				{x = array[4]}
		if((array[6]==array[7])&&(array[7]==array[8]))
				{x = array[7]}
		// vert
		if((array[0]==array[3])&&(array[3]==array[6]))
				{x = array[3]}
		if((array[1]==array[4])&&(array[4]==array[7]))
				{x = array[4]}
		if((array[2]==array[5])&&(array[5]==array[8]))
				{x = array[5]}
		// diag
		if((array[0]==array[4])&&(array[4]==array[8]))
				{x = array[4]}
		if((array[2]==array[4])&&(array[4]==array[6]))
				{x = array[4]}
		// return
		// 0 = no winner, continue
		// 1 = player 1 wins
		// 2 = player 2 wins
		// 3 = no winner, full
		return x;
	},
};

get('c0').addEventListener('click',function(){hotseat.boxclicked(0);},false);
get('c1').addEventListener('click',function(){hotseat.boxclicked(1);},false);
get('c2').addEventListener('click',function(){hotseat.boxclicked(2);},false);
get('c3').addEventListener('click',function(){hotseat.boxclicked(3);},false);
get('c4').addEventListener('click',function(){hotseat.boxclicked(4);},false);
get('c5').addEventListener('click',function(){hotseat.boxclicked(5);},false);
get('c6').addEventListener('click',function(){hotseat.boxclicked(6);},false);
get('c7').addEventListener('click',function(){hotseat.boxclicked(7);},false);
get('c8').addEventListener('click',function(){hotseat.boxclicked(8);},false);
});