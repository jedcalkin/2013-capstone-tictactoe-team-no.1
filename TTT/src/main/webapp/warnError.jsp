<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="java.util.ResourceBundle" %>
<% ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );
%>
<%@ include file="page_head_tmp.include" %>
</header>
<section>
<div>
	<h2><%= resourceBundle.getString( "OOPS" ) %>!!</h2>
	<h2>500</h2>
	<h2><%= resourceBundle.getString( "MESSAGE" ) %>!!!</h2>
	<h4><%= resourceBundle.getString( "MESSAGE_GIVE_USER" ) %>.</h4>
	<h4><%= resourceBundle.getString( "MESSAGE_GUDIE_USER_ERROR" ) %>.</h4>
</div>
</section>
<%@ include file="page_tail_tmp.include" %>