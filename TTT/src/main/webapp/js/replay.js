requirejs.config({
		paths : {
			jquery : '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min',
			i18n : '//cdn.jsdelivr.net/i18next/1.7.1/i18next.min'
	},
	shim: {
			jquery : {
					exports: "jQuery"
			},
			i18n : {
				exports: "i18n"
			}
	}
});

require( ['jquery', 'tictactoe', 'menu'], function( $, TicTacToe, d3 ) {

	var timer3 = '';
	var timeout = 3000;
	var game = [];
	var move = 0;

	var loadgame = function(){
		i=0;
		while((i<81)&&(window.localStorage['m'+i]!='end')){
			game[i] = window.localStorage['m'+i];
		i++;}
		replay_game(0, 1000);
	}

	var replay_game = function(x, time){
		timeout = rangeslider.value*100;
		speed.innerHTML = 'Speed: '+timeout+'ms'
		move = 0;
		//clearTimeout(timer3);
		// start
		timer3 = setTimeout(place_move, timeout);
	}

	var place_move = function () {
		timeout = rangeslider.value*100;
		speed.innerHTML = 'Speed: '+timeout+'ms'
		
		id = game[move];
		if (move%2==0) { peice = "cross_mini"; }
			else { peice = "nought_mini"; }
		$(id).addClass(peice);

		move +=1;
		if(move<game.length){
			clearTimeout(timer3);
			timer3 = setTimeout(place_move, timeout);
		}
	}
	var unplace_move = function () {
		move-=1;
		id = game[move];
		$(id).removeClass();
	}

	var state = 'run'
	var pause_f = function(){
		if(state=='run'){
			clearTimeout(timer3);
			state = 'stoped';
			pause.innerHTML = 'Play';
		}else{
			place_move();
			state = 'run';
			pause.innerHTML = '||';
		}
	}

	var foward = function(){
		id = game[move];
		if (move%2==0) { peice = "cross_mini"; }
			else { peice = "nought_mini"; }
		$(id).addClass(peice);
		move +=1;
	}
	
	var slide = function(){
		clearTimeout(timer3);
		timeout = rangeslider.value*100;
		speed.innerHTML = 'Speed: '+timeout+'ms'
		timer3 = setTimeout(place_move, timeout);
	}

	rangeslider.addEventListener('change',slide,false);
	stepfoawrd.addEventListener('click',foward,false);
	stepback.addEventListener('click',unplace_move,false);
	pause.addEventListener('click',pause_f,false);
	loadgame();
});
