package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.BotCode;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.j256.ormlite.dao.Dao;

/**
 * Test cases for the ListBots servlet
 *
 * @author Colin Campbell
 */
@RunWith( MockitoJUnitRunner.class )
public class ListBotsTests {

	private ListBots servlet;

	@Mock
	private Dao<BotCode, Integer> botCodeDao;

	@Mock
	private ServletContext context;

	@Mock
	private ServletConfig config;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Before
	public void setUp() throws Exception {

		when( context.getAttribute( Application.BOT_DAO ) ).thenReturn( botCodeDao );
		when( config.getServletContext() ).thenReturn( context );

		servlet = new ListBots();
		servlet.init( config );
	}

	@Test
	@SuppressWarnings("unchecked")
	public void test() throws Exception {

		final StringWriter input = new StringWriter();

		final User botCreator = new User();
		botCreator.setDisplayName( "Bot Creator" );
		final BotCode botCode = new BotCode( "MyBot", "MyCode" );
		botCode.setUser( botCreator );
		botCode.setIdentity( 8547 );
		final List<BotCode> botCodes = new ImmutableList.Builder<BotCode>()
			.add( botCode )
			.build();

		when( botCodeDao.queryForFieldValues(
				( Map<String, Object> ) any() )
			)
			.thenReturn(
				botCodes
			);

		when( response.getWriter() )
			.thenReturn(
				new PrintWriter( input )
			);

		servlet.doGet( request, response );

		final JsonArray result = new JsonParser()
			.parse( input.toString() )
			.getAsJsonArray();

		final JsonObject bot = result.get( 0 )
			.getAsJsonObject();

		assertEquals( "Bots identity should be 8547", 8547, bot.get( "identity" ).getAsInt() );
	}
}
