<%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
<%@ page import="org.apache.commons.codec.digest.DigestUtils" %>
<%@ page import="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import='com.j256.ormlite.dao.Dao' %>
<%@ page import='nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application' %>
<%@ page session="true" %>
<jsp:useBean id="user" class="nz.ac.massey.cs.capstone.tictactoe.webapp.models.User" scope="session"/>
<%
        String show="block";
		ResourceBundle resourceBundle = ResourceBundle.getBundle( "nz.ac.massey.cs.capstone.tictactoe.webapp.i18n.Language", request.getLocale() );
		String userId = request.getParameter("userId");
		if(!user.getIdentity().equals(userId)){
			 show ="none";
		}
		
			@SuppressWarnings("unchecked")
			final Dao<User, String> userDao = ( Dao<User, String> ) getServletContext().getAttribute( Application.USER_DAO );
			final User currentUser=userDao.queryForId(userId);
			userDao.refresh( user );
		
	
%>

<%@ include file="page_head.include" %>
<div id="login_info">
	<a href="https://en.gravatar.com/">
		<img class="glossy image-wrap" src="http://www.gravatar.com/avatar/<%= DigestUtils.md5Hex( user.getEmail() )%>?s=80" alt="gravatar" />
	</a>
	<%= resourceBundle.getString( "CURRENTLY_LOGIN" ) %>:<br>
	<b><%=currentUser.getEmail() %></b><br>
</div>
</header>
<script data-main="js/profile" src="js/require.js"></script>

<%@ include file="page_menu.include" %>

<section>
	<div id="player1" class="current-player">
		<b id="me"></b><br>
		<img src="http://www.gravatar.com/avatar/<%=DigestUtils.md5Hex( currentUser.getEmail() ) %>?s=256" alt="gravatar" />
		<br>
		<b><%= resourceBundle.getString( "WINS" ) %>:&ensp;</b><%=currentUser.getNumWins()%><br>
		<b><%= resourceBundle.getString( "DRAWS" ) %>:&ensp;</b><%= currentUser.getNumDraws()%><br>
		<b><%= resourceBundle.getString( "LOSSES" ) %>:&ensp;</b><%=currentUser.getNumLoses() %><br>
	</div>
	<div id="change_profile">
		<h2><%= resourceBundle.getString( "PROFILE" ) %></h2><hr>
		<div id="contentOfProfile">
			<form id="myForm">
				<div id="updateprofile">
						
						<input type="hidden" id="editiable" value="<%=show%> " />

					</div>
			</form>
		</div>
	</div>
</section>

<%@ include file="page_tail.include" %>