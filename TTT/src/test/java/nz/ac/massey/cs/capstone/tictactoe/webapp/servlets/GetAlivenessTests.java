package nz.ac.massey.cs.capstone.tictactoe.webapp.servlets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nz.ac.massey.cs.capstone.tictactoe.gamecore.Game;
import nz.ac.massey.cs.capstone.tictactoe.gamecore.Lobby;
import nz.ac.massey.cs.capstone.tictactoe.webapp.listeners.Application;
import nz.ac.massey.cs.capstone.tictactoe.webapp.models.User;
import nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.GetAliveness;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author Siyan Li
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GetAlivenessTests {
	
	private Lobby lobby;

	private User creator;

	private User opponent;

	private Map<Game,Long[]> list;

	private GetAliveness servlet;

	@Mock
	private  ServletContext context;

	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpSession session;

	@Mock
	private HttpServletResponse response;

	@Mock
	private ServletConfig config;
	

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		lobby = new Lobby();

		creator = new User();
		creator.setIdentity( "creator" );

		opponent = new User();
		opponent.setIdentity( "opponent" );

		list=new ConcurrentHashMap<Game,Long[]>();

		when( context.getAttribute( Application.TIMEOUTS ) ).thenReturn( list );
		when( context.getAttribute( Application.LOBBY ) ).thenReturn( lobby );
		when( config.getServletContext() ).thenReturn( context );
		when( request.getSession() ).thenReturn( session );

		servlet = new GetAliveness();
		servlet.init( config );
	}

	/**
	 * Test method for no opponent {@link nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.GetAliveness#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
	 * @throws Exception 
	 */
	@Test
	public void testNoOpponent() throws Exception {

		final String gameId = lobby.createGame( creator.getIdentity(), true,null );
		final StringWriter input = new StringWriter();

		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getSession().getAttribute("user")).thenReturn(creator);
		
		servlet.doGet( request, response );
		
		 verify( response ).setContentType( "application/json" );

			final JsonObject result = new JsonParser()
				.parse( input.toString() )
				.getAsJsonObject();
			assertEquals("should get aliveness true","true", ( result.get( "alive" )).getAsString() );
	}
	
	/**
	 * Test method for Timeout {@link nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.GetAliveness#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
	 * @throws Exception 
	 */
	@Test
	public void testTimeout() throws Exception {
		
		final String gameId = lobby.createGame( creator.getIdentity(), true,null );
		lobby.joinGame( gameId, opponent.getIdentity() );
		final StringWriter input = new StringWriter();
	    final Game game=lobby.getGameByGameId(gameId);
	    Long[] time=new Long[2];
	    time[0]=(long) 1;
	    list.put(game, time);
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getSession().getAttribute("user")).thenReturn(opponent);
		
		servlet.doGet( request, response );
		
		 verify( response ).setContentType( "application/json" );

			final JsonObject result = new JsonParser()
				.parse( input.toString() )
				.getAsJsonObject();
			assertEquals("should get aliveness false","false", ( result.get( "alive" )).getAsString() );
	}
	
	/**
	 * Test method for not timeout {@link nz.ac.massey.cs.capstone.tictactoe.webapp.servlets.GetAliveness#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
	 * @throws Exception 
	 */
	@Test
	public void testNoTimeout() throws Exception {

		final String gameId = lobby.createGame( creator.getIdentity(), true,null );
		lobby.joinGame( gameId, opponent.getIdentity() );
		final StringWriter input = new StringWriter();
		 
		final Game game=lobby.getGameByGameId(gameId);
	    Long[] time=new Long[2];
	    time[0]=System.currentTimeMillis();
	    list.put(game, time);
	    
		when( request.getParameter( "gameId") ).thenReturn( gameId );
		when( response.getWriter() ).thenReturn( new PrintWriter( input ) );
		when( request.getSession().getAttribute("user")).thenReturn(opponent);
		
		servlet.doGet( request, response );
		
		 verify( response ).setContentType( "application/json" );

		 final JsonObject result = new JsonParser()
		 	.parse( input.toString() )
		 	.getAsJsonObject();

		 assertEquals("should get aliveness true","true", ( result.get( "alive" )).getAsString() );
	}

}
